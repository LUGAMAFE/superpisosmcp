var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();
local_data = {
    "proxy": ""
}
gulp.task('browser-sync', function() {
    browserSync.init({
        proxy  : local_data.proxy,
        online : false,
        open   : false,
    });
});

var sassPaths = [
    'bower_components/foundation-sites/scss',
    'bower_components/motion-ui/src'
];

gulp.task('sass', function() {
    return gulp.src('scss/app.scss')
        .pipe($.sass({
            includePaths: sassPaths,
            outputStyle: 'compressed' // if css compressed **file size**
        })
        .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('../public/assets/css'));
});

gulp.task('admin', function() {
    return gulp.src('scss_admin/admin.scss')
        .pipe($.sass({
            includePaths: sassPaths,
            outputStyle: 'compressed' // if css compressed **file size**
        })
        .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('../public/assets/css'));
});

gulp.task('default', ['sass', 'admin', 'browser-sync'], function() {
    gulp.watch(['scss/**/*.scss'], ['sass']);
    gulp.watch(['scss_admin/**/*.scss'], ['admin']);
    gulp.watch('scss/**/*.scss').on('change', browserSync.reload);
});
