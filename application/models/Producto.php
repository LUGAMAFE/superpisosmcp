<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Producto extends CI_Model {

	protected $_table = 'art';
	protected $_prefix = '';
	protected $_id         = '';
	protected $pre_insert  = array('url_str', 'datetime_creado');
	protected $pre_update  = array('url_str', 'datetime_modificado');
	protected $field_names = array();

	public function __construct() {
		parent::__construct("productos");	
		$this->_id = 'Articulo';
		foreach($this->db->list_fields($this->_table) as $key => $row) {
			if($this->_id != $row) {
				$this->field_names = array_merge($this->field_names, array($row));
			}
		}		
	}
}