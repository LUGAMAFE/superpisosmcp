<?php
class Carrito extends MY_Model {

    public function __construct(){
        parent::__construct("productos");
    }

    public function obtenerProductosCarrito($productosCarrito){
        if($productosCarrito == null){
            return [];
        }else{
            //$productosCarrito = str_replace("%2C", ",", $productosCarrito);
            $this->db->select('*');
            $this->db->from('productos');
            $this->db->join('imagenes_productos', 'productos.id_mcp = imagenes_productos.id_producto_asoc_producto', "left");
            $this->db->join('archivos', 'imagenes_productos.id_file_asoc_producto = archivos.id_file', "left");
            $this->db->where("imagenes_productos.imagen_principal = 1 AND productos.id_mcp IN ($productosCarrito)");
            $query = $this->db->get();
            return $query->result_array();
        }
    }

    public function obtenerProductosRelacionadosCarrito($productosCarrito){
        $prodRel = "";
        $listaIdsMcp = [];
        foreach($productosCarrito as $productoCarrito){
            $arregloProdRel = json_decode($productoCarrito['productos_asociados']);
            $listaIdsMcp = array_merge($listaIdsMcp, $arregloProdRel);
        }
        $implodeIds = implode("','", $listaIdsMcp);
        $prodRel .= $implodeIds;
        if($prodRel == ""){
            return [];
        }else{
            $query = $this->db->query("select productos.id_mcp AS id_mcp, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.descripcion_producto AS desc_producto, productos.informacion_extra_producto AS info_extra, productos.producto_rey AS producto_rey, productos.descuento_activado_producto AS descuento_activado, productos.nuevo_prod AS has_data, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_producto
                from productos join subcategorias on productos.subcategoria_mcp = subcategorias.nombre_subcategoria
                left join categorias_subcategorias on subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria
                left join categorias on categorias_subcategorias.categorias_id_cat = categorias.id_cat 
                left join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto'
                left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
                WHERE productos.nuevo_prod != 1 AND productos.visible_prod != 0 AND id_mcp IN ('$prodRel') ORDER BY RAND() LIMIT 12;");
            return $query->result_array();
        }
    }

    public function obtenerCantidadCarrito($guardado = false, $eliminado = false){
        if(isset($_COOKIE['productos_carrito'])){
            $productosCarrito = $_COOKIE['productos_carrito'];
            $productosCarrito = explode(",",$productosCarrito);
            $productosCarrito = array_unique($productosCarrito);
            $cuenta = count($productosCarrito);
        } else {
            $cuenta = 0;
        }

        if($guardado){
            return $cuenta + 1;
        } else if($eliminado){
            return $cuenta - 1;
        } else {
            return $cuenta;
        }
    }

    public function guardarProductoCarrito($id, $cantidad){
        if(isset($_COOKIE['productos_carrito'])){
            $ahora = strtotime("now");
            $limite = strtotime("tomorrow");
            $differenceInSeconds = $limite - $ahora;
            $prodCarritosCookie = $_COOKIE['productos_carrito'].",".$id;
            setcookie('productos_carrito', $prodCarritosCookie, time() + ($differenceInSeconds), "/");
            if(isset($_COOKIE['cantidades_carrito'])){
                $cantidades = json_decode($_COOKIE['cantidades_carrito']);
                $cantidades->$id = intval($cantidad);
                $cantidades = json_encode($cantidades);
                setcookie('cantidades_carrito', $cantidades, time() + ($differenceInSeconds), "/");
            } else {
                $cantidades[$id] = intval($cantidad);
                $cantidades = json_encode($cantidades);
                setcookie('cantidades_carrito', $cantidades, time() + ($differenceInSeconds), "/");
            }
            $productosCarrito = $_COOKIE['productos_carrito'];
            $productosCarrito = explode(",",$productosCarrito);
            $productosCarrito = array_unique($productosCarrito);
            $cuenta = count($productosCarrito);
            $productosCarrito = $prodCarritosCookie;
            $productosCarrito = explode(",",$productosCarrito);
            $productosCarrito = array_unique($productosCarrito);
            $cuentaNueva = count($productosCarrito);
            if($cuenta == $cuentaNueva){
                return false;
            } else {
                return true;
            }
        } else {
            $ahora = strtotime("now");
            $limite = strtotime("tomorrow");
            $differenceInSeconds = $limite - $ahora;
            $cantidades[$id] = intval($cantidad);
            $cantidades = json_encode($cantidades);
            setcookie('productos_carrito', $id, time() + ($differenceInSeconds), "/");
            setcookie('cantidades_carrito', $cantidades, time() + ($differenceInSeconds), "/");
            return true;
        }
    }

    public function eliminarProductoCarrito($id){
        $eliminado = array(",".$id,$id);
        $_COOKIE['productos_carrito'] = str_replace($eliminado, "", $_COOKIE['productos_carrito']);
        $ahora = strtotime("now");
        $limite = strtotime("tomorrow");
        $differenceInSeconds = $limite - $ahora;
        setcookie('productos_carrito', $_COOKIE['productos_carrito'], time() + ($differenceInSeconds), "/");
        if($_COOKIE['productos_carrito'] == "") {
            setcookie("productos_carrito", "", time() - 3600);
        }
        return true;
    }

}