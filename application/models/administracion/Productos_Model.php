<?php
class Productos_Model extends MY_Model {

    public function __construct(){
        parent::__construct("productos");
    }

    public function getProducto($datos){
        if(isset($datos["id"])){
            $data["id_mcp"] = $datos["id"];
        }
        $query = $this->db->get_where($this->table, $data);
        $row = $query->row_array();
        return $row;
    }

    public function guardarImagenesProducto($id, $ids, $jsonInputExtras){
        foreach ($ids as $key => $value) {
            $datos = array(
                "id_producto_asoc_producto" => $id,
                "id_file_asoc_producto" => $value,
                "tipo_imagen_producto" => "imagen_producto",
                "json_input_extras" => json_encode($jsonInputExtras[$key]),
                "imagen_principal" => $jsonInputExtras[$key]->imagen_principal
            );
            $this->db->insert("imagenes_productos", $datos);
        }
    }

    public function actualizarProducto($data, $id){
        $this->db->where("id_mcp", $id);
        $resultado = $this->db->update("productos", $data);
    }

    public function guardarImagenesCasos($id, $ids, $jsonInputExtras){
        foreach ($ids as $key => $value) {
            $datos = array(
                "id_producto_asoc_producto" => $id,
                "id_file_asoc_producto" => $value,
                "tipo_imagen_producto" => "imagen_caso",
                "json_input_extras" => isset($jsonInputExtras[$key]) ? json_encode($jsonInputExtras[$key]) :  json_encode(new stdClass()),
            );
            $this->db->insert("imagenes_productos", $datos);
        }
    }

    public function getImagenesProducto($id){
        $query = "SELECT archivos.*,imagenes_productos.json_input_extras FROM archivos
        LEFT JOIN imagenes_productos ON imagenes_productos.id_file_asoc_producto = archivos.id_file 
        LEFT JOIN productos ON productos.id_mcp = imagenes_productos.id_producto_asoc_producto 
        WHERE productos.id_mcp = '$id' AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' ";
        $row = $this->query($query);
        return $row;
    }
    
    public function getImagenesOrdenadasProducto($id){
        $query = "SELECT archivos.*,imagenes_productos.json_input_extras FROM archivos
        LEFT JOIN imagenes_productos ON imagenes_productos.id_file_asoc_producto = archivos.id_file 
        LEFT JOIN productos ON productos.id_mcp = imagenes_productos.id_producto_asoc_producto 
        WHERE productos.id_mcp = '$id' AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' ORDER BY imagenes_productos.imagen_principal DESC ";
        $row = $this->query($query);
        return $row;
    }

    public function emptyImagenesProducto($id){
        $this->db->where('id_producto_asoc_producto', $id);
        $this->db->where("tipo_imagen_producto", 'imagen_producto');
        $this->db->delete('imagenes_productos');
    }

    public function emptyImagenesCasosProducto($id){
        $this->db->where('id_producto_asoc_producto', $id);
        $this->db->where("tipo_imagen_producto", 'imagen_caso');
        $this->db->delete('imagenes_productos');
    }

    public function getImagenesCasosProducto($id){
        $query = "SELECT archivos.*,imagenes_productos.json_input_extras FROM archivos
        LEFT JOIN imagenes_productos ON imagenes_productos.id_file_asoc_producto = archivos.id_file 
        LEFT JOIN productos ON productos.id_mcp = imagenes_productos.id_producto_asoc_producto 
        WHERE productos.id_mcp = '$id' AND imagenes_productos.tipo_imagen_producto = 'imagen_caso' ";
        $row = $this->query($query);
        return $row;
    }

    public function getProductosInfo(){
    $this->db->select('id_mcp AS "Articulo", grupo_mcp AS "Grupo", desc_mcp AS "Descripcion1", subcategoria_mcp AS "Subcategoria"');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function insertarNuevosProductos(array $productos){
        if(count($productos) > 0){
            foreach ($productos as $producto) {
                $inserciones[] = array(
                    "id_mcp" => $producto["Articulo"],
                    "visible_prod" => 0,
                    "nuevo_prod" => 1,
                    "grupo_mcp" => $producto["Grupo"],
                    "desc_mcp" => $producto["Descripcion1"],
                    "subcategoria_mcp" => $producto["Subcategoria"]
                );
            }
            $resultado = $this->db->insert_batch($this->table, $inserciones);
        }
    }

    public function actualizarProductos(array $productos){
        if(count($productos) > 0){
            foreach ($productos as $producto) {
                $update = array(
                    "grupo_mcp" => $producto["Grupo"],
                    "desc_mcp" => $producto["Descripcion1"],
                    "subcategoria_mcp" => $producto["Subcategoria"]
                );
                $this->db->where("id_mcp", $producto["Articulo"]);
                $result = $this->db->update($this->table, $update);
            }
        }
    }

    public function actualizarVisibilidadProducto($idProducto, $visibilidad){
        $visibilidad = ($visibilidad === 'true' || $visibilidad === 'TRUE') ? true : false;
        if(!is_bool($visibilidad)){
            return false;
        }
        $visible = $visibilidad ? 1 : 0;
        $update = array(
            "visible_prod" =>  $visible
        );
        $this->db->where("id_mcp", $idProducto);
        $result = $this->db->update($this->table, $update);
        return $result;
    }

    public function obtenerProductosTabla($filters = array(), $columnas, $count = false){
        if($count){
            $this->db->select('COUNT(*)');
        }else{
            $this->db->select('*');
        }

        $this->db->from('productos_tabla_vista');

        if(array_key_exists("search", $filters)){
            $search = $filters["search"];
            $tokens = explode(" ", $search);

            foreach($columnas as $columna){
                $this->db->or_like($columna, $search);
                foreach ($tokens as $token) {
                    if($token !== $search && $token !== ""){
                        $this->db->or_like($columna, $token);
                    }
                }
            }

        }

        if(!$count && array_key_exists("offset", $filters) && array_key_exists("length", $filters)){
            $this->db->limit($filters["length"], $filters["offset"]);
        }

        if(array_key_exists("order", $filters)){
            $this->db->order_by($filters["order"]["column"], $filters["order"]["dir"]);
        }
        
        $query = $this->db->get();  

        if($count){
            return $query->result_array()[0]["COUNT(*)"];
        }

        $res = $this->db->last_query();

        return $query->result_array();
    }

    public function obtenerFiltrosProducto($idProducto){
        $query = $this->db->query("select categorias.filtros_categoria AS filtros_categoria 
        from (((productos join subcategorias on((productos.subcategoria_mcp = subcategorias.nombre_subcategoria))) 
        left join categorias_subcategorias on((subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria))) 
        left join categorias on((categorias_subcategorias.categorias_id_cat = categorias.id_cat))) 
        WHERE productos.id_mcp = '" . $idProducto . "'");
        //$res = $this->db->last_query();
        //$query = $this->db->get();
        $result = $query->row();
        return json_decode($result->filtros_categoria);
    }
    
    public function obtenerListaProductosParaRelacionados($excludedId){
        $query = $this->db->query("select productos.id_prod AS id_prod, productos.id_mcp AS id_mcp, ifnull(categorias.nombre_categoria,'Sin Categorizar') AS nombre_categoria, subcategorias.nombre_subcategoria AS nombre_subcategoria, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.desc_mcp AS desc_mcp, (case when (productos.visible_prod = 1) then 'Visible' when (productos.visible_prod = 0) then 'Oculto' end) AS `visible_prod`, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_producto
        from productos join subcategorias on productos.subcategoria_mcp = subcategorias.nombre_subcategoria 
        left join categorias_subcategorias on subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria
        left join categorias on categorias_subcategorias.categorias_id_cat = categorias.id_cat 
        left join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1
        left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
        WHERE productos.id_mcp != '".$excludedId."';");
        return json_encode($query->result_array());
    }

    public function obtenerProductosRelacionados($listaIdsMcp, $excludedId){
        if($listaIdsMcp == null){
            $query = [];
        }else{
            $implodeIds = implode("','", $listaIdsMcp);
            $query = $this->db->query("select productos.id_prod AS id_prod, productos.id_mcp AS id_mcp, ifnull(categorias.nombre_categoria,'Sin Categorizar') AS nombre_categoria, subcategorias.nombre_subcategoria AS nombre_subcategoria, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.desc_mcp AS desc_mcp, (case when (productos.visible_prod = 1) then 'Visible' when (productos.visible_prod = 0) then 'Oculto' end) AS `visible_prod`, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_producto
            from productos join subcategorias on productos.subcategoria_mcp = subcategorias.nombre_subcategoria
            left join categorias_subcategorias on subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria
            left join categorias on categorias_subcategorias.categorias_id_cat = categorias.id_cat 
            left join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto'
            left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
            WHERE productos.id_mcp != '".$excludedId."' AND id_mcp IN ('$implodeIds');");
            $query = $query->result_array();
        }
        return json_encode($query);
    }

    public function obtenerProductosRelacionadosParaInfoProducto($listaIdsMcp){
        if($listaIdsMcp == null){
            return [];
        }else{
            $implodeIds = implode("','", $listaIdsMcp);
            $query = $this->db->query("select productos.id_mcp AS id_mcp, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.descripcion_producto AS desc_producto, productos.informacion_extra_producto AS info_extra, productos.producto_rey AS producto_rey, productos.descuento_activado_producto AS descuento_activado, productos.nuevo_prod AS has_data, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_producto
                from productos join subcategorias on productos.subcategoria_mcp = subcategorias.nombre_subcategoria
                left join categorias_subcategorias on subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria
                left join categorias on categorias_subcategorias.categorias_id_cat = categorias.id_cat 
                left join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto'
                left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
                WHERE productos.nuevo_prod != 1 AND productos.visible_prod != 0 AND id_mcp IN ('$implodeIds') ORDER BY RAND() LIMIT 12;");
            return $query->result_array();
        }
    }
            
    public function obtenerListaProductosMasVendidosParaHome(){
        $query = $this->db->query("select productos.id_mcp AS id_mcp, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.descripcion_producto AS desc_producto, productos.informacion_extra_producto AS info_extra, productos.producto_rey AS producto_rey, productos.descuento_activado_producto AS descuento_activado, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_producto
        from productos join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1
        left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
        WHERE productos.nuevo_prod != 1 AND productos.visible_prod != 0 AND productos.producto_mas_vendido = 1 ORDER BY RAND() LIMIT 12;");
        return json_encode($query->result_array());
    }

    public function obtenerListaProductosDelReyParaHome(){
        $query = $this->db->query("select productos.id_mcp AS id_mcp, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.descripcion_producto AS desc_producto, productos.informacion_extra_producto AS info_extra, productos.descuento_activado_producto AS descuento_activado, ifnull(archivos.full_route_file,  'assets/img/notfound.png') AS imagen_producto
        from productos join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1
        left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
        WHERE productos.nuevo_prod != 1 AND productos.visible_prod != 0 AND productos.producto_rey = 1 ORDER BY RAND() LIMIT 12;");
        return json_encode($query->result_array());
    }

    public function obtenerListaProductosRecomendados(){
        $query = $this->db->query("select productos.id_mcp AS id_mcp, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.descripcion_producto AS desc_producto, productos.informacion_extra_producto AS info_extra, productos.producto_rey AS producto_rey, productos.descuento_activado_producto AS descuento_activado, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_producto
        from productos join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1
        left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
        WHERE productos.nuevo_prod != 1 AND productos.visible_prod != 0 ORDER BY RAND() LIMIT 12;");
        return json_encode($query->result_array());
    }

    public function obtenerInfoProductoCompleta($id){
        $query = $this->db->query("select productos.id_prod AS id_prod, productos.id_mcp AS id_mcp, ifnull(categorias.nombre_categoria,'Sin Categorizar') AS nombre_categoria, subcategorias.nombre_subcategoria AS nombre_subcategoria, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.desc_mcp AS desc_mcp, (case when (productos.visible_prod = 1) then 'Visible' when (productos.visible_prod = 0) then 'Oculto' end) AS `visible_prod`, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_producto,
        productos.descripcion_producto AS desc_prod, productos.informacion_extra_producto AS info_prod, productos.especificaciones_producto AS espec_prod, productos.producto_rey AS producto_rey, productos.descuento_activado_producto AS descuento_activado, productos.descuento_producto AS descuento_producto, productos.filtros_producto AS filtros_producto, productos.productos_asociados AS productos_asociados, productos.nuevo_prod AS has_data, categorias.filtros_categoria AS filtros_categoria
        from productos join subcategorias on productos.subcategoria_mcp = subcategorias.nombre_subcategoria 
        left join categorias_subcategorias on subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria
        left join categorias on categorias_subcategorias.categorias_id_cat = categorias.id_cat 
        left join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1
        left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
        WHERE productos.nuevo_prod != 1 AND productos.visible_prod != 0 AND productos.id_mcp = '".$id."';");
        $result = $query->result_array();
        if(empty($result)){
            return $result;
        }
        return $result[0];
    }

    public function getProductosEnRemate(){
        $this->db->select("
        productos.id_mcp AS id_mcp, 
        productos.id_mcp AS id_mcp, 
        productos.nombre_producto AS nombre_producto, 
        productos.descripcion_producto AS desc_producto, 
        productos.informacion_extra_producto AS info_extra, 
        productos.descuento_activado_producto AS descuento_activado,
        productos.producto_rey,
        productos.producto_mas_vendido,
        productos.descuento_producto AS descuento_producto,
        productos.filtros_producto,
        archivos_vista.*");
        $this->db->from($this->table);
        $this->db->join("imagenes_productos", "imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1", "left");
        $this->db->join("archivos_vista", "imagenes_productos.id_file_asoc_producto = archivos_vista.id_file");
        $this->db->where('nuevo_prod', "0");
        $this->db->where("visible_prod", "1");
        $this->db->where("descuento_activado_producto", "1");
        $this->db->where('descuento_producto is NOT NULL', NULL, FALSE);
        $this->db->where('descuento_producto !=', "");
        $query = $this->db->get();
        return $query->result();
    }

    public function getProductosCategoria($categoria){
        $this->db->select("
        productos.id_mcp AS id_mcp, 
        productos.id_mcp AS id_mcp, 
        productos.nombre_producto AS nombre_producto, 
        productos.descripcion_producto AS desc_producto, 
        productos.informacion_extra_producto AS info_extra, 
        productos.descuento_activado_producto AS descuento_activado,
        productos.producto_rey,
        productos.producto_mas_vendido,
        productos.descuento_producto AS descuento_producto,
        productos.filtros_producto,
        archivos_vista.*");
        $this->db->from($this->table);
        $this->db->join("subcategorias", "productos.subcategoria_mcp = subcategorias.nombre_subcategoria");
        $this->db->join("categorias_subcategorias", "subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria", "left");
        $this->db->join("categorias", "categorias_subcategorias.categorias_id_cat = categorias.id_cat", "left");
        $this->db->join("imagenes_productos", "imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1", "left");
        $this->db->join("archivos_vista", "imagenes_productos.id_file_asoc_producto = archivos_vista.id_file");
        $this->db->where('nuevo_prod', "0");
        $this->db->where("visible_prod", "1");
        $this->db->where('nombre_categoria', ucwords(mb_strtolower($categoria)));
        $query = $this->db->get();
        
        //echo($this->db->last_query());
        return $query->result();
    }

    public function getProductosSubcategoria($subcategoria){
        $this->db->select("
        productos.id_mcp AS id_mcp, 
        productos.id_mcp AS id_mcp, 
        productos.nombre_producto AS nombre_producto, 
        productos.descripcion_producto AS desc_producto, 
        productos.informacion_extra_producto AS info_extra, 
        productos.descuento_activado_producto AS descuento_activado,
        productos.producto_rey,
        productos.producto_mas_vendido,
        productos.descuento_producto AS descuento_producto,
        productos.filtros_producto,
        archivos_vista.*");
        $this->db->from($this->table);
        $this->db->join("subcategorias", "productos.subcategoria_mcp = subcategorias.nombre_subcategoria");
        $this->db->join("categorias_subcategorias", "subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria", "left");
        $this->db->join("categorias", "categorias_subcategorias.categorias_id_cat = categorias.id_cat", "left");
        $this->db->join("imagenes_productos", "imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1", "left");
        $this->db->join("archivos_vista", "imagenes_productos.id_file_asoc_producto = archivos_vista.id_file");
        $this->db->where('nuevo_prod', "0");
        $this->db->where("visible_prod", "1");
        $this->db->where('nombre_subcategoria', ucwords(mb_strtolower($subcategoria)));
        $query = $this->db->get();
        
        //echo($this->db->last_query());
        return $query->result();
    }

    public function obtenerFiltros($productos){
        $result = [];
        foreach($productos as $producto){
            $filtros = json_decode($producto->filtros_producto);
            foreach($filtros as $key=>$valor){
                $producto->$key = $valor;
            }
            $result[] = $producto;
        }
        return $result;
    }

    public function countProductosEnRemate(){
        $this->db->select("COUNT(*) AS COUNT");
        $this->db->from($this->table);
        $this->datosProductosRemate();
        $query = $this->db->get();
        return $query->row();
    }

    private function datosProductosRemate(){
        $this->db->join("imagenes_productos", "imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1", "left");
        $this->db->join("archivos_vista", "imagenes_productos.id_file_asoc_producto = archivos_vista.id_file");
        $this->db->where('nuevo_prod', "0");
        $this->db->where("visible_prod", "1");
        $this->db->where("descuento_activado_producto", "1");
        $this->db->where('descuento_producto is NOT NULL', NULL, FALSE);
        $this->db->where('descuento_producto !=', "");
    }

    public function getProductosBusqueda($termino){
        if ($termino == ""){
            return [];
        } else {
            $this->db->select("
            productos.id_mcp AS id_mcp, 
            productos.id_mcp AS id_mcp, 
            productos.nombre_producto AS nombre_producto, 
            productos.descripcion_producto AS desc_producto, 
            productos.informacion_extra_producto AS info_extra, 
            productos.descuento_activado_producto AS descuento_activado,
            productos.producto_rey,
            productos.producto_mas_vendido,
            productos.descuento_producto AS descuento_producto,
            productos.filtros_producto,
            archivos_vista.*");
            $this->db->from($this->table);
            $this->db->join("subcategorias", "productos.subcategoria_mcp = subcategorias.nombre_subcategoria");
            $this->db->join("imagenes_productos", "imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto' AND imagenes_productos.imagen_principal = 1", "left");
            $this->db->join("archivos_vista", "imagenes_productos.id_file_asoc_producto = archivos_vista.id_file");
            $this->db->where('nuevo_prod', "0");
            $this->db->where("visible_prod", "1");
            $this->db->like('nombre_producto', ucwords(mb_strtolower($termino)), 'both');
            $this->db->or_like('id_mcp', ucwords(mb_strtolower($termino)), 'both');
            $query = $this->db->get();
            
            //echo($this->db->last_query());
            return $query->result();
        }
    }
    

    public function obtenerDetallesProducto($datosProducto){
        $filtrosProducto = json_decode($datosProducto['filtros_producto']);
        $filtrosCategoria = json_decode($datosProducto['filtros_categoria']);
        $detalles = [];
        if(!empty($filtrosCategoria) && !empty($filtrosProducto)){
            foreach ($filtrosCategoria->schema as $filtroCategoria)
            {
                if($filtroCategoria->active){
                    $detalle['nombre_detalle'] = $filtroCategoria->name;
                    $uuid = $filtroCategoria->uuid;
                    $detalle['valor_detalle'] = $filtrosProducto->$uuid;
                    if($filtroCategoria->type == "color"){
                        foreach ($filtroCategoria->enum as $enum){
                            if($enum->color == $detalle['valor_detalle']){
                                $detalle['valor_detalle'] = $enum->name;
                            }
                        }
                    }
                    if($filtroCategoria->type == "checkbox"){
                        $cadenaValor = "";
                        $i = 0;
                        foreach ($detalle['valor_detalle'] as $valor){
                            $cadenaValor .= $valor;
                            if(++$i != count($detalle['valor_detalle'])) {
                                $cadenaValor .= " / ";
                            }
                        }
                        $detalle['valor_detalle'] = $cadenaValor;
                    }
                    $detalles[] = $detalle;
                }
            }
        }
        return $detalles;
    }
}