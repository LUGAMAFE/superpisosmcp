<?php
class Categorias_Model extends MY_Model {

    public function __construct(){
        parent::__construct("categorias");	
    }

    //Funciones De Grupos_Model que antes se usaban y se pasaron a Subcategorias_Model
    //guardarGrupos()
    //eliminarGrupos()
    //getGrupos()
    //getGruposPendientes()
    //existeGrupo()

    public function getCategorias($categoria = FALSE){
        if ($categoria === FALSE){
            $this->db->select('id_cat, nombre_categoria, CASE WHEN (nueva_categoria = 1) THEN \'true\' WHEN (nueva_categoria = 0) THEN \'false\' END AS nueva_categoria, 
                nombre_subcategoria, id_subcategoria, texto_informativo_categoria, publicidad_categoria, filtros_categoria,
                imagen_categoria.archivo_original AS imagen_categoria_original,
                imagen_categoria.archivo_large AS imagen_categoria_large,
                imagen_categoria.archivo_medium AS imagen_categoria_medium,
                imagen_categoria.archivo_small AS imagen_categoria_small,
                imagen_banner.archivo_original AS imagen_banner_original,
                imagen_banner.archivo_large AS imagen_banner_large,
                imagen_banner.archivo_medium AS imagen_banner_medium,
                imagen_banner.archivo_small AS imagen_banner_small
            ');
            $this->db->from($this->table);
            $this->db->join('categorias_subcategorias', 'categorias.id_cat = categorias_subcategorias.categorias_id_cat', "left");
            $this->db->join('subcategorias', 'subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria', "left");
            $this->db->join('archivos_vista AS imagen_categoria', 'imagen_categoria.id_file = categorias.id_file_img_home_categoria', "left");
            $this->db->join('archivos_vista AS imagen_banner', 'imagen_banner.id_file = categorias.id_file_img_banner_categoria', "left");
            $this->db->order_by("categorias.orden_categoria");
            //$this->db->order_by("categorias.nombre_categoria");
            //$this->db->order_by("subcategorias.nombre_subcategoria");
            $query = $this->db->get();  
            return $query->result_array();
        }   

// select id_cat, nombre_categoria, CASE WHEN (nueva_categoria = 1) THEN 'true' WHEN (nueva_categoria = 0) THEN 'false' END AS nueva_categoria, nombre_subcategoria, id_subcategoria, 
// imagen_categoria.archivo_original AS imagen_categoria_original,
// imagen_categoria.archivo_large AS imagen_categoria_large,
// imagen_categoria.archivo_medium AS imagen_categoria_medium,
// imagen_categoria.archivo_small AS imagen_categoria_small,
// imagen_banner.archivo_original AS imagen_banner_original,
// imagen_banner.archivo_large AS imagen_banner_large,
// imagen_banner.archivo_medium AS imagen_banner_medium,
// imagen_banner.archivo_small AS imagen_banner_small
// from categorias
// left join categorias_subcategorias on categorias.id_cat = categorias_subcategorias.categorias_id_cat
// left join subcategorias on subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria
// left join archivos_vista AS imagen_categoria on imagen_categoria.id_file = categorias.id_file_img_home_categoria
// left join archivos_vista AS imagen_banner ON imagen_banner.id_file = categorias.id_file_img_banner_categoria
// order by categorias.nombre_categoria, subcategorias.nombre_subcategoria;

        $query = $this->db->get_where('nombre', array('nombre' => $categoria));
        return $query->row_array();
    }

    public function getCategoriasConSubcategorias(){
        $categorias = $this->getCategorias();
        function anadirCategoria(&$output, $categoria, $subcategorias){
            $output[] = array(
                "idCategoria" => $categoria["id_cat"],
                "nombreCategoria" => $categoria["nombre_categoria"],
                "nuevaCategoria" => ($categoria["nueva_categoria"] == "true"),
                "imagenCategoriaOriginal" => $categoria["imagen_categoria_original"],
                "imagenCategoriaLarge" => $categoria["imagen_categoria_large"],
                "imagenCategoriaMedium" => $categoria["imagen_categoria_medium"],
                "imagenCategoriaSmall" => $categoria["imagen_categoria_small"],
                "imagenBannerOriginal" => $categoria["imagen_banner_original"],
                "imagenBannerLarge" => $categoria["imagen_banner_large"],
                "imagenBannerMedium" => $categoria["imagen_banner_medium"],
                "imagenBannerSmall" => $categoria["imagen_banner_small"],
                "informacionCategoria" => $categoria["texto_informativo_categoria"], 
                "infoBannerCategoria" => $categoria["publicidad_categoria"], 
                "filtros_categoria" => json_decode($categoria["filtros_categoria"]),
                "subcategorias" => $subcategorias,
            );
        }
        function anadirSubcategoria(&$arraySubcategorias, $categoriaConSubcategoria){
            if($categoriaConSubcategoria["id_subcategoria"] == null && $categoriaConSubcategoria["nombre_subcategoria"] == null){
                return;
            }
            $arraySubcategorias[] = array(
                "idSubcategoria" => $categoriaConSubcategoria["id_subcategoria"],
                "nombreSubcategoria" => $categoriaConSubcategoria["nombre_subcategoria"],
            );
        }
		for ($i=0, $categoriaPasada = null, $subcategorias = [], $outputCategorias = []; $i < count($categorias); $i++) { 
			$categoria = $categorias[$i];
			if($categoriaPasada == null || $categoriaPasada["nombre_categoria"] == $categoria["nombre_categoria"]){
                anadirSubcategoria($subcategorias, $categoria);
			}else{
				anadirCategoria($outputCategorias, $categoriaPasada, $subcategorias);
				$subcategorias = [];
				anadirSubcategoria($subcategorias, $categoria);
            }

            $categoriaPasada = $categoria;

            if($i+1 == count($categorias)){
                anadirCategoria($outputCategorias, $categoriaPasada, $subcategorias);
            }
			
		}
		return $outputCategorias;
    }

    public function actualizarCategoria($data, $id){
        $this->update($id, $data);
    }

    public function existeCategoria($categoria){
        $nombreCategoria = ucwords(mb_strtolower($categoria["nombre"], 'UTF-8'));
        $idCat = $categoria["id"];
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("categorias.nombre_categoria = '$nombreCategoria' AND categorias.id_cat = $idCat");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function existeCategoriaNombre($categoria){
        $nombreCategoria = ucwords(mb_strtolower($categoria["nombre"], 'UTF-8'));
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where("categorias.nombre_categoria = '$nombreCategoria'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function subcategoriasPendientesACategoria($categoria, $subcategorias){
        $this->load->model('administracion/Subcategorias_Model');
        $existeCategoria = $this->existeCategoria($categoria);
        if($existeCategoria){
            $idCategoria = $categoria["id"];
            foreach ($subcategorias as $subcategoria) {
                if(!$this->Subcategorias_Model->existeSubcategoria($subcategoria)){
                    return false;
                }else{
                    $data[] = array(
                            'categorias_id_cat' => $idCategoria,
                            'subcategorias_id_subcategoria' => $subcategoria["idSubcategoria"]
                    );
                }
            }
            $resultado = $this->db->insert_batch('categorias_subcategorias', $data);
            return $resultado === FALSE || $resultado <= 0 ? FALSE : TRUE;
        }else{
            return false;
        }
    }

    public function subcategoriaCategoriaAsubcategoriasPendientes($categoria, $subcategorias){
        $this->load->model('administracion/Subcategorias_Model');
        $existeCategoria = $this->existeCategoria($categoria);
        if($existeCategoria){
            $idCategoria = $categoria["id"];
            foreach ($subcategorias as $subcategoria) {
                if(!$this->Subcategorias_Model->existeSubcategoria($subcategoria)){
                    return false;
                }
            }
            $this->db->trans_start();
                foreach ($subcategorias as $subcategoria) {
                    $data = array(
                        'categorias_id_cat' => $idCategoria,
                        'subcategorias_id_subcategoria' => $subcategoria["idSubcategoria"]
                    );
                    $resultado = $this->db->delete('categorias_subcategorias', $data);
                    if(!$resultado){
                        break;
                    }
                }
            $this->db->trans_complete();
            return $resultado === FALSE ? FALSE : TRUE;
        }else{
            return false;
        }
    }

    public function subcategoriaCategoriaAsubcategoriaCategoria($categoriaOrigen, $categoriaDestino, $subcategorias){
        $this->load->model('administracion/Subcategorias_Model');
        
        $existeCategoriaOrigen = $this->existeCategoria($categoriaOrigen);
        $existeCategoriaDestino = $this->existeCategoria($categoriaDestino);

        if($existeCategoriaOrigen && $existeCategoriaDestino){
            $idCategoriaOrigen = $categoriaOrigen["id"];
            $idCategoriaDestino = $categoriaDestino["id"];

            foreach ($subcategorias as $subcategoria) {
                if(!$this->Subcategorias_Model->existeSubcategoria($subcategoria)){
                    return false;
                }
            }

            $this->db->trans_start();
                foreach ($subcategorias as $subcategoria) {
                    $data = array(
                        'categorias_id_cat' => $idCategoriaOrigen,
                        'subcategorias_id_subcategoria' => $subcategoria["idSubcategoria"]
                    );
                    $resultado = $this->db->delete('categorias_subcategorias', $data);
                    if(!$resultado){
                        break;
                    }
                }
                if($resultado){
                    foreach ($subcategorias as $subcategoria) {
                        $data2[] = array(
                            'categorias_id_cat' => $idCategoriaDestino,
                            'subcategorias_id_subcategoria' => $subcategoria["idSubcategoria"]
                        );
                    }
                    $resultado = $this->db->insert_batch('categorias_subcategorias', $data2);
                }
            $this->db->trans_complete();
            return $resultado === FALSE || $resultado <= 0  ? FALSE : TRUE;
        }else{
            return false;
        }
    }

    public function nuevaCategoria($categoria){
        $data = array(
            'nombre_categoria' => ucwords(mb_strtolower($categoria["nombre"], 'UTF-8')),
        );
        $resultado = $this->db->insert($this->table, $data);
        
        return $resultado;
    }

    public function eliminarCategoria($categoria){
        $existeCategoria = $this->existeCategoria($categoria);
        if(!$existeCategoria){
            return false;
        }

        $this->db->where('categorias_id_cat', $categoria["id"]);
        $resultado = $this->db->delete('categorias_subcategorias');
        
        $this->db->where('id_cat', $categoria["id"]);
        $resultado = $this->db->delete($this->table);

        if($resultado && $this->count() <= 0){
            $this->db->query("ALTER TABLE categorias AUTO_INCREMENT =  1");
        }

        return $resultado;
    }

    public function getBannerCategoria($id){
        $datos = array(
            "id" => $id,
        );
        $row = $this->getCategoria($datos);
        if(is_null($row["id_file_img_banner_categoria"])){
            return null;
        }
        return $this->Archivos_Model->obtenerArchivoPorId($row["id_file_img_banner_categoria"]);
    }

    public function getImagenHomeCategoria($id){
        $datos = array(
            "id" => $id,
        );
        $row = $this->getCategoria($datos);
        if(is_null($row["id_file_img_home_categoria"])){
            return null;
        }
        return $this->Archivos_Model->obtenerArchivoPorId($row["id_file_img_home_categoria"]);
    }

    public function getCategoria($categoria){
        // $data = array(
        //     'nombre_categoria' => $categoria["nombre"],
        // );
        if(isset($categoria["nombre"])){
            $data['nombre_categoria'] = $categoria["nombre"];
        }

        if(isset($categoria["id"])){
            $data['id_cat'] = $categoria["id"];
        }

        $query = $this->db->get_where($this->table, $data);
        $row = $query->row_array();
        return $row;
    }

    public function obtenerListaCategoriasParaHome(){
        $query = $this->db->query("select categorias.id_cat AS id_cat, categorias.nombre_categoria AS nombre_categoria, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_categoria
        from categorias join archivos on categorias.id_file_img_home_categoria = archivos.id_file order by orden_categoria asc;");
        return json_encode($query->result_array());
    }
}