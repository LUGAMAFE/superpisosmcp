<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subdist_Model extends MY_Model {

	public function __construct(){
        parent::__construct("subdist_page_data");	
    }

    public function get_banner(){
        $banners = $this->db
                        ->join('archivos as img', 'img.id_file = subdist_page_data.id_file_img_subdist', 'left')
                        ->get('subdist_page_data');

        $result = $banners->result();
        return $result;
    }

    public function guardarImagenSubdist($data){
        if($this->db->count_all('subdist_page_data') > 0){
            $this->db->empty_table('subdist_page_data'); 
        }
        
        foreach ($data as $key => $value ) {
            $dato = ["id_file_img_subdist" => $value];
            $this->db->insert("subdist_page_data", $dato);
        }
    }

    public function obtenerImagenSubdist(){
        $this->db->select('*');
        $this->db->from('subdist_page_data');
        $this->db->join('archivos as img', 'img.id_file = subdist_page_data.id_file_img_subdist', 'left');
        $query = $this->db->get();  
        return $query->result_array();
    }

}    