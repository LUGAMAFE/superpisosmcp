<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Remates_Model extends MY_Model {

	public function __construct(){
        parent::__construct("remates_page_data");	
    }

    public function get_banner(){
        $banners = $this->db->join('archivos as img', 'img.id_file = remates_page_data.id_file_img_remates', 'left')->get('remates_page_data');
        $resultado = $banners->result();
        return (count($resultado) > 0 ) ? $resultado[0]->full_route_file : null;
    }

    public function guardarImagenRemates($data, $update){
        if($update){
            $this->db->where($this->llave_primaria, 1);
            $resultado = $this->db->update($this->table, $data);
        }else{
            $resultado = $this->db->insert($this->table, $data);
        }
        return $resultado;
    }

    public function obtenerImagenRemates(){
        return $this->getFirst();
    }
}    