<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cotizacion_Model extends MY_Model {

	public function __construct(){
        parent::__construct("cotizacion_page_data");	
    }

    public function get_banner(){
        $banners = $this->db
                        ->join('archivos as img', 'img.id_file = cotizacion_page_data.id_file_img_cotizacion', 'left')
                        ->get('cotizacion_page_data');

        $result = $banners->result();
        return $result;
    }

    public function guardarImagenCotizacion($data){
        if($this->db->count_all('cotizacion_page_data') > 0){
            $this->db->empty_table('cotizacion_page_data'); 
        }
        
        foreach ($data as $key => $value ) {
            $dato = ["id_file_img_cotizacion" => $value];
            $this->db->insert("cotizacion_page_data", $dato);
        }
    }

    public function obtenerImagenCotizacion(){
        $this->db->select('*');
        $this->db->from('cotizacion_page_data');
        $this->db->join('archivos as img', 'img.id_file = cotizacion_page_data.id_file_img_cotizacion', 'left');
        $query = $this->db->get();  
        return $query->result_array();
    }

}    