<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ventana_Model extends MY_Model {

	public function __construct(){
        parent::__construct("ventana_page_data");	
    }

    public function get_banner(){
        $banners = $this->db
                        ->join('archivos as img', 'img.id_file = ventana_page_data.id_file_img_ventana', 'left')
                        ->get('ventana_page_data');

        $result = $banners->result();
        return $result;
    }

    public function guardarImagenesVentana($data, $jsonInputExtras){
        if($this->db->count_all('ventana_page_data') > 0){
            $this->db->empty_table('ventana_page_data'); 
        }
        
        foreach ($data as $key => $value ) {
            $dato = ["id_file_img_ventana" => $value,
                    "json_input_extras" => json_encode($jsonInputExtras[$key]),
                    "is_activo" => $jsonInputExtras[$key]["activo_ventana"]];
            $this->db->insert("ventana_page_data", $dato);
        }
    }

    public function obtenerImagenesVentana(){
        $this->db->select('*');
        $this->db->from('ventana_page_data');
        $this->db->join('archivos as img', 'img.id_file = ventana_page_data.id_file_img_ventana', 'left');
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function guardarIsActivo($data){
        $this->db->select('*');
        $this->db->from('ventana_page_info');
        $this->db->where('id_ventana', 1);
        $query = $this->db->get();  
        $datos = array(
            "is_activo" => $data,
        );
        if($result = $query->result_array() != null){
            $this->db->where('id_ventana', 1);
            $this->db->update('ventana_page_info', $datos);
        } else {
            $this->db->insert('ventana_page_info', $datos);
        }
    }

    public function obtenerIsActivo(){
        $this->db->select('is_activo');
        $this->db->from('ventana_page_info');
        $this->db->where('id_ventana', 1);
        $query = $this->db->get();
        $result = $query->result_array();
        if($result != null){
            return $result[0]["is_activo"];
        } else {
            return 0;
        }
    }

    public function obtenerImagenesActivasVentana(){
        $this->db->select('*');
        $this->db->from('ventana_page_data');
        $this->db->join('archivos as img', 'img.id_file = ventana_page_data.id_file_img_ventana', 'left');
        $this->db->where('ventana_page_data.is_activo', 1);
        $query = $this->db->get();  
        return $query->result_array();
    }
}    