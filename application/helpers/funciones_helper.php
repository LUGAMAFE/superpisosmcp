<?php 
	if ( ! function_exists('get_ip_address')){
		function get_ip_address(){
			foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
				if (array_key_exists($key, $_SERVER) === true){
					foreach (explode(',', $_SERVER[$key]) as $ip){
						$ip = trim($ip); // just to be safe

						if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
							return $ip;
						}
					}
				}
			}
		}
	}

	if( !function_exists("post_petition"))
	{
		function post_petition($url, $data){
			if(!is_string($url) || !is_array($data)){
				return null;
			} 
			// use key 'http' even if you send the request to https://...
			$options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data)
				)
			);
			$context  = stream_context_create($options);
			$result = @file_get_contents($url, false, $context);
			if ($result === FALSE) { 
				return null;
			}
			return $result;
		}
	}

	if ( ! function_exists('glob_recursive'))
	{
		// Does not support flag GLOB_BRACE
		
		function glob_recursive($pattern, $flags = 0)
		{
			$files = glob($pattern, $flags);
			
			foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
			{
				$files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
			}
			
			return $files;
		}
	}

	if ( ! function_exists('removeDir'))
	{
		/**
		 * Removes a directory and all files contained inside
		 * @param string $dir
		 */
		function removeDir($dir){
			foreach (scandir($dir) as $item){
				if ($item == "." || $item == "..")
					continue;

				if (is_dir($item)){
					removeDir($item);
				} else {
					unlink(join(DIRECTORY_SEPARATOR, array($dir, $item)));
				}

			}
			rmdir($dir);
		}
	}
    
	if ( ! function_exists('isWindows'))
	{
		/**
		 * Determines is the OS is Windows or not
		 *
		 * @return boolean
		 */
		function isWindows() {
			$isWin = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
			return $isWin;
		}
	}

	if ( ! function_exists('isInaccessible'))
	{
		/**
		 * Determines whether a directory can be accessed.
		 *
		 * is_executable() is not reliable on Windows prior PHP 5.0.0
		 *  (http://www.php.net/manual/en/function.is-executable.php)
		 * The following tests if the current OS is Windows and if so, merely
		 * checks if the folder is writable;
		 * otherwise, it checks additionally for executable status (like before).
		 *
		 * @param string $directory The target directory to test access
		 */
		function isInaccessible($directory) {
			$isWin = isWindows();
			$folderInaccessible = ($isWin) ? !is_writable($directory) : ( !is_writable($directory) && !is_executable($directory) );
			return $folderInaccessible;
		}
	}

	if ( ! function_exists('redirect_back'))
	{
		function redirect_back()
		{
			if(isset($_SERVER['HTTP_REFERER']))
			{
				header('Location: '.$_SERVER['HTTP_REFERER']);
			}
			else
			{
				header('Location: http://'.$_SERVER['SERVER_NAME']);
			}
			exit;
		}
	}
	
	if(! function_exists('get_request_method')) 
	{
		// This will retrieve the "intended" request method.  Normally, this is the
        // actual method of the request.  Sometimes, though, the intended request method
        // must be hidden in the parameters of the request.  For example, when attempting to
        // delete a file using a POST request. In that case, "DELETE" will be sent along with
        // the request in a "_method" parameter.
        function get_request_method() {
            global $HTTP_RAW_POST_DATA;

            if(isset($HTTP_RAW_POST_DATA)) {
                parse_str($HTTP_RAW_POST_DATA, $_POST);
            }

            if (isset($_POST["_method"]) && $_POST["_method"] != null) {
                return $_POST["_method"];
            }

            return $_SERVER["REQUEST_METHOD"];
        }
	}

	if(! function_exists('url_link')) 
	{
		function url_link($uri = '', $protocol = NULL)
		{
			$ci = get_instance();
			$url = '';
			if($ci->session->userdata('idioma') == 'en') {
				$url = 'en/';
			} else {
				$url = 'es/';
			}
			return $ci->config->site_url($url.$uri, $protocol);
		}
	}

	if(!function_exists('active')) {
		function active($a, $b)  
		{
			$result = '';
			if($a == $b) {
				$result = 'class="active"';
			}
			return $result;
		}
	}

	if(!function_exists('active_idioma')) {
		function active_idioma($a, $b)  
		{
			$b = (!empty($b)) ? $b : 'es';
			$result = '';
			if($a == $b) {
				$result = 'class="active"';
			}
			echo $result;
		}
	}

	if(!function_exists('print_m')) {
		function print_m($array) {
			echo '<pre>';
			print_r($array);
			echo '</pre>';
		}
	}

	if(!function_exists('shuffle_assoc')) {
		function shuffle_assoc($list) { 
			if (!is_array($list)) return $list; 
			$keys = array_keys($list); 
			shuffle($keys); 
			$random = array(); 
			foreach ($keys as $key) {
				$random[$key] = $list[$key];
			} 
			return $random; 
		} 
	}

	if(!function_exists('url_img')) {
		function url_img($path = '', $file_name = '', $ext = '')  
		{
			if(!empty($path)) {
				$foto = $path.'/'.$file_name.$ext;
				$foto = str_replace('//', '/', $foto);
				$foto = str_replace('./', '/', $foto);
				$foto = site_url($foto);
			return $foto;
			} else {
				return site_url('assets/img/default.png');
			}
			
		}
	}

	if (! function_exists('create_slug'))
	{
		function create_slug($slug)
		{
			return strtolower(url_title(convert_accented_characters($slug), '-', TRUE));
		}
	}

	if(!function_exists("curl_get"))
	{
		function curl_get($url) {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, True);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
			$return = curl_exec($curl);
			curl_close($curl);
			return $return;
		}
	}

	if ( ! function_exists('validarSubcategorias')){
		function validarSubcategorias(){
            $CI = get_instance();
            //$subcategoriasWeb = @file_get_contents('http://slim.test/subcategorias');
            $subcategoriasWeb = @file_get_contents('http://189.202.21.252:3600/slim/public/subcategorias');
            $mensaje = array(
                "error" => FALSE,
            );;
            if($subcategoriasWeb != FALSE){
                $subcategoriasWeb = json_decode($subcategoriasWeb, true);
                if($subcategoriasWeb["error"] != TRUE){
                    $subcategorias = $subcategoriasWeb["resultado"];
                    $subcategorias = array_map(function($val) {
                        $val["nombre_subcategoria"] = ucwords($val["nombre_subcategoria"]);
                        return $val;
                    },$subcategorias); 

                    $subcategoriasBase = $CI->Subcategorias_Model->getSubcategorias();
                    $subcategoriasWebService = array_column($subcategorias, 'nombre_subcategoria');
                    $subcategoriasLocal = array_column($subcategoriasBase, 'nombre_subcategoria');
                    $subcategoriasAgregar = array_diff($subcategoriasWebService, $subcategoriasLocal);
                    
                    $CI->Subcategorias_Model->guardarSubcategorias($subcategoriasAgregar);
                    $subcategoriasEliminar = array_diff($subcategoriasLocal, $subcategoriasWebService);
                    $CI->Subcategorias_Model->eliminarSubcategorias($subcategoriasEliminar);
                    $numNuevos = count($subcategoriasAgregar);
                    if($numNuevos > 0){
                        $mensaje = array(
                            "error" => TRUE,
                            "mensaje" => "Hay $numNuevos nuevas subcategorias por asignar a alguna categoria.",
                            "title" => "Dirigete a la pestaña de categorias para asignarlos.",
                            "type" => "warning",
                        );
                    }
                }
            }else{
                $mensaje = array(
                    "error" => TRUE,
                    "mensaje" => "Imposible Actualizar Las Subcategorias desde el WebService",
                    "title" => "Puedes Seguir Trabajando y Recargar la Pagina Luego",
                    "type" => "error",
                );
            }
            return $mensaje;
		}
    }
?>