<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Admin Pop-up</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/pagina") ?>">Contenido Pagina</a></li>
            <li class="breadcrumb-item active">Admin Pop-up</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Editar Pop-up</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
                <div class="card-body">
                  <div class="row justify-content-center">
                      <div class="col-12">

                          <div class="form-group">
                              <div class="form-group checkbox-options">
                                <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" name="all-activo-ventana" id="all-activo-ventana" 
                                <?php if($is_activo==1){
                                  echo("checked");
                                 } ?>>
                                <label class="custom-control-label" for="all-activo-ventana">Activar/Desactivar Pop-up</label>
                                </div>
                              </div>
                              <label>Imagen Pop-up <small>Tamaño: 724 x 724 pixeles</small></label>
                              <div class="upload-area multiple requerido" id="imagen-ventana-informativa">
                              </div>
                          </div>

                      </div>   
                  </div>   
                </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="<?= site_url("administracion/pagina") ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-primary float-right">Guardar</div>
              </div>
            </form>
            <div class="overlay">
              <div class="wrap">
                <i class="loader rotating-infinite fas fa-sync-alt"></i>
              </div>
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>