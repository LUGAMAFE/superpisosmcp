<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Administración Producto <?= $id ?></h1>
          <h3><?= $name ?></h3>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/inventario") ?>">Inventario y Productos</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/inventario/productos") ?>">Productos</a></li>
            <li class="breadcrumb-item active">Administración Producto <?= $id ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <!-- form start -->
        <form id="form" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form" class="row justify-content-center">

            <div class="col-9 col-md-10">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6">
                    <div class="card card-dark">
                        <div class="card-header">
                            <h3 class="card-title">Datos Basicos</h3>
                        </div>
                        <!-- /.card-header --> 
                        <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Nombre Producto</label>
                                    <input type="text" class="form-control" name="nombre" placeholder="Nombre del Producto.." required value="<?= isset($datos["nombre_producto"]) ? $datos["nombre_producto"] : ""; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Imagenes del Producto<small>Tamaño minimo: 200 x 200 pixeles</small></label>
                                    <div class="upload-area img-productos requerido" id="imagenes-producto"></div>
                                </div>
                            </div>   
                        </div>   
                        </div>
                        <div class="overlay">
                            <i class="loader rotating-infinite fas fa-sync-alt"></i>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    </div>
                    <div class="col-12 col-md-6">
                    <div class="card card-dark">
                        <div class="card-header">
                            <h3 class="card-title">Información Producto</h3>
                        </div>
                        <!-- /.card-header --> 
                        <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="input-producto-mostrar" id="input-producto-mostrar" <?=$datos["visible_prod"] === "1" ? 'checked' : '';?>>
                                    <label class="custom-control-label" for="input-producto-mostrar">Mostrar Producto</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="input-producto-rey" id="input-producto-rey" <?=$datos["producto_rey"] === "1" ? 'checked' : '';?>>
                                    <label class="custom-control-label" for="input-producto-rey">Producto del Rey</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="input-producto-mas-vendido" id="input-producto-mas-vendido" <?=$datos["producto_mas_vendido"] === "1" ? 'checked' : '';?>>
                                    <label class="custom-control-label" for="input-producto-mas-vendido">Producto Más Vendido</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="input-producto-descuento" id="input-producto-descuento" <?=$datos["descuento_activado_producto"] === "1" ? 'checked' : '';?>>
                                    <label class="custom-control-label" for="input-producto-descuento">Producto En Descuento</label>
                                    </div>
                                    <input type="text" class="form-control" name="descuento" placeholder="Descuento del Producto.." style="display: none;" value="<?= isset($datos["descuento_producto"]) ? $datos["descuento_producto"] : ""; ?>">
                                </div>
                                
                                <div class="form-group">
                                    <label>Descripción Producto</label>
                                    <textarea class="form-control" rows="3" name="descripcion" placeholder="Descripcion del Producto..." required><?= isset($datos["descripcion_producto"]) ? $datos["descripcion_producto"] : ""; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Información Extra Producto</label>
                                    <textarea class="form-control" rows="3" name="informacion" placeholder="Informacion del Producto..."><?= isset($datos["informacion_extra_producto"]) ? $datos["informacion_extra_producto"] : ""; ?></textarea>
                                </div>
                                <div class="form-group mb-0">
                                    <label>Productos Relacionados</label>
                                    <div class="input-group">
                                        <input type="search" class="form-control" id="productos-asociados-input" placeholder="Buscar" autocomplete="false">
                                        <div class="input-group-append">
                                            <span class="btn btn-navbar input-group-text"><i class="fas fa-search"></i></span>
                                        </div>
                                    </div>
                                </div>  
                                <div class="lista-productos asociados-busqueda list-group" style="z-index: 10;">
                                </div>
                                <div class="lista-productos asociados-seleccionados list-group mt-3">
                                </div>                              
                            </div>   
                        </div>   
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay">
                            <i class="loader rotating-infinite fas fa-sync-alt"></i>
                        </div>
                    </div>
                    <!-- /.card -->
                    </div>
                    <?php if(isset($filtros)): ?>
                    <div class="col-12 inputs-filtros">
                    <div class="card card-dark">
                        <div class="card-header">
                            <h3 class="card-title">Filtros del Producto</h3>
                        </div>
                        <!-- /.card-header --> 
                        <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <?php foreach ($filtros->schema as $filtro) { 
                                    $idFiltro = $filtro->uuid?>
                                    <?php if($filtro->active): ?>
                                        <div class="form-group filter-item requerido" filterType=<?=($filtro->type);?>  id=<?=($idFiltro);?>>
                                            <label class="text-capitalize">
                                                <?=($filtro->name);?>
                                            </label>
                                        <?php if($filtro->type == "string"): ?>
                                            <input class="form-control form-control-sm" type="text" required
                                            maxlength=<?php if(isset($filtro->maxLength)): echo($filtro->maxLength); endif;?>
                                            minlength=<?php if(isset($filtro->minLength)): echo($filtro->minLength); endif;?> 
                                            value=<?= isset($filtrosPrevios->$idFiltro) ? $filtrosPrevios->$idFiltro : ""; ?>>
                                        <?php endif; ?>
                                        <?php if($filtro->type == "number"): ?>
                                            <input class="form-control form-control-sm" type="number" id="number-<?=($idFiltro);?>"
                                            min=<?php if(isset($filtro->minLength)): echo($filtro->minLength); endif;?>
                                            max=<?php if(isset($filtro->maxLength)): echo($filtro->maxLength); endif;?> required
                                            onchange="checkNumberValidity('#number-<?=($idFiltro);?>',<?=($filtro->isFloat);?>,<?=($filtro->numberOfDecimals);?>)"
                                            value=<?= isset($filtrosPrevios->$idFiltro) ? $filtrosPrevios->$idFiltro : ""; ?>>
                                            <div class="invalid-feedback">
                                                El número es requerido y debe tener máximo <?=($filtro->numberOfDecimals);?> decimales.
                                            </div>
                                            <script type="text/javascript">
                                                function checkNumberValidity(numberId, isFloat, numberOfDecimals){
                                                    if(isFloat){
                                                        let value =  parseFloat($(`${numberId}`).val());
                                                        let decimales = 0;
                                                        if (Math.floor(value) !== value){
                                                            decimales = value.toString().split(".")[1].length;
                                                        }
                                                        if(decimales>numberOfDecimals){
                                                            $(`${numberId}`).addClass('is-invalid');
                                                            return false;
                                                        }
                                                        $(`${numberId}`).removeClass('is-invalid');
                                                        return true;
                                                    }else{
                                                        return true;
                                                    }
                                                }
                                                $(document).ready(function() {
                                                    hasStep('#number-<?=($idFiltro);?>',<?=($filtro->numberOfDecimals);?>);
                                                });
                                            </script>
                                        <?php endif; ?>
                                        <?php if($filtro->type == "select"): ?>
                                            <div class="input-group input-group-sm justify-self-start">
                                                <select class="custom-select" id="inputGroupSelect02" required>
                                                <?php if(isset($filtrosPrevios->$idFiltro)):  ?>
                                                    <?php foreach ($filtro->enum as $valor) { 
                                                        if($valor == $filtrosPrevios->$idFiltro) { ?>
                                                        <option value="<?=($valor);?>" selected><?=($valor);?></option>
                                                        <?php } else { ?>
                                                        <option value="<?=($valor);?>"><?=($valor);?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php else: ?>
                                                    <option disabled><?=($filtro->name);?></option>
                                                    <?php foreach ($filtro->enum as $valor) { ?>
                                                        <option value="<?=($valor);?>"><?=($valor);?></option>
                                                    <?php } ?>
                                                <?php endif; ?>
                                                </select>
                                            </div>
                                        <?php endif; ?>
                                        <?php if($filtro->type == "radio"): ?>
                                            <div class="form-group">
                                            <?php if(isset($filtrosPrevios->$idFiltro)):  ?>
                                                <?php foreach ($filtro->enum as $valor) { 
                                                    if($valor == $filtrosPrevios->$idFiltro) { ?>
                                                        <div class="custom-control custom-switch">
                                                        <input type="radio" class="custom-control-input" name="radio-<?=($idFiltro);?>" id="radio-<?=($idFiltro);?>-<?=($valor);?>" 
                                                        value="<?=($valor);?>" checked="checked" required>
                                                        <label class="custom-control-label" for="radio-<?=($idFiltro);?>-<?=($valor);?>"><?=($valor);?></label>
                                                        </div>
                                                    <?php } else { ?>
                                                        <div class="custom-control custom-switch">
                                                        <input type="radio" class="custom-control-input" name="radio-<?=($idFiltro);?>" id="radio-<?=($idFiltro);?>-<?=($valor);?>" 
                                                        value="<?=($valor);?>" required>
                                                        <label class="custom-control-label" for="radio-<?=($idFiltro);?>-<?=($valor);?>"><?=($valor);?></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php else: ?>
                                                <?php foreach ($filtro->enum as $valor) { ?>
                                                    <div class="custom-control custom-switch">
                                                    <input type="radio" class="custom-control-input" name="radio-<?=($idFiltro);?>" id="radio-<?=($idFiltro);?>-<?=($valor);?>" 
                                                    value="<?=($valor);?>" required>
                                                    <label class="custom-control-label" for="radio-<?=($idFiltro);?>-<?=($valor);?>"><?=($valor);?></label>
                                                    </div>
                                                <?php } ?>
                                            <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if($filtro->type == "checkbox"): ?>
                                            <div class="form-group checkbox-options">
                                            <?php if(isset($filtrosPrevios->$idFiltro)):  ?>
                                                <?php foreach ($filtro->enum as $valor) { 
                                                    if(in_array($valor, $filtrosPrevios->$idFiltro)) { ?>
                                                        <div class="custom-control custom-switch">
                                                        <input type="checkbox" class="custom-control-input" name="checkbox-<?=($valor);?>" id="checkbox-<?=($valor);?>" 
                                                        value="<?=($valor);?>" checked="checked">
                                                        <label class="custom-control-label" for="checkbox-<?=($valor);?>"><?=($valor);?></label>
                                                        </div>
                                                    <?php } else { ?>
                                                        <div class="custom-control custom-switch">
                                                        <input type="checkbox" class="custom-control-input" name="checkbox-<?=($valor);?>" id="checkbox-<?=($valor);?>" 
                                                        value="<?=($valor);?>">
                                                        <label class="custom-control-label" for="checkbox-<?=($valor);?>"><?=($valor);?></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php else: ?>
                                                <?php foreach ($filtro->enum as $valor) { ?>
                                                    <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input" name="checkbox-<?=($valor);?>" id="checkbox-<?=($valor);?>" 
                                                    value="<?=($valor);?>" required>
                                                    <label class="custom-control-label" for="checkbox-<?=($valor);?>"><?=($valor);?></label>
                                                    </div>
                                                <?php } ?>
                                            <?php endif; ?>
                                            </div>
                                            <script type="text/javascript">
                                               $(function(){
                                                    var requiredCheckboxes = $('.checkbox-options :checkbox');
                                                    if(!requiredCheckboxes.is(':checked')) {
                                                    requiredCheckboxes[0].setCustomValidity("Seleccione al menos una de estas opciones");
                                                    }
                                                    requiredCheckboxes.change(function(){
                                                        if(requiredCheckboxes.is(':checked')) {
                                                            requiredCheckboxes.removeAttr('required');
                                                            requiredCheckboxes[0].setCustomValidity("");
                                                        } else {
                                                            requiredCheckboxes.attr('required', 'required');
                                                            requiredCheckboxes[0].setCustomValidity("Seleccione al menos una de estas opciones");
                                                        }
                                                    });
                                                });
                                            </script>
                                        <?php endif; ?>
                                        <?php if($filtro->type == "color"): ?>
                                             <div class="form-group" style="max-height: 25vh">    
                                                <?php foreach ($filtro->enum as $valor) { ?>
                                                <?php if(isset($filtrosPrevios->$idFiltro)):  ?>
                                                    <?php if($valor->color == $filtrosPrevios->$idFiltro) { ?>
                                                            <div class="custom-control custom-switch">
                                                            <input type="radio" class="custom-control-input" name="radio-<?=($idFiltro);?>" id="radio-<?=($idFiltro);?>-<?=($valor->name);?>" 
                                                            value="<?=($valor->color);?>" checked="checked" required>
                                                            <label class="custom-control-label" for="radio-<?=($idFiltro);?>-<?=($valor->name);?>"><?=($valor->name);?></label>
                                                            <div style="background-color:<?=($valor->color);?>; width:2vh; height:2vh; display:inline-block;" 
                                                            id="color-<?=($idFiltro);?>-<?=($valor->name);?>"></div>
                                                            <script type="text/javascript">
                                                                $(document).ready(function() {
                                                                    hasBorder("#color-<?=($idFiltro);?>-<?=($valor->name);?>","<?=($valor->color);?>");
                                                                });
                                                            </script>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="custom-control custom-switch">
                                                            <input type="radio" class="custom-control-input" name="radio-<?=($idFiltro);?>" id="radio-<?=($idFiltro);?>-<?=($valor->name);?>" 
                                                            value="<?=($valor->color);?>" required>
                                                            <label class="custom-control-label" for="radio-<?=($idFiltro);?>-<?=($valor->name);?>"><?=($valor->name);?></label>
                                                            <div style="background-color:<?=($valor->color);?>; width:2vh; height:2vh; display:inline-block;" 
                                                            id="color-<?=($idFiltro);?>-<?=($valor->name);?>"></div>
                                                            <script type="text/javascript">
                                                                $(document).ready(function() {
                                                                    hasBorder("#color-<?=($idFiltro);?>-<?=($valor->name);?>","<?=($valor->color);?>");
                                                                });
                                                            </script>
                                                            </div>
                                                        <?php } ?>
                                                <?php else: ?>
                                                    <div class="custom-control custom-switch">
                                                    <input type="radio" class="custom-control-input" name="radio-<?=($idFiltro);?>" id="radio-<?=($idFiltro);?>-<?=($valor->name);?>" 
                                                    value="<?=($valor->color);?>" required>
                                                    <label class="custom-control-label" for="radio-<?=($idFiltro);?>-<?=($valor->name);?>"><?=($valor->name);?></label>
                                                    <div style="background-color:<?=($valor->color);?>; width:2vh; height:2vh; display:inline-block;" 
                                                    id="color-<?=($idFiltro);?>-<?=($valor->name);?>"></div>
                                                    <script type="text/javascript">
                                                        $(document).ready(function() {
                                                            hasBorder("#color-<?=($idFiltro);?>-<?=($valor->name);?>","<?=($valor->color);?>");
                                                        });
                                                    </script>
                                                    </div>
                                                <?php endif; ?>
                                                <?php } ?>
                                            </div>
                                        <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                <?php } ?>
                            </div>   
                        </div>   
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay">
                            <i class="loader rotating-infinite fas fa-sync-alt"></i>
                        </div>
                    </div>
                    <!-- /.card -->
                    </div>
                    <?php endif; ?>
                    <input type="hidden" name="datos-filtro" id="datos-filtro" value="">
                    <div class="col-12">
                    <div class="card card-dark">
                        <div class="card-header">
                            <h3 class="card-title">Información de la Tabla Producto</h3>
                        </div>
                        <!-- /.card-header --> 
                        <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="form-group bg-white requerido">
                                    <label>Especificaciones</label>
                                    <textarea class="summer" name="especificaciones" placeholder="Especificaciones del Producto...">
                                        <?php if(isset($datos["especificaciones_producto"])): echo($datos["especificaciones_producto"]); endif;?>
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label>Imagenes Usos<small>Tamaño minimo: 400 x 200 pixeles</small></label>
                                    <div class="upload-area img-usos" id="imagenes-uso"></div>
                                </div>
                            </div>   
                        </div>   
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay">
                            <i class="loader rotating-infinite fas fa-sync-alt"></i>
                        </div>
                    </div>
                    <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->    
            </div>
            <!--/.col-->
            <input type="hidden" name="id-producto" value="<?= $id ?>">

            <div class="col-2 col-md-2">
                <div>
                    <div class="info-box position-fixed">
                        <div class="info-box-content row">
                            <a href="<?= ($datos["nuevo_prod"] == "0") ? "producto/{$datos['id_mcp']}/{$datos['nombre_producto']}" : "";?>"  class="col-12 mb-2 btn btn-info btn-cancelar <?= ($datos["nuevo_prod"] == "0") ? "" : "disabled"; ?>"> Ver Producto</a>
                            <a href="<?= $cancelar ?>"  class="col-12 mb-2 btn btn-danger btn-cancelar">Cancelar</a>
                            <div id="submit-form" class="col-12 btn btn-primary float-right">Guardar</div>
                        </div>
                    </div>
                <!-- /.info-box -->
                </div>
            </div>
        </form>
        <!-- /.row FORM-->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script>
  const idProducto = "<?=$id?>" ; 
  const jsonProductos = <?= $productosListaParaRelacionados?>;
  const productosRelacionados = <?= $productosRelacionados?>;
</script>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>


