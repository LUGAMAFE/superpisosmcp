<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Productos <i class="nav-icon fas fa-barcode"></i></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/inventario") ?>">Inventario y Productos</a></li>
            <li class="breadcrumb-item active">Productos</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="card card-secondary">
                        <div class="card-header" style="marginbottom: 1rem;">
                            <h3 class="card-title">Productos</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h6 class="mb-2">Ultima Fecha Actualización: <span class="font-weight-bold"><?= $ultimaFecha ?></span></h6>
                                    <button type="button" id="actualizar-registros" class="btn mb-4 btn-md btn-block btn-danger">
                                        <span class="loader-actualizar spinner-border spinner-border-sm float-left" style="width: 1.5rem; height: 1.5rem;" role="status" aria-hidden="true"></span>
                                        <span class="texto">Actualizar registros de productos</span>
                                        <span class="loader-actualizar spinner-border spinner-border-sm float-right" style="width: 1.5rem; height: 1.5rem;" role="status" aria-hidden="true"></span>
                                    </button>
                                    <table id="table_id" class="table display table-bordered table-striped dt-responsive" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Articulo</th>
                                                <th>Nombre Articulo</th>
                                                <th>Categoria</th>
                                                <th>Subcategoria</th>
                                                <th>Mostrar</th>
                                                <th>Nuevo Producto</th>
                                                <th>Info</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Articulo</th>
                                                <th>Nombre Articulo</th>
                                                <th>Categoria</th>
                                                <th>Subcategoria</th>
                                                <th>Mostrar</th>
                                                <th>Nuevo Producto</th>
                                                <th>Info</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row --> 
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay">
                            <div class="wrap">
                                <i class="loader rotating-infinite fas fa-sync-alt"></i>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

            <!-- <div class="contenedorProductos">
                <div class="despliegueTablaProductos">
                    <div class="tablaProductos">
                        
                    </div>
                </div>
            </div> -->
</div>


<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>