<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2 align-items-center">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Modificar Categoria <i class="nav-icon fas fa-folder"></i>  <br> <?= $datos["nombre_categoria"] ?> </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right ">
          <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/inventario") ?>">Inventario y Productos</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/inventario/categorias") ?>">Categorias</a></li>
            <li class="breadcrumb-item active">Modificar Categoria <?= $id ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-12">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Editar Categoria: <?=$datos["nombre_categoria"] ?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
                <div class="card-body">
                  <div class="row justify-content-center">
                      <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>Imagen Home Categoria <small>Tamaño: 250 x 250 pixeles</small></label>
                                        <div class="upload-area individual requerido" id="imagen-home-categoria"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>Imagen Banner Categoria <small>Tamaño: 1920 x 350 pixeles</small></label>
                                        <div class="upload-area individual requerido" id="imagen-banner-categoria"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                  <div class="form-group">
                                      <label>Texto Informativo Categoria</label>
                                      <input type="text" class="form-control" name="texto-informativo" placeholder="Texto Informativo Categoria.." value="<?= isset($datos["texto_informativo_categoria"]) ? $datos["texto_informativo_categoria"] : ""; ?>">
                                  </div>
                                </div>
                                <div class="col-12">
                                  <div class="form-group">
                                      <label>Publicidad Categoria</label>
                                      <input type="text" class="form-control" name="publicidad" placeholder="Publicidad Categoria.." value="<?= isset($datos["publicidad_categoria"]) ? $datos["publicidad_categoria"] : ""; ?>">
                                  </div>
                                </div>
                                <input type="hidden" name="id-categoria" value="<?= $id ?>">
                            </div>
                            <div class="row">
                                <div class="col-12">
                                  <div class="filters py-1 px-1">
                                      <h3>Filtros de Categoria</h3>
                                      <div class="filters-builder">
                                          <input type="hidden" name="filters-config" value='<?= isset($datos["filtros_categoria"]) ? $datos["filtros_categoria"] : ""; ?>'>
                                          <!-- START FILTERS CONTAINER -->
                                          <div class="main-filter-container filter-group-container">
                                              <div class="filter-group-header">
                                                  <div class="filter-selector input-group input-group-sm justify-self-start">
                                                      <select class="custom-select">
                                                          <option disabled selected>Seleccionar Tipo de Filtro</option>
                                                          <option value="1">Texto</option>
                                                          <option value="2">Numérico</option>
                                                          <option value="3">Selección Despliegue</option>
                                                          <option value="4">Selección Menú</option>
                                                          <option value="5">Selección Múltiple</option>
                                                          <option value="6">Color</option>
                                                          <!-- <option value="7">Slider</option> -->
                                                      </select>
                                                      <div class="input-group-append">
                                                        <button type="button" class="btn btn-success btn-sm add"><i class="fas fa-plus"></i> Añadir Filtro</button>
                                                        <button type="button" class="btn btn-danger btn-sm cancel"><i class="fas fa-times"></i> Cancelar Añadir</button>
                                                      </div>
                                                  </div>
                                                  <div class="filter-example justify-self-start">
                                                    <span class="text-danger font-weight-bold">Ejemplo de Filtro: </span><span class="type-filter-name">Selección Despliegue</span>
                                                    <div class="ejemplo texto">
                                                        <input class="form-control form-control-sm" type="text" placeholder="Cualquier Letra">
                                                    </div>
                                                    <div class="ejemplo numerico">
                                                        <input class="form-control form-control-sm" type="number" placeholder="Solo Numeros">
                                                    </div>
                                                    <div class="ejemplo despliegue">
                                                        <div class="input-group input-group-sm justify-self-start">
                                                          <select class="custom-select" id="inputGroupSelect02">
                                                              <option disabled selected>Esto es un ejemplo</option>
                                                              <option value="1">Perro</option>
                                                              <option value="2">Gato</option>
                                                              <option value="3">Tortuga</option>
                                                              <option value="4">Leon</option>
                                                          </select>
                                                        </div>
                                                    </div>
                                                    <div class="ejemplo menu">
                                                        <div class="form-group">
                                                            <div class="label">Solo Puedes Seleccionar Una Opcion</div>
                                                            <div class="custom-control custom-switch">
                                                              <input type="radio" class="custom-control-input" name="radio-example" id="radio-example-1">
                                                              <label class="custom-control-label" for="radio-example-1">Perro</label>
                                                            </div>
                                                            <div class="custom-control custom-switch">
                                                              <input type="radio" class="custom-control-input" name="radio-example" id="radio-example-2">
                                                              <label class="custom-control-label" for="radio-example-2">Gato</label>
                                                            </div>
                                                            <div class="custom-control custom-switch">
                                                              <input type="radio" class="custom-control-input" name="radio-example" id="radio-example-3">
                                                              <label class="custom-control-label" for="radio-example-3">Tortuga</label>
                                                            </div>
                                                            <div class="custom-control custom-switch">
                                                              <input type="radio" class="custom-control-input" name="radio-example" id="radio-example-4">
                                                              <label class="custom-control-label" for="radio-example-4">Leon</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ejemplo multiple">
                                                        <div class="form-group">
                                                            <div class="label">Puedes Seleccionar Varias Opciones</div>
                                                            <div class="custom-control custom-switch">
                                                              <input type="checkbox" class="custom-control-input" name="check-example" id="check-example-1">
                                                              <label class="custom-control-label" for="check-example-1">Perro</label>
                                                            </div>
                                                            <div class="custom-control custom-switch">
                                                              <input type="checkbox" class="custom-control-input" name="check-example" id="check-example-2">
                                                              <label class="custom-control-label" for="check-example-2">Gato</label>
                                                            </div>
                                                            <div class="custom-control custom-switch">
                                                              <input type="checkbox" class="custom-control-input" name="check-example" id="check-example-3">
                                                              <label class="custom-control-label" for="check-example-3">Tortuga</label>
                                                            </div>
                                                            <div class="custom-control custom-switch">
                                                              <input type="checkbox" class="custom-control-input" name="check-example" id="check-example-4">
                                                              <label class="custom-control-label" for="check-example-4">Leon</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ejemplo color">
                                                        <div class="input-group input-group-sm mb-3 colorpicker-example colorpicker-element">
                                                            <input type="text" class="form-control form-control-sm" id="color-example" value="#fff">
                                                            <div class="input-group-append">
                                                              <span class="input-group-text" id="basic-addon2"><i class="fas fa-square"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                                                  <div class="filter-name-input hidden input-group input-group-sm justify-self-start">
                                                      <input class="form-control form-control-sm" type="text" placeholder="Nombrar el Filtro">
                                                      <div class="input-group-append">
                                                        <button type="button" class="btn btn-primary btn-sm continue">Continuar <i class="fas fa-arrow-right"></i></button>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="filter-group-body">
                                                  <div class="filters-list">
                                                  </div>
                                                <!-- END FILTER LIST -->
                                              </div>
                                              <!-- END FILTER GROUP BODY -->
                                          </div> 
                                          <!-- END FILTER GROUP -->
                                      </div>
                                      <!-- END FILTERS BUILDER -->
                                  </div>
                                </div>
                            </div>
                      </div>   
                  </div>   
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="<?= $cancelar ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-primary float-right">Guardar</div>
              </div>
            </form>
            <div class="overlay">
              <i class="loader rotating-infinite fas fa-sync-alt"></i>
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script>
  const idCategoria = <?=$id?> ; 
</script>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>
