<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Categorias <i class="nav-icon fas fa-folder"></i></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/inventario") ?>">Inventario y Productos</a></li>
            <li class="breadcrumb-item active">Categorias</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="card card-danger">
                        <div class="card-header categorias-dragger-titulo">
                            <h3 class="card-title text-center titulo-categorias-section">Categorias</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="maximize"><i class="fas fa-expand"></i></button>
                            </div>
                        </div>
                        <div class="contenedorCategorias">
                            <div class="arriba">
                                <div class="secciones">
                                    <div class="listaCategorias">
                                        <div class="contenedorAgregar">
                                            <div id="agregarCat"class="agregar">
                                                <input type="text" name="nuevaCategoria" id="nuevaCategoria" placeholder="Categoria Nueva">
                                                <div class="add">+</div>
                                            </div>
                                        </div> 
                                        <div id="contenedorCate" class="contenedorCate scrollbar-inner">
                                            <?php for ($i = 0; $i < count($_categorias); $i++): $categoria = $_categorias[$i] ?>
                                                <div class="categoria">
                                                    <div class="nombre">
                                                        <img class="handler svg" src="<?= site_url('assets/img/iconos/move-arrows.svg');?>" alt="">
                                                        <p class="nombreCategoria"><?=$categoria["nombreCategoria"]?></p>
                                                        <a href="#modalEditarCategoria"><img class="options svg" src="<?= site_url('assets/img/iconos/menu-bars.svg');?>" alt=""></a>
                                                    </div>
                                                    <ul id-categoria="<?=$categoria["idCategoria"]?>" class="grupos scrollbar-inner contenedor-grupos"> 
                                                        <?php foreach ($categoria["subcategorias"] as $subcategoria):
                                                            if($subcategoria["idSubcategoria"] !== NULL && $subcategoria["nombreSubcategoria"] !== NULL  ):?>
                                                            <li id-grupo="<?= $subcategoria["idSubcategoria"]; ?>" class="grupo"><img class="handler-grupo svg" src="<?= site_url('assets/img/iconos/six-dots.svg');?>" alt=""> <?= $subcategoria["nombreSubcategoria"]; ?> </li>
                                                            <?php endif;
                                                        endforeach;?>
                                                    </ul>
                                                </div>
                                            <?php endfor; ?>
                                        </div>
                                    </div>

                                    <div id="despliegueCategorias" class="despliegueCategorias">
                                        <?php for ($i = 0; $i < count($categoriasDesplegadas); $i++): $categoria = $categoriasDesplegadas[$i]?>
                                            <div class="categoria">
                                                <div class="nombre">
                                                    <img class="handler svg" src="<?= site_url('assets/img/iconos/move-arrows.svg');?>" alt="">
                                                    <p class="nombreCategoria"><?=$categoria["nombreCategoria"]?></p>
                                                    <a href="#modalEditarCategoria"><img class="options svg" src="<?= site_url('assets/img/iconos/menu-bars.svg');?>" alt=""></a>
                                                </div>
                                                <ul id-categoria="<?=$categoria["idCategoria"]?>" class="grupos scrollbar-inner contenedor-grupos"> 
                                                    <?php foreach ($categoria["subcategorias"] as $subcategoria):
                                                        if($subcategoria["idSubcategoria"] !== NULL && $subcategoria["nombreSubcategoria"] !== NULL  ):?>
                                                        <li id-grupo="<?= $subcategoria["idSubcategoria"]; ?>" class="grupo"><img class="handler-grupo svg" src="<?= site_url('assets/img/iconos/six-dots.svg');?>" alt=""> <?= $subcategoria["nombreSubcategoria"]; ?> </li>
                                                        <?php endif; 
                                                    endforeach;?>
                                                </ul>
                                            </div>
                                        <?php endfor; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="abajo">
                                <div class="contenedorCategoriasBasura">
                                    <p class="titulo">Eliminar</p>
                                    <div id="categoriasBasura" class="contenidoBasura">
                                    </div>
                                </div>
                                <div class="contenedorGrupos">
                                    <h3 class="titulo categorias-dragger-titulo">Subcategorias Pendientes a Asignar</h3>
                                    <ul id="listaGruposPendientes" class="grupos scrollbar-inner"> 
                                        <?php foreach ($subcategoriasPendientes as $subcategoria):?>
                                            <li id-grupo="<?= $subcategoria["id_subcategoria"]; ?>" class="grupo"><img class="handler-grupo svg" src="<?= site_url('assets/img/iconos/six-dots.svg');?>" alt=""> <?= $subcategoria["nombre_subcategoria"]; ?> </li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="overlay">
                            <i class="loader rotating-infinite fas fa-sync-alt"></i>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

            <!-- <div class="contenedorProductos">
                <div class="despliegueTablaProductos">
                    <div class="tablaProductos">
                        
                    </div>
                </div>
            </div> -->
</div>

<!-- Modal HTML embedded directly into document -->
<div id="modalEditarCategoria" class="modal" style="display:none;">
    <a class="closeModalCancelar" href="#" rel="modal:close">Cancelar</a>
    <p class="titulo">Categoria: <span id-categoria="" class="categoria"></span></p>
    <div class="opcion opcionesBanner">
        <p class="textoOpcion"> <span>Opción: </span> Cambiar Banner de Categoria</p>
        <input name="input-file-1" id="input-file-1"> 
    </div>
    <div class="opcion opcionesTextoCategoria">
        <p class="textoOpcion"> <span>Opción: </span> Cambiar Texto Informativo Categoria</p>
        <label>Texto Categoria:<input type="text" placeholder="Cambiar Texto"></label>
        
    </div>
</div>


<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>