<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard Home </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Home</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row d-flex justify-content-center justify-content-md-start">
        <div class="col-12 col-md-10 col-lg-6">
          <!-- small box -->
          <a href="<?= site_url("administracion/pagina") ?>" class="small-box bg-secondary">
            <div class="inner">
              <h3>Contenido Pagina</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-scroll"></i>
            </div>
            <div class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <!-- ./col -->
        <div class="col-12 col-md-10 col-lg-6">
          <!-- small box -->
          <a href="<?= site_url("administracion/inventario") ?>" class="small-box bg-secondary">
            <div class="inner">
              <h3>Inventario y Productos</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-boxes"></i>
            </div>
            <div class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?php echo $this->load->view('admin/utils/sweetAlerts', '', true); ?>