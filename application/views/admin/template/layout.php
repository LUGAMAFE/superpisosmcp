<?= doctype('html5'); ?> 
<html class="no-js" lang="es" itemscope itemtype="https://schema.org/LocalBusiness" ng-app="myApp">
    <head <?php if(isset($prefix)) { echo $prefix; } ?>>
        <!-- META -->
		<?= meta('charset', 'utf-8'); ?>
		<?= meta('viewport', 'width=device-width, initial-scale=1.0, user-scalable=no'); ?>
        <?= meta('x-ua-compatible', 'ie=edge', 'equiv'); ?>

        <!-- BASE -->
        <base href="<?= base_url();?>">
        
        <!-- PROPERTY -->
        <meta name="description" content="">
   		<meta name="author" content="">

        <!-- TITLE -->
        <title><?= $this->config->item('name_site');?> - <?= $title ?></title>

        <!-- PAGE ICON -->
        <?= link_tag(array('href' => 'assets/img/icon.png', 'rel' => 'shortcut icon')); ?>

        <!-- STYLES CSS -->
            <!-- Font Awesome -->
            <link rel="stylesheet" href="assets/adminLTE/plugins/fontawesome-free/css/all.min.css">
            <!-- Ionicons -->
            <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
            <!-- iCheck -->
            <link rel="stylesheet" href="assets/adminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
            <!-- overlayScrollbars -->
            <link rel="stylesheet" href="assets/adminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
            <!-- J Box -->
            <link href="assets/adminLTE/plugins/jBox/dist/jBox.all.min.css" rel="stylesheet">
            <!-- Sweet Alert 2 -->
            <link href="assets/adminLTE/plugins/sweetalert2/sweetalert2.min.css" rel="stylesheet">
            <!-- Fine Uploader -->
            <?= link_tag('assets/adminLTE/plugins/fine-uploader/fine-uploader-new.min.css'); ?>
            <?= link_tag('assets/adminLTE/plugins/fine-uploader/fine-uploader-gallery.min.css'); ?>
            <!-- Google Font: Source Sans Pro -->
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
            <!-- Theme style -->
            <link rel="stylesheet" href="assets/adminLTE/dist/css/adminlte.min.css">
            <!-- Bootstrap Color Picker -->
            <link rel="stylesheet" href="assets/adminLTE/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">

        <!-- /OWN STYLES CSS -->    
        <?= $_styles ?>
            <!-- Own CSS -->
            <?= link_tag('assets/css/adminlte.css'); ?>

        <!-- /STYLES CSS -->

        <!-- TOP SCRIPTS -->
        <script src="assets/js/modernizr.js"></script>
        <!-- jQuery -->
        <script src="assets/adminLTE/plugins/jquery/jquery-3.4.1.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="assets/adminLTE/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
        $.widget.bridge('uibutton', $.ui.button)
        </script>
        <!-- Bootstrap 4 -->
        <script src="assets/adminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script> const siteURL = "<?= current_url();?>/", 
                    baseURL = "<?= base_url();?>"; </script>
        <!-- /TOP SCRIPTS -->

    </head>
    <!-- BODY -->
    <body class="hold-transition fixed skin-yellow <?= $bodyclasses ?>">
            <!-- PAGE CONTENT -->
            <?= $navbar; ?>	
            <?= $content; ?>
            <?= $footer; ?>
            <!-- /PAGE CONTENT -->

        <!-- BOTTOM SCRIPTS -->
            <!-- lodash.js -->
            <script src="assets/adminLTE/plugins/lodash.js"></script>
            <!-- overlayScrollbars -->
            <script src="assets/adminLTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
            <!-- FastClick -->
            <script src="assets/adminLTE/plugins/fastclick/fastclick.js"></script>
            <!-- AdminLTE App -->
            <script src="assets/adminLTE/dist/js/adminlte.js"></script>
            <!-- J Box -->
            <script src="assets/adminLTE/plugins/jBox/dist/jBox.all.min.js"></script>
            <!-- Sweet Alert 2 -->
            <script src="assets/adminLTE/plugins/sweetalert2/sweetalert2.all.js"></script>
            <!-- Fine Uploader -->
            <script src="<?= site_url('assets/adminLTE/plugins/fine-uploader/fine-uploader.min.js'); ?>"></script>
            <!-- bootstrap color picker -->
            <script src="assets/adminLTE/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

            <script type="text/template" id="qq-template">
                <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Suelta Tus Imagenes Aqui" style="overflow-x: hidden;">
                <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
                </div>
                    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                        <span class="qq-upload-drop-area-text-selector"></span>
                    </div>
                    <div class="qq-upload-button-selector qq-upload-button">
                        <div>Subir Imagen(es)</div>
                    </div>
                    <span class="qq-drop-processing-selector qq-drop-processing">
                        <span>Procesando Archivos Seleccionados...</span>
                        <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                    </span>
                    <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
                        <li>
                            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                            <div class="qq-progress-bar-container-selector">
                                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                            </div>
                            <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                            <div class="qq-thumbnail-wrapper">
                                <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                            </div>
                            <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                            <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                                <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                                Reintentar
                            </button>

                            <div class="qq-file-info">
                                <div class="qq-file-name">
                                    <span class="qq-upload-file-selector qq-upload-file"></span>
                                    <span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>
                                </div>
                                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                                <span class="qq-upload-size-selector qq-upload-size"></span>
                                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                                    <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                                </button>
                                <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                                    <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                                </button>
                                <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                                    <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                                </button>
                            </div>
                        </li>
                    </ul>

                    <dialog class="qq-alert-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">Cerrar</button>
                        </div>
                    </dialog>

                    <dialog class="qq-confirm-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">No</button>
                            <button type="button" class="qq-ok-button-selector">Si</button>
                        </div>
                    </dialog>

                    <dialog class="qq-prompt-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <input type="text">
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">Cancelar</button>
                            <button type="button" class="qq-ok-button-selector">Ok</button>
                        </div>
                    </dialog>
                </div>
            </script>
            
            <!-- GLOBAL SHARED SCRIPTS -->
            <script src="assets/adminLTE/plugins/global.js"></script>    
        <?= $_scripts; ?>
        <!-- /BOTTOM SCRIPTS -->
    </body>
    <!-- /BODY -->
</html>
