<?php $urlSegments = $this->uri->segment_array(); $totalSegments = $this->uri->total_segments(); //var_dump($urlSegments, $totalSegments);?>
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-success navbar-dark border-bottom ">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block <?= ($totalSegments === 1 && $urlSegments[1] === "administracion") ? "active": ""; ?>">
        <a href="<?= site_url("administracion") ?>" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item">
        <a class="nav-link cerrar-sesion">
          <i class="fas fa-sign-out-alt"></i>
          Cerrar Sesión
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          <i class="fas fa-th-large"></i>
          Personalizar
        </a>
      </li>

    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-success elevation-4">
    <!-- Brand Logo -->
    <a href="#" target="_blank" class="p-0 brand-link navbar-light d-flex justify-content-center align-items-center">
      <img class="w-50" id="logo" src="assets/img/logo.svg">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center">
        <div class="image d-flex justify-content-center align-items-center">
          <i class="fas fa-user"></i>
        </div>
        <!-- <li class="nav-item">
            <i class="fas fa-user"></i>
        </li> -->
        <div class="info">
          <a class="d-block text-uppercase"><?= $this->session->userdata("username"); ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Contenido Pagina</li>
          <li class="nav-item has-treeview <?= ($totalSegments >= 2 && $urlSegments[2] === "pagina") ? "menu-open": ""; ?>">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Contenido Pagina
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="administracion/pagina/home" class="nav-link <?= ($totalSegments >= 3 && $urlSegments[3] === "home") ? "active": ""; ?>">
                <i class="nav-icon fas fa-home"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="administracion/pagina/remates" class="nav-link <?= ($totalSegments >= 3 && $urlSegments[3] === "remates") ? "active": ""; ?>">
                <i class="nav-icon fas fa-piggy-bank"></i>
                  <p>Remates</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="administracion/pagina/bolsa-trabajo" class="nav-link <?= ($totalSegments >= 3 && $urlSegments[3] === "bolsa-trabajo") ? "active": ""; ?>">
                <i class="nav-icon fas fa-briefcase"></i>
                  <p>Bolsa de Trabajo</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="administracion/pagina/ventana-info" class="nav-link <?= ($totalSegments >= 3 && $urlSegments[3] === "ventana-info") ? "active": ""; ?>">
                <i class="nav-icon fas fa-window-restore"></i>
                  <p>Pop-up</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="administracion/pagina/cotizacion" class="nav-link <?= ($totalSegments >= 3 && $urlSegments[3] === "cotizacion-info") ? "active": ""; ?>">
                <i class="nav-icon fas fa-file-invoice-dollar"></i>
                  <p>Cotización</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="administracion/pagina/subdistribucion" class="nav-link <?= ($totalSegments >= 3 && $urlSegments[3] === "subdist-info") ? "active": ""; ?>">
                <i class="nav-icon fas fa-bezier-curve"></i>
                  <p>Subdistribución</p>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="administracion/pagina/home" class="nav-link active">
                <i class="nav-icon fas fa-truck-moving"></i>
                  <p>Subdistribuidores</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="administracion/pagina/home" class="nav-link active">
                <i class="nav-icon fas fa-headset"></i>
                  <p>Contacto</p>
                </a>
              </li> -->
            </ul>
          </li>     
          <!-- Inventario y Productos -->
          <li class="nav-header">Inventario y Productos</li>
          <li class="nav-item has-treeview <?= ($totalSegments >= 2 && $urlSegments[2] === "inventario") ? "menu-open": ""; ?>">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Inventario y Productos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item active">
                <a href="administracion/inventario/productos" class="nav-link <?= ($totalSegments >= 3 && $urlSegments[3] === "productos") ? "active": ""; ?>">
                <i class="nav-icon fas fa-barcode"></i>
                  <p>Productos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="administracion/inventario/categorias" class="nav-link <?= ($totalSegments >= 3 && $urlSegments[3] === "categorias") ? "active": ""; ?>">
                <i class="nav-icon fas fa-folder"></i>
                  <p>Categorias</p>
                </a>
              </li>
            </ul>
          </li>  
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>