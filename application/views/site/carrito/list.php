<div class="carrito">
    <div class="titulo">
        <h3>Esta a Punto de Renovar</h3>
        <p>Algo Nuevo en su Espacio.</p>
    </div>
    <?php if(!empty($productos_carrito)):?>
        <div class="contenido">
            <div class="subitulo">
                <h4>Carrito de Cotización</h4>
            </div>
            <div class="grid">
                <div id="izqCarrito" class="izq">
                    <?php foreach($productos_carrito as $producto_carrito):  $id="'".$producto_carrito['id_mcp']."'"?>
                        <div class="producto">
                            <div class="contenido-producto">
                                <div class="articulo">
                                    <p class="header">Articulo</p>
                                    <div class="body">
                                        <div class="despliegue">
                                            <div class="imagen">
                                                <img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url($producto_carrito['full_route_file']);?>');" alt="">
                                            </div>
                                            <div class="detalles">
                                                <p class="nombre"><?=$producto_carrito['nombre_producto']?></p>
                                                <p class="desc"><?=$producto_carrito['descripcion_producto']?></p>
                                            </div>
                                        </div>
                                    </div>
								</div>
                                <div class="cantidad">
                                    <p class="header">Cantidad</p>
                                    <div class="body">
                                        <div class="sumadorProductos">
                                            <button class="menos">-</button>
                                            <input type="text" class="cantidad" value="<?= isset($cantidades_carrito->{$id}) ? $cantidades_carrito->{$id} : 1;?>">
                                            <button  class="mas">+</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="cancelar">
                                    <a href="#"><p class="header"><img src="<?= site_url('assets/img/iconos/right.svg');?>">Seguir Cotizando</p></a>
                                    <div class="body">
                                        <button class="btn-cancelar" id-prod="<?=$producto_carrito['id_mcp']?>">
                                            <img src="<?= site_url('assets/img/iconos/cancel-product.svg');?>">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <button class="descargar" data-fancybox="form-carrito" data-src="#form-carrito" href="javascript:;">Enviar Cotización</button>
                    <!--<a class="descargar" data-fancybox="form-carrito" data-src="#form-carrito" href="javascript:;">Enviar Cotización</a>-->
                </div>
                <div class="der" data-sticky-container>
                    <div class="sticky" data-sticky data-sticky-on="small" data-margin-top="8" data-anchor="izqCarrito">
                        <?php if(!empty($banner_carrito)): ?>
                            <img src="<?= site_url($banner_carrito[0]['full_route_file']);?>" alt="">
                        <?php else: ?>
                            <img src="<?= site_url('assets/img/banners/carrito/banner.png');?>" alt="">
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <form id="form-carrito" action="enviar-cotizacion" method="post" style="display: none; width: 100%; max-width: 660px;" class="">
        <h2 class="mb-3">
            Datos para cotización
        </h2>
        <div>
            <label class="text-capitalize">Nombre:</label>
            <input type="text" value="" name="nombre" class="form-control" placeholder="Nombre" required>
        </div>
        <div>
            <label class="text-capitalize">Apellidos:</label>
            <input type="text" value="" name="apellido" class="form-control" placeholder="Apellidos" required>
        </div>
        <div>
            <label class="text-capitalize">Correo Electronico:</label>
            <input type="email" value="" name="correo" class="form-control" placeholder="ejemplo@gant.com" required>
        </div>
        <div>
            <label class="text-capitalize">Teléfono de Contacto:</label>
            <input type="tel" pattern="[0-9]{10}" value="" name="telefono" class="form-control" placeholder="Número de 10 digitos" required>
        </div>
        <div>
            <label class="toggleButton">
                <input type="checkbox" class="custom-control-input" name="checkbox-carrito" id="checkbox-carrito" value="acepto" required>
                <div>
                    <svg viewBox="0 0 44 44">
                        <path d="M14,24 L21,31 L39.7428882,11.5937758 C35.2809627,6.53125861 30.0333333,4 24,4 C12.95,4 4,12.95 4,24 C4,35.05 12.95,44 24,44 C35.05,44 44,35.05 44,24 C44,19.3 42.5809627,15.1645919 39.7428882,11.5937758" transform="translate(-2.000000, -2.000000)"></path>
                    </svg>
                </div>
                <label class="custom-control-label" for="checkbox-carrito">¿Acepta enviar la cotización?</label>
            </label>
        </div>
        <input type="hidden" name="cantidad-real-producto" id="cantidad-real-producto">
        <div class="mb-0 text-right">
            <input type="submit" class="enviar button" value="Enviar">
        </div>
    </form>
</div>

<script>
	$(document).ready(function(){

        $(".btn-cancelar").click(function(){
            let productos = $(".producto");

            $(this).parent().parent().parent().parent().remove();
        });

        $(".sumadorProductos").each(function (index, element) {
            let sumador = element;
            let menos = $(sumador).find(".menos");
            let mas = $(sumador).find(".mas");
            let cantidad = $(sumador).find(".cantidad");

            let contador = 1,
                min = 1,
                max = 9999;


            //cambiarCantidad();

            function cambiarCantidad() {
                $(cantidad).val(contador);
                $("#cantidad-real-producto").attr('value', contador);
            };

            $(cantidad).on("change", function(){
                let val = $(cantidad).val();
                if(val < min){
                    contador = min;
                }else if(val > max){
                    contador = max;
                }else if(isNaN(val)){
                    contador = min;
                }else{
                    contador = val; 
                }
            
                $(cantidad).val(contador);
            });

            $(mas).on('click touch', function() {
                if(contador < max){
                    contador++;
                    cambiarCantidad();
                }
            });

            $(menos).on('click touch', function() {
                if(contador > min){
                    contador--;
                    cambiarCantidad();
                }
            });
            
        });

        /*$("#form-carrito").submit(function(e) {

            e.preventDefault();
            $.fancybox.close();

            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), 
                success: function(respuesta) {
                    console.log(respuesta);
                    result = respuesta;

                    new jBox('Notice', {
                        content: 'Su pedido de cotización ha sido enviado, un asesor lo contactará pronto', 
                        color: 'green',
                        showCountdown: true,
                        autoClose: 3000,
                        delayOnHover: true,
                        attributes: {y: 'bottom'}}).open();
                },
                error: function() {
                    result = "";
                    $.fancybox.close();
                    
                    new jBox('Notice', {
                        content: 'Ha ocurrido un error al enviar su pedido de cotización, intentelo nuevamente', 
                        color: 'red',
                        showCountdown: true,
                        autoClose: 3000,
                        delayOnHover: true,
                        attributes: {y: 'bottom'}}).open();
                }
            });
        });*/
	});
</script>


<?php if(!empty($productos_relacionados_carrito)):?>
	<div class="titulo-site">
		<h1 class="titulo">Productos Relacionados</h1>
	</div>

	<div class="row expanded lista-productos slick-productos">
	<?php foreach ($productos_relacionados_carrito as $producto):?>
		<div class="cont-productos">
			<div class="info-producto">
					<?php if($producto['descuento_activado'] == '1'): ?>
					<div class="descuento">
						<div class="img">
							<img src="<?= site_url('assets/img/iconos/descuento.svg'); ?>" alt="">
						</div>
					</div>
					<?php endif; ?>
					<?php if($producto['producto_rey'] == '1'): ?>
					<div class="corona">
						<div class="img">
							<img src="<?= site_url('assets/img/iconos/corona.png'); ?>" alt="">
						</div>
					</div>
					<?php endif; ?>
					<div class="img">
						<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url($producto['imagen_producto']);?>');" alt="">
						<?php if($producto['info_extra'] != ''): ?>
						<div class="info">
						<div class="img-info tooltip<?=$producto['id_mcp']?>" data-id="img-info<?=$producto['id_mcp']?>" data-jbox-content="<?=$producto['info_extra']?>">
							<?= file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
						</div>
						<script type="text/javascript">
							$(document).ready(function() {
								new jBox('Mouse', {
									attach: $("[data-id='img-info<?=$producto['id_mcp']?>']"),
									title: 'Información Adicional',
									getContent: 'data-jbox-content'
								});
							});
						</script>
						</div>
						<?php endif; ?>
					</div>
					<div class="texto clearfix">
						<h1 class="titulo"><?= $producto['nombre_producto'] ?></h1>
						<p>
						<?= $producto['desc_producto'] ?>
						</p>
						<span class="codigo">Art. <?= $producto['id_mcp'] ?></span>
					</div>
			</div>
			<div class="text-center btn">
				<a href="<?=base_url();?>producto/<?=$producto['id_mcp']?>" class="button expanded success">Ver detalles</a>
			</div>
		</div>
	<?php endforeach; ?>
	</div>
<?php endif; ?>

<script>
	$(document)
		.on('ready', function() {
			$('.slick-productos').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?= site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?= site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 4,
				slidesToScroll: 1,
				infinite: true,
				autoplay: true,
				arrows: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
				responsive: [
					{   breakpoint: 950,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true
						}
					},
					{	breakpoint: 700,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true,
							arrows: false,
						}
					},
					{	breakpoint: 400,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true,
							arrows: false,
						}
					},
				]
            });
            
            $('.btn-cancelar').click(function (){
			let url = '<?=base_url();?>carrito';
			let id = $(this).attr('id-prod');
            $.ajax({
                    async: false,
                    url: url,
                    data: {eliminar_carrito:true, id_producto:id},
                    type: 'POST',
                    success: function(respuesta) {
                        console.log(respuesta);
                        result = respuesta;
                        $('#cantidadCarrito').text(result);
                    },
                    error: function() {
                        result = "";
                    }
                });
		    });
		});
</script>