<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Cotización MCP</title>
        <style type="text/css">
        </style>
    </head>
    <body>

        <img src="<?= site_url('assets/img/logo-mini.png');?>" alt="" style="float: right">
        <h1>Cotización MCP</h1>

        <div>
            <h4>Datos de Contacto de Cliente: </h4>
            Nombre:  <?=$_POST['nombre']?><br>
            Apellidos:  <?=$_POST['apellido']?><br>
            E-mail:  <?=$_POST['correo']?><br>
            Teléfono: <?=$_POST['telefono']?>
        </div>

        <h4>Articulos: </h4>

        <table style="width:100%">
                <?php foreach($productos_carrito as $producto_carrito):  $id="'".$producto_carrito['id_mcp']."'"?>
                    <tr class="producto">
                                        <td style="width:17%">
                                            <div class="imagen">
                                                <img src="<?= site_url($producto_carrito['full_route_file']);?>" style="background-image: url('<?= site_url($producto_carrito['full_route_file']);?>');" alt="">
                                            </div>
                                        </td>
                                        <td style="vertical-align: top; border-collapse: collapse;">
                                            <div class="detalles">
                                                <p style="text-transform: uppercase;font-size: var(--small-text);font-style: oblique;font-weight: 600;text-align: left;
                                                margin-bottom: 0.5rem;">Nombre: <?=$producto_carrito['nombre_producto']?><br>
                                                Cantidad: <?php 
                                                if(isset($_POST['cantidad-real-producto']) && $_POST['cantidad-real-producto'] != ""):
                                                    echo $_POST['cantidad-real-producto'];
                                                elseif(isset($cantidades_carrito->{$id})): 
                                                    echo $cantidades_carrito->{$id};
                                                else: echo 1; endif;?></p>
                                            </div>
                                        </td>
                    </tr>
                <?php endforeach; ?>
        </table>

    </body>
</html>