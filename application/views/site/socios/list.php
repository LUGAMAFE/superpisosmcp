<div class="formaParte">
    <div class="contenido">
        <div class="izq">
            <h3>Forma Parte de Nuestro Equipo</h3>
            <p>Mándenos su información mediante este formulario y nos pondremos en contacto.</p>
            <form action="enviar-socio" method="post">
                    <input type="text" id="nombre" placeholder="Nombre" name="nombre" required>
                    <input type="text" id="apellido" placeholder="Apellido" name="apellido" required>
                    <input type="text" id="razon" placeholder="Razón social" name="razon" required>
                    <input type="text" id="ubicacion" placeholder="Ubicación" name="ubicacion" required>
                    <input type="email" id="email" placeholder="Correo Electrónico" name="email" required>
                    <input type="tel" pattern="[0-9]{10}" id="telefono2" placeholder="Número Celular" name="telefono" required>
                    <textarea name="mensaje" id="mensaje" placeholder="Mensaje" id="" rows="4"></textarea>
                    <input id="enviar" type="submit" value="Enviar">
            </form>
        </div>
        <div class="der">
            <?php if(!empty($banner_carrito)): ?>
                <img src="<?= site_url($banner_carrito[0]['full_route_file']);?>" alt="" class="yeah">
            <?php else: ?>
                <img src="<?= site_url('assets/img/griton.png');?>" alt="" class="yeah">
            <?php endif; ?>
        </div>
    </div>
</div>