<div class="row expanded busuqeda-productos">
	<div class="cont-titulo" style="margin-bottom:2rem">
		<h1 class="titulo">Su Busqueda: <?= $termino != "" ? $termino : "No se encontró término de busqueda"; ?></h1>
	</div>
	<div class="lista-productos filtro-producto">
		<div class="row expanded small-up-2 medium-up-3 large-up-4">
			<?php foreach ($productos as $producto): ?>
				<div class="column column-block" style="margin-bottom:2rem">
					<!-- Template Producto -->
					<div class="cont-productos">
						<div class="info-producto clearfix">
							<?php if($producto->descuento_activado == 1): ?>
								<div class="descuento">
									<div class="img">
										<img src="<?= site_url('assets/img/iconos/descuento.svg'); ?>" alt="">
									</div>
								</div>
							<?php endif; ?>
							<?php if($producto->producto_rey == 1): ?>
								<div class="corona">
									<div class="img">
										<img src="<?= site_url('assets/img/iconos/corona.png'); ?>" alt="">
									</div>
								</div>
							<?php endif; ?>
							<div class="img">
								<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url($producto->archivo_medium);?>');" alt="">
								<?php if($producto->info_extra != null): ?>
								<div class="info">
									<div class="img-info tooltip<?=$producto->id_mcp?>" data-id="img-info<?=$producto->id_mcp?>" data-jbox-content="<?=$producto->info_extra?>">
										<?= file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
									</div>
									<script type="text/javascript">
										$(document).ready(function() {
											new jBox('Mouse', {
												attach: $("[data-id='img-info<?=$producto->id_mcp?>']"),
												title: 'Información Adicional',
												getContent: 'data-jbox-content'
											});
										});
									</script>
								</div>
								<?php endif; ?>
							</div>
							<div class="texto clearfix">
								<h1 class="titulo"><?= $producto->nombre_producto ?></h1>
								<p>
								<?= $producto->desc_producto ?>
								</p>
								<span class="codigo">Art. <?= $producto->id_mcp ?></span>
							</div>
							<div class="text-center btn">
								<a href="<?=base_url();?>producto/<?=$producto->id_mcp?>" class="button expanded success">Ver detalles</a>
							</div>
						</div>
					</div>
					<!-- END Template Producto -->
				</div>
            <?php endforeach; ?>
            <?php if(empty($productos)): ?>
                <h3>No Se Encontraron Resultados Para Su Busqueda</h3>
            <?php endif; ?>
  		</div>
	</div>
</div>