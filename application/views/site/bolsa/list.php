<div class="formaParte">
    <div class="contenido">
        <div class="izq">
            <h3>Forma Parte de Nuestro Equipo</h3>
            <p>Mándenos su curriculum mediante este formulario y nos pondremos en contacto.</p>
            <form action="enviar-trabajo" method="post" enctype="multipart/form-data">
                    <input type="text" id="nombre" placeholder="Nombre" name="nombre" required>
                    <input type="text" id="apellido" placeholder="Apellido" name="apellido" required>
                    <input type="email" id="email" placeholder="Correo Electrónico" name="email" required>
                    <input type="tel" pattern="[0-9]{10}" id="telefono" name="telefono" placeholder="Número Celular" required>
                    <div class="archivo" id="archivo">
                        <span class="curiculum">
                            <input type="file" name="curiculum" id="curiculum" placeholder="Sube tu curriculum" accept="application/pdf" required>
                        </span>
                        <label for="curiculum">
                            <span>Sube tu curriculum</span>
                        </label>
                        <p class="textoSubido">archivo cargado</p>
                    </div>
                    <textarea name="" id="mensaje" placeholder="Mensaje" name="mensaje" id="" rows="4"></textarea>
                    <input id="enviar" type="submit" value="Enviar">
            </form>
        </div>
        <div class="der">
            <img src="<¡= site_url('assets/img/griton.png');?>" alt="" class="yeah">
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('input[type=file]').change(function(){
            var filename = $(this).val().split('\\').pop();
            var idname = $(this).attr('id');
            $('span.'+idname).next().find('span').html(filename);
            $('span.'+idname).next().next().show();
        });
    });
</script>