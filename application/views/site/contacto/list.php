<div class="row contacto">
	<div class="titulo-contacto">
		<h1 class="titulo">
			estamos ubicados en mérida
			<span>¡Le esperamooos!</span>
		</h1>
		<div class="row cont-contacto">
			<div class="large-13 columns">
				<h4 class="titulo-contacto">
					Sucursales 
					<img src="<?php echo site_url('assets/img/iconos/shop.svg');?>" alt="">
				</h4>
				<div class="row texto-contacto">
					<div class="large-8 columns">
						<h6 class="titulo-texto">CEDIS</h6>
						<a class="telefono" href="tel:+529999460315">
							<img src="<?php echo site_url('assets/img/iconos/telefono.png');?>" alt="">
							Tel: (999) 946 03 15
						</a>
						<p class="direccion">
							PERIFERICO SUR KM 1 A 300 MTS<br> 
							DE LA ACADEMIA DE POLICIAS <br>
							C.P 97255
						</p>
						<p>
							Mérida, Yucatán, México.
						</p>
						<a href="mailto:pisoventas.cedis@superpisosmcp.com">pisoventas.cedis@superpisosmcp.com</a>
						<a href="mailto:ventas.mayoreo@superpisosmcp.com">ventas.mayoreo@superpisosmcp.com</a>
					</div>
					<div class="large-8 columns">
						<h6 class="titulo-texto">Arco Dragones</h6>
						<a class="telefono" href="tel:+529999234802">
							<img src="<?php echo site_url('assets/img/iconos/telefono.png');?>" alt="">
							Tel: (999) 923 48 02
						</a>
						<p class="direccion">
							Calle 50 x 61 Col. Centro
						</p>
						<p>
							Mérida, Yucatán, México.
						</p>
						<a href="mailto:sucursaldragones@superpisosmcp.com">sucursaldragones@superpisosmcp.com</a>
					</div>
					<div class="large-8 columns">
						<h6 class="titulo-texto">60 NORTE</h6>
						<a class="telefono" href="tel:+529999277223">
							<img src="<?php echo site_url('assets/img/iconos/telefono.png');?>" alt="">
							Tel: (999) 927 72 23
						</a>
						<p class="direccion">
							Calle 60 X CIRCUITO COLONIAS
						</p>
						<p>
							Mérida, Yucatán, México.
						</p>
						<a href="mailto:suc60norte@superpisosmcp.com">suc60norte@superpisosmcp.com</a>
					</div>
				</div>
				<h4 class="titulo-contacto">
					envíanos un correo 
					<img src="<?php echo site_url('assets/img/iconos/mail.svg');?>" alt="">
				</h4>
				<div class="form">
					<form action="" method="post">
						<div class="row">
							<div class="small-12 columns">
								<label>
									<input type="text" placeholder="Nombres">
								</label>
							</div>
							<div class="small-12 columns">
								<label>
									<input type="text" placeholder="Apellidos">
								</label>
							</div>
							<div class="small-24 columns">
								<label>
									<input type="text" placeholder="Correo Electrónico">
								</label>
							</div>
							<div class="small-4 columns">
								<label>
									<input type="text" placeholder="+52">
								</label>
							</div>
							<div class="small-20 columns">
								<label>
									<input type="text" placeholder="Teléfono">
								</label>
							</div>
							<div class="small-24 columns">
								<label>
									<textarea placeholder="Mensaje"></textarea>
								</label>
							</div>
							<div class="small-24 columns">
								<button type="button" class="success button">Enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="large-11 columns mapa">
				<div id="mapa"></div>
			</div>
		</div>
	</div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKF75u1uI_LgNOqJTpIwvM3bnkimIEqoQ&callback=initialize" async defer></script>
<script>
	var styleArray = 
	[{
		stylers: [
			{ hue: "#c78336" },
			{ saturation: "-20" },
			{ visibility: "simplified" },
			{ color: "#009b37"},
		]
	}];
	function initialize() {
		var myLatlng = new google.maps.LatLng(21.043629, -89.628137);
		var imagePath = '<?php echo site_url('assets/img/pin.png');?>'
		var mapOptions = {
			zoom: 17,
			scrollwheel: false,
			center: myLatlng,
			styles: styleArray,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		var map = new google.maps.Map(document.getElementById('mapa'), mapOptions);
		var contentString = 'Samline';
		var infowindow = new google.maps.InfoWindow({
			content: contentString,
			maxWidth: 500
		});
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			icon: imagePath,
			title: 'image title'
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});
		google.maps.event.addDomListener(window, "resize", function() {
			var center = map.getCenter();
			google.maps.event.trigger(map, "resize");
			map.setCenter(center);
		});
	}
</script>