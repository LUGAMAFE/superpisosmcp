<?= doctype('html5'); ?> 
<!--[if lt IE 7]> 	<html class="no-js ie6 oldie" lang="es" itemscope itemtype="https://schema.org/LocalBusiness"> 	<![endif]-->
<!--[if IE 7]>    	<html class="no-js ie7 oldie" lang="es" itemscope itemtype="https://schema.org/LocalBusiness"> 	<![endif]-->
<!--[if IE 8]>    	<html class="no-js ie8 oldie" lang="es" itemscope itemtype="https://schema.org/LocalBusiness"> 	<![endif]-->
<!--[if IE 9]>    	<html class="no-js ie9" lang="es" itemscope itemtype="https://schema.org/LocalBusiness"> 	<![endif]-->
<!--[if gt IE 9]><!--> 	<html class="no-js" lang="es" itemscope itemtype="https://schema.org/LocalBusiness" ng-app="app"> 		<!--<![endif]-->
	<head<?php if(isset($prefix)) { echo $prefix; } ?>>
		<?= meta('charset', 'utf-8'); ?>
		<?= meta('viewport', 'width=device-width, initial-scale=1.0, user-scalable=no'); ?>
		<?= meta('x-ua-compatible', 'ie=edge', 'equiv'); ?>
		
		<meta name="description" content="">
   		<meta name="author" content="">
   		<base href="<?= base_url();?>">
		<title><?= $this->config->item('name_site');?> - <?= $title ?></title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<?= link_tag(array('href' => 'assets/img/icon.png', 'rel' => 'shortcut icon')); ?>
		<link href="assets/plugins/mmenu/mburger.css" rel="stylesheet">
		<link href="assets/plugins/mmenu/mmenu.css" rel="stylesheet">
		<?= link_tag('assets/css/app.css?v=0.5'); ?>
		<script src="<?= site_url('assets/js/modernizr.js'); ?>"></script>
		<?= $_styles ?>
		<?= $metadatos; ?>	
		<!--[if lt IE 9]>
		<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
		<script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
		<script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
		<![endif]-->
		<script src="<?= site_url('assets/js/jquery.min.js')?>" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<div class="wrapper_body">
			<?= $this->load->view('site/partials/angular', '', true); ?>
			<?= $navbar; ?>	
			<?= $banner; ?>		
			<?= $content; ?>
			<?= $sucursales; ?>
			<?= $contacto; ?>
			<?= $footer; ?>
			<?= $ventana_info; ?>
			<!-- <div class="loader_main">
				<div class="icon">
					<img src="assets/img/loader.svg" alt="">
				</div>
			</div> -->
		</div>
		<?= $_scripts; ?>
		<script src="<?= site_url('assets/js/what-input.js')?>" type="text/javascript" charset="utf-8"></script>
		<script src="<?= site_url('assets/js/slick.min.js')?>" type="text/javascript" charset="utf-8"></script>
		<script src="<?= site_url('assets/js/foundation.js')?>" type="text/javascript" charset="utf-8"></script>
		<script src="<?= site_url('assets/js/app.js')?>" type="text/javascript" charset="utf-8"></script>
		<!-- BOTTOM SCRIPTS -->
		<script src="assets/plugins/mmenu/mburger.js"></script>
		<script src="assets/plugins/mmenu/mmenu.js"></script>
		<script src="assets/plugins/menu.js"></script>
		<!-- <script src="assets/js/loader.js" ></script> -->
		
	</body>
	<!-- <¿php if(ENVIRONMENT == 'development'): ?>
		<script id="__bs_script__">//<![CDATA[
		document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.3'><\/script>".replace("HOST", location.hostname));
	//]]></script>
	<¿php endif ?> -->
</html>
