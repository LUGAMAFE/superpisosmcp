<div class="vistaProducto">
	<div class="contenido">
		<div class="foto">
			<div class="imagen" data-fancybox-trigger="imagesExtra" data-fancybox-index="0">
				<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" data-zoom="<?= site_url($datosProducto['imagen_producto']);?>" style="background-image: url('<?= site_url($datosProducto['imagen_producto']);?>');" alt="">
			</div>
			<?php if(!empty($imagenesExtra)): ?>
				<div class="imagenes-extra">
					<div class="swiper-container swiper-container-imagenes-extra">
						<div class="swiper-wrapper">
							<?php $is_first = 0; foreach($imagenesExtra as $imgExtra): 
								if($is_first == 0) : ?>
									<div class="swiper-slide is-slide-active" fancybox-id="<?= $is_first ?>"  style="background-image:url('<?= site_url($imgExtra['full_route_file']);?>');" src="<?= site_url($imgExtra['full_route_file']);?>">
										<a href="<?= site_url($imgExtra['full_route_file']);?>" data-fancybox="imagesExtra" data-caption="Imagenes del Producto" data-thumb="<?= site_url($imgExtra['full_route_file']);?>" style="position:absolute;z-index:1;width:100%;height:100%"></a>
									</div>
								<?php else: ?>
									<div class="swiper-slide" fancybox-id="<?= $is_first ?>" style="background-image:url('<?= site_url($imgExtra['full_route_file']);?>');" src="<?= site_url($imgExtra['full_route_file']);?>">
										<a href="<?= site_url($imgExtra['full_route_file']);?>" data-fancybox="imagesExtra" data-caption="Imagenes del Producto" data-thumb="<?= site_url($imgExtra['full_route_file']);?>" style="position:absolute;z-index:1;width:100%;height:100%"></a>
									</div>
								<?php endif; $is_first = $is_first+1;?>
							<?php endforeach; ?>
						</div>
						<div class="swiper-pagination swiper-pagination-imagenes-extra"></div>
					</div>
				</div>
			<?php endif; ?>
			<button class="compartir"><img class="share" src="<?= site_url('assets/img/iconos/share.svg');?>" alt=""> <span>Compartir</span> </button>
			<div class="share-dialog" data-options='{"touch" : false}'>
				<div class="header-dialog">
					<h3 class="dialog-title">¡Comparte Nuestro Producto!</h3>
				</div>
				<div class="targets">
					<a class="button button-social" href="https://www.facebook.com/dialog/share?app_id=1769058286560294&display=popup&href=<?=current_url();?>" target="_blank" rel="nofollow">
						<i class="fab fa-facebook-square" style="padding:2px;top:-1px"></i>
						<span>Facebook</span>
					</a>
					
					<a class="button button-social" href="https://api.whatsapp.com/send?text=<?= current_url(); ?>" data-action="share/whatsapp/share" target="_blank" rel="nofollow">
						<i class="fab fa-whatsapp" style="padding:2px;top:-1px"></i>
						<span>Whatsapp</span>
					</a>
				</div>
				<div class="link">
					<div class="pen-url" id="pen-url" title="<?= current_url();?>"><?= current_url();?></div>
					<button class="copy-link" id="copy-link" style="background-color:#359444">Copy Link</button>
				</div>
			</div>
		</div>	
		<div class="descProducto">
			<div class="opciones">
				<h3 class="tituloProducto"><?=$datosProducto['nombre_producto']?></h3>
				<div class="tamano">
					<!--<p>PEI: 3</p>-->
					<p><?=$datosProducto['desc_mcp']?></p>
				</div>
				<div class="precio">
					<?php if($datosProducto['descuento_activado'] == '1'): ?>
					<p class="desde"><?=$datosProducto['descuento_producto']?></p>
					<?php endif; ?>
					<div id="sumadorProductos">
						<button class="menos">-</button>
						<input type="text" class="cantidad" value="10">
						<button  class="mas">+</button>
					</div>
				</div>
				<?php if(!empty($detallesProducto)): ?>
					<div class="detalles">
						<div class="swiper-container swiper-container-detalles">
							<div class="swiper-wrapper">
								<?php foreach($detallesProducto as $detalle): ?>
									<div class="swiper-slide">
										<div class="detalle">
											<p><?=$detalle['nombre_detalle']?></p>
											<p><?=$detalle['valor_detalle']?></p>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="swiper-button-next swiper-button-next-detalles"></div>
  							<div class="swiper-button-prev swiper-button-prev-detalles"></div>
						</div>
					</div>
				<?php endif; ?>
				<button class="anadir" id-prod="<?=$datosProducto['id_mcp']?>">Añadir a la cotización</button>
			</div>
			<div class="descripcion">
				<p class="titulo">Descripción</p>
				<p class="fullDescProducto">
					<?=$datosProducto['desc_prod']?> <br>
					<?=$datosProducto['info_prod']?>
				</p>
				<p class="codigo">Art. <?=$datosProducto['id_mcp']?></p>
			</div>
		</div>
	</div>
</div>

<div class="tabul">
	<div class="contenidoTab">
		<ul class="tabs" data-tabs id="collapsing-tabs">
			<li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Especificaciones</a></li>
			<li class="tabs-title"><a href="#panel2">Usos</a></li>
		</ul>

		<div class="tabs-content" data-tabs-content="collapsing-tabs">
			<div class="tabs-panel is-active" id="panel1">
				<div class="contenido">
					<div class="texto">
						<!--<ul>
							<li>NUMERO 1</li>
							<li>Apariencia estetica que se mantienen en tendencia</li>
							<li>Diseño que se adapta a cualquier espacio</li>
							<li>resistencia en ambientes exteriores y areas humedas</li>
							<li>Variedad de acabados: pulido, brillante, mate o ceroso</li>
						</ul>-->
						<p class="especProducto">
							<?=$datosProducto['espec_prod']?>
						</p>
					</div>
				</div>
			</div>
			<div class="tabs-panel" id="panel2">
			<div class="contenido">
					<div class="swiper-container swiper-container-usos">
						<div class="swiper-wrapper">
						<?php foreach($imagenesUsos as $imagenUsos): ?>
							<div class="swiper-slide" style="background-image:url('<?= site_url($imagenUsos['full_route_file']);?>');">
								<a href="<?= site_url($imagenUsos['full_route_file']);?>" data-fancybox="images" data-thumb="<?= site_url($imagenUsos['full_route_file']);?>" data-caption="Usos" style="position:absolute;z-index:1;width:100%;height:100%"></a>
							</div>
						<?php endforeach; ?>
						</div>
						<div class="swiper-pagination swiper-pagination-usos"></div>
						<div class="swiper-button-next swiper-button-next-usos"></div>
  						<div class="swiper-button-prev swiper-button-prev-usos"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(!empty($productosRelacionados)): ?>
	<div class="titulo-site">
		<h1 class="titulo">Productos Relacionados</h1>
	</div>

	<div class="row expanded lista-productos slick-productos">
		<?php foreach ($productosRelacionados as $producto): ?>
			<div class="cont-productos">
				<div class="info-producto">
						<?php if($producto['descuento_activado'] == '1'): ?>
						<div class="descuento">
							<div class="img">
								<img src="<?= site_url('assets/img/iconos/descuento.svg'); ?>" alt="">
							</div>
						</div>
						<?php endif; ?>
						<?php if($producto['producto_rey'] == '1'): ?>
						<div class="corona">
							<div class="img">
								<img src="<?= site_url('assets/img/iconos/corona.png'); ?>" alt="">
							</div>
						</div>
						<?php endif; ?>
						<div class="img">
							<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url($producto['imagen_producto']);?>');" alt="">
							<?php if($producto['info_extra'] != ''): ?>
							<div class="info">
							<div class="img-info tooltip<?=$producto['id_mcp']?>" data-id="img-info<?=$producto['id_mcp']?>" data-jbox-content="<?=$producto['info_extra']?>">
								<?= file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
							</div>
							<script type="text/javascript">
								$(document).ready(function() {
									new jBox('Mouse', {
										attach: $("[data-id='img-info<?=$producto['id_mcp']?>']"),
										title: 'Información Adicional',
										getContent: 'data-jbox-content'
									});
								});
							</script>
							</div>
							<?php endif; ?>
						</div>
						<div class="texto clearfix">
							<h1 class="titulo"><?= $producto['nombre_producto'] ?></h1>
							<p>
							<?= $producto['desc_producto'] ?>
							</p>
							<span class="codigo">Art. <?= $producto['id_mcp'] ?></span>
						</div>
					</div>
					<div class="text-center btn">
						<a href="<?=base_url();?>producto/<?=$producto['id_mcp']?>" class="button expanded success">Ver detalles</a>
					</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>

<script>
	const urlCarrito = '<?=base_url();?>carrito';
	 
	$(document).on('ready', function() {
		$('.slick-productos').slick({
			prevArrow: '<button type="button" class="slick-prev"><img src="<?= site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
			nextArrow: '<button type="button" class="slick-next"><img src="<?= site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: true,
			autoplay: true,
			arrows: false,
			centerPadding: '60px',
			autoplaySpeed: 5000,
			responsive: [
				{   breakpoint: 950,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						infinite: true,
						autoplay: true,
						autoplaySpeed: 6000,
						dots: true
					}
				},
				{	breakpoint: 700,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						infinite: true,
						autoplay: true,
						autoplaySpeed: 6000,
						dots: true,
						arrows: false,
					}
				},
				{	breakpoint: 400,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						infinite: true,
						autoplay: true,
						autoplaySpeed: 6000,
						dots: true,
						arrows: false,
					}
				},
			]
		});
	});
</script>