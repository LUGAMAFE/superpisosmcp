<div class="vistaProducto">
	<div class="contenido">
		<div class="foto">
			<div class="imagen">
				<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url('assets/img/notfound.png');?>');" alt="">
			</div>
		</div>	
		<div class="descProducto">
			<div class="opciones">
				<h3 class="tituloProducto">Producto No Disponible</h3>
				<div class="tamano">
                    <a href="<?= base_url();?>">
                        <p>Regresar a la página principal</p>
                    </a>
				</div>
			</div>
		</div>
	</div>
</div>

