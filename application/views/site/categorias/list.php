<div ng-controller="controllerProductosFiltro">
<div class="row expanded categoria-productos">
	<div class="cont-titulo">
		<h1 class="titulo"><?= $datosCategoria["informacionCategoria"] ?></h1>
	</div>
	<?php if($datosCategoria["nombreCategoria"] != "Remates" && !empty($datosCategoria["filtros_categoria"]->schema)): ?>
		<div class="filtros">
			<h3 class="titulo">Filtrar por:</h3>
			<div class="cont-filtros">
				<form action="">
					<div class="items-filtros clearfix" style="display: table-row;">
						<div ng-repeat="filtro in filtros" ng-cloak style="width: fit-content;display: table-cell;vertical-align: middle;padding: 0.35rem;">
							<label for="" ng-show="(filtro.type == 'string' || filtro.type == 'number') && filtro.active == true">
								<input type="text" ng-model="buscarProducto[filtro.uuid]" placeholder="{{filtro.name}}" style="margin: 0">
							</label>
							
							<label for="" ng-show="(filtro.type == 'select' || filtro.type == 'radio') && filtro.active == true">
								<select ng-model="buscarProducto[filtro.uuid]">
									<option value="">{{filtro.name}}</option>
									<option value="{{valueOpcion}}" ng-repeat="(keyOpcion, valueOpcion) in filtro.enum">{{valueOpcion}}</option>
								</select>
							</label>

							<label for="" ng-show="(filtro.type == 'checkbox') && filtro.active == true" style="font-size:0.95rem">
								<!--<select multiple>
									<option value="">{{filtro.name}}</option>
									<option value="" ng-repeat="(keyOpcion, valueOpcion) in filtro.enum">{{valueOpcion}}</option>
								</select>-->
								{{filtro.name}}:
								<ui-select multiple ng-model="buscarProducto[filtro.uuid]" theme="select2" ng-disabled="ctrl.disabled" close-on-select="false" style="width: 200px;margin-left: 0.2rem" title="Choose a color">
									<ui-select-match>
										{{$item}}
									</ui-select-match>
									<ui-select-choices repeat="opcion in filtro.enum">
										{{opcion}}
									</ui-select-choices>
								</ui-select>
							</label>

							<label for="" ng-show="filtro.type == 'color' && filtro.active == true">
								<!--<select multiple>
									<option value="">{{filtro.name}}</option>
									<option value="" ng-repeat="(keyOpcion, valueOpcion) in filtro.enum">{{valueOpcion.name}}</option>
								</select>
								<select2 ng-model="selected" s2-options="valueOpcion.name as valueOpcion.name for valueOpcion in filtro.enum"></select2>-->
								{{filtro.name}}:
								<ui-select multiple ng-model="buscarProducto[filtro.uuid]" theme="select2" ng-disabled="ctrl.disabled" close-on-select="false" style="width: 200px;margin-left: 0.2rem" title="Choose a color">
									<ui-select-match>
										{{$item.name}}
									</ui-select-match>
									<ui-select-choices repeat="opcion in filtro.enum">
										<span>
											<div ng-style="{'background-color':'{{opcion.color}}', width:'2vh', height:'2vh', display:'inline-block',border:'1px solid', 'border-color':'black'}" ></div>
											{{opcion.name}}
										</span>
									</ui-select-choices>
								</ui-select>
							</label>
						</div>
						
						
					</div>
				</form>
			</div>
			<?php /*<pre>
				{{buscarProducto | json}}
			</pre>*/ ?>
			<div style="display: none">
				<h3 class="titulo">Ordenar por:</h3>
				<div class="cont-filtros small">
					<form action="">
						<div class="items-filtros clearfix">
							<select name="" id="">
								<option value="">--</option>
							</select>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="row text-right">
		<pagination
			class="pagination" 
			total-items="(productos | filtrosCategoria: buscarProducto).length" 
			page="currentPage" 
			items-per-page="pageSize" 
			ng-model="currentPage"
			previous-text="Anterior" 
			next-text="Siguiente"
			max-size="5"></pagination>
	</div>
	<div class="lista-productos filtro-producto">
		<div class="row expanded small-up-2 medium-up-3 large-up-4">
				<div ng-repeat="producto in productos | filtrosCategoria: buscarProducto | start: (currentPage - 1) * pageSize | limitTo: pageSize track by $index" ng-cloak class="column column-block">
					<!-- Template Producto -->
					<div class="cont-productos">
						<div class="info-producto clearfix">
								<div class="descuento" ng-if="producto.descuento_activado == 1">
									<div class="img">
										<img src="<?= site_url('assets/img/iconos/descuento.svg'); ?>" alt="">
									</div>
								</div>
								<div class="corona" ng-if="producto.producto_rey == 1">
									<div class="img">
										<img src="<?= site_url('assets/img/iconos/corona.png'); ?>" alt="">
									</div>
								</div>
								<div class="img">
									<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url("{{producto.archivo_medium}}");?>');" alt="">
									<div class="info" ng-if="producto.info_extra != ''">
										<div class="img-info tooltip{{producto.id_mcp}}" data-id="img-info{{producto.id_mcp}}" data-jbox-content="{{producto.info_extra}}" ng-init="activarJbox(producto.id_mcp)">
											<?= file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
										</div>
									</div>
								</div>
							<div class="texto clearfix">
								<h1 class="titulo">{{producto.nombre_producto}}</h1>
								<p>
								{{producto.desc_producto}}
								</p>
								<span class="codigo">Art. {{producto.id_mcp}}</span>
							</div>
							<div class="text-center btn">
								<a href="<?=base_url();?>producto/{{producto.id_mcp}}" class="button expanded success">Ver detalles</a>
							</div>
						</div>
					</div>
					<!-- END Template Producto -->
				</div>
  		</div>
	</div>
</div>

<?php if(!empty($productos_recomendados)): ?>
	<div class="titulo-site">
		<h1 class="titulo">Le Recomendamos</h1>
	</div>
	<div class="cien">
		<div class="row expanded lista-productos slick-productos">
			<?php foreach ($productos_recomendados as $producto) { ?>
			<div class="cont-productos">
				<div class="info-producto">
					<?php if($producto->descuento_activado == 1): ?>
					<div class="descuento">
						<div class="img">
							<img src="<?= site_url('assets/img/iconos/descuento.svg'); ?>" alt="">
						</div>
					</div>
					<?php endif; ?>
					<?php if($producto->producto_rey == 1): ?>
					<div class="corona">
						<div class="img">
							<img src="<?= site_url('assets/img/iconos/corona.png'); ?>" alt="">
						</div>
					</div>
					<?php endif; ?>
					<div class="img">
						<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url($producto->imagen_producto);?>');" alt="">
						<?php if($producto->info_extra != null): ?>
						<div class="info">
						<div class="img-info tooltip<?=$producto->id_mcp?>" data-id="img-info<?=$producto->id_mcp?>" data-jbox-content="<?=$producto->info_extra?>">
							<?= file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
						</div>
						<script type="text/javascript">
							$(document).ready(function() {
								new jBox('Mouse', {
									attach: $("[data-id='img-info<?=$producto->id_mcp?>']"),
									title: 'Información Adicional',
									getContent: 'data-jbox-content'
								});
							});
						</script>
						</div>
						<?php endif; ?>
					</div>
					<div class="texto clearfix">
						<h1 class="titulo"><?= $producto->nombre_producto ?></h1>
						<p>
						<?= $producto->desc_producto ?>
						</p>
						<span class="codigo">Art. <?= $producto->id_mcp ?></span>
					</div>
				</div>
				<div class="text-center btn">
					<a href="<?=base_url();?>producto/<?=$producto->id_mcp?>" class="button expanded success">Ver detalles</a>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
<?php endif ?>	

</div>
<script>
	$(document)
		.on('ready', function() {
			$('.slick-productos').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?= site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?= site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 4,
				slidesToScroll: 1,
				infinite: true,
				autoplay: true,
				arrows: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
				responsive: [
					{   breakpoint: 950,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true
						}
					},
					{	breakpoint: 700,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true,
							arrows: false,
						}
					},
					{	breakpoint: 400,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true,
							arrows: false,
						}
					},
				]
			});
		});
</script>

<script>
	<?php /*function removeDuplicates(originalArray, prop) {
		var newArray = [];
		var lookupObject  = {};

		for(var i in originalArray) {
			lookupObject[originalArray[i][prop]] = originalArray[i];
		}

		for(i in lookupObject) {
			newArray.push(lookupObject[i]);
		}
		return newArray;
	}*/ ?>

	myApp.filter('start', function () {
	    return function (input, start) {
	        if (!input || !input.length) { return; }

	        start = +start;
	        return input.slice(start);
	    };
	});

	myApp.filter('filtrosCategoria', function() {
		return function(items, tipo) {
			filtered = items;
			if(typeof tipo === 'object' && tipo !== null && tipo !== '') {
				if(Object.keys(tipo).length > 0) {
					var filteredaux = [];
					angular.forEach(tipo, function(value, key) {
			        	angular.forEach(filtered, function(item) {
			        		if(item[key]) {
			        			if(typeof value === 'object') {
			        				if(Object.keys(value).length > 0) {
			        					angular.forEach(value, function(colorValue) {
			        						if(colorValue.color == item[key]) {
			        							filteredaux.push(item);
			        						}
			        					});
			        				} else {
			        					if(item[key] == value.color) {
					        				filteredaux.push(item);
					        			}
			        				}
			        			} else {
			        				if(item[key] == value) {
				        				filteredaux.push(item);
				        			}
			        			}			        			
			        		}
				        });
			        });
			        filtered = filteredaux;
				}
			}
			if(Object.keys(filtered).length > 0) {
				return filtered;
			} else {
				return items;
			}
	    };
	});

	myApp.controller('controllerProductosFiltro', function($scope, $http, $filter) {
		$scope.productos = <?= $productosJson; ?>;
		$scope.filtros = <?= isset($filtrosJson) ? $filtrosJson : "''" ?>;
		$scope.filtros = $scope.filtros.schema;
		$scope.buscarProducto = {};

		$scope.currentPage = 1;
    	$scope.pageSize = 12;
		//$scope.selected = { value: $scope.filtros[0] };
		//console.log($scope.productos);
		//console.log($scope.filtros);

		$scope.activarJbox = function($producto_id){
			$(document).ready(function() {
				new jBox('Mouse', {
					attach: $("[data-id='img-info"+$producto_id+"']"),
					title: 'Información Adicional',
					getContent: 'data-jbox-content'
				});
			});
		}
    });
</script>