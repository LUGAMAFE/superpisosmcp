
<?php foreach ($categorias_home as $categoria) { ?>
	<div class="categoria">
		<a href="categoria/<?=$categoria->id_cat. "/". strtolower($categoria->nombre_categoria); ?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
			<div class="img">
				<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
				<div class="center">
					<img src="<?= site_url($categoria->imagen_categoria);?>" alt="">
				</div>
			</div>
			<div class="texto">
				<h1 class="titulo"><?=$categoria->nombre_categoria?></h1>
			</div>
		</a>
	</div>
<?php } ?>

<!--<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/pisos.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Pisos</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/recubrimientos.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Recubrimientos</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/porcelanatos.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Porcelanicos</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/descorados.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Decorados</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/pisos.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Adhesivos</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/cortadoras.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Cortadoras</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/muebles-de-bano.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Muebles de baño</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/griferia.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Griferia</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/tinas.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Tinas</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/canceles.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Canceles</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/tinacos.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Tinacos</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/bombas.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Bombas</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/calentadores.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Calentadores</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/tarjas.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Tarjas</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/parrillas-y-campanas.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Parrillas y campanas</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/puertas-y-marcos.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Puertas y marcos</h1>
		</div>
	</a>
</div>

<div class="categoria">
	<a href="<?= site_url('categoria');?>" class="clearfix cont-items" href="<?= site_url('home/categoria');?>">
		<div class="img">
			<img src="<?= site_url('assets/img/fondo-categoria-producto-home.png');?>" alt="">
			<div class="center">
				<img src="<?= site_url('assets/img/prueba/categorias/accesorios-y-ferreteria.png');?>" alt="">
			</div>
		</div>
		<div class="texto">
			<h1 class="titulo">Accesorios y ferretería</h1>
		</div>
	</a>
</div>-->