<script>
	$(document)
		.on('click', '.btn-visualizar-categorias', function(e) {
			e.preventDefault();
			var option = $(this).attr('data-option');
			if(option == 0) {
				$('.cont-items').addClass('active');
				$(this).attr('data-option', 1);
			} else{
				$('.cont-items').removeClass('active');
				$(this).attr('data-option', 0);
			}
		})
</script>
<div>
	<div class="titulo-site">
		<h1 class="titulo padding-top">Nuestros productos</h1>
	</div>

	<div class="categorias categorias-home">
		<div class="row expanded list-categorias">
			<?= $this->load->view('site/home/categorias', $categorias_home, true); ?>
		</div>
		<div class="btn-site text-center">
			<a href="#" class="button success btn-visualizar-categorias" data-option="0">Ver todos los productos</a>
		</div>
	</div>
</div>

<?php if(!empty($info_home['home'][0]->img_secundario_dir)): ?>
<div class="banner banner-home-2">
	<div class="fondo">
		<img src="<?= site_url('assets/img/fondo-banner-home-2.png');?>" style="background-image: url('<?= $info_home['home'][0]->img_secundario_dir.$info_home['home'][0]->img_secundario_name.' (large).'.$info_home['home'][0]->img_secundario_ext?>');" alt="">
	</div>
</div>
<?php endif ?>

<?php if(count($p_mas_vendidos) > 0): ?>
<div class="titulo-site">
	<h1 class="titulo">Los más vendidos</h1>
</div>
<div class="cien">
	<div class="row expanded lista-productos slick-productos">
		<?php foreach ($p_mas_vendidos as $producto) { ?>
		<div class="cont-productos">
			<div class="info-producto">
				<?php if($producto->descuento_activado == 1): ?>
				<div class="descuento">
					<div class="img">
						<img src="<?= site_url('assets/img/iconos/descuento.svg'); ?>" alt="">
					</div>
				</div>
				<?php endif; ?>
				<?php if($producto->producto_rey == 1): ?>
				<div class="corona">
					<div class="img">
						<img src="<?= site_url('assets/img/iconos/corona.png'); ?>" alt="">
					</div>
				</div>
				<?php endif; ?>
				<div class="img">
					<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url($producto->imagen_producto);?>');" alt="">
					<?php if($producto->info_extra != null): ?>
					<div class="info">
					<div class="img-info tooltip<?=$producto->id_mcp?>" data-id="img-info<?=$producto->id_mcp?>" data-jbox-content="<?=$producto->info_extra?>">
						<?= file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
					</div>
					<script type="text/javascript">
						$(document).ready(function() {
							new jBox('Mouse', {
								attach: $("[data-id='img-info<?=$producto->id_mcp?>']"),
								title: 'Información Adicional',
								getContent: 'data-jbox-content'
							});
						});
					</script>
					</div>
					<?php endif; ?>
				</div>
				<div class="texto clearfix">
					<h1 class="titulo"><?= $producto->nombre_producto ?></h1>
					<p>
					<?= $producto->desc_producto ?>
					</p>
					<span class="codigo">Art. <?= $producto->id_mcp ?></span>
				</div>
			</div>
			<div class="text-center btn">
				<a href="<?=base_url();?>producto/<?=$producto->id_mcp?>" class="button expanded success">Ver detalles</a>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
<?php endif ?>	

<?php if(count($p_del_rey) > 0): ?>
<div class="titulo-site">
	<h1 class="titulo">productos del rey</h1>
</div>

<div class="cien">
	<div class="row expanded lista-productos slick-productos">
		<?php foreach ($p_del_rey as $key=>$producto) { ?>
			<div class="cont-productos">
				<div class="info-producto">
					<?php if($producto->descuento_activado == 1): ?>
					<div class="descuento">
						<div class="img">
							<img src="<?= site_url('assets/img/iconos/descuento.svg'); ?>" alt="">
						</div>
					</div>
					<?php endif; ?>
					<div class="corona">
						<div class="img">
							<img src="<?= site_url('assets/img/iconos/corona.png'); ?>" alt="">
						</div>
					</div>
					<div class="img">
						<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url($producto->imagen_producto);?>');" alt="">
						<?php if($producto->info_extra != null): ?>
						<div class="info">
							<div class="img-info tooltip<?=$producto->id_mcp?>" data-id="img-info<?=$producto->id_mcp?>" data-jbox-content="<?=$producto->info_extra?>">
								<?= file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
							</div>
							<script type="text/javascript">
								$(document).ready(function() {
									new jBox('Mouse', {
										attach: $("[data-id='img-info<?=$producto->id_mcp?>']"),
										title: 'Información Adicional',
										getContent: 'data-jbox-content'
									});
								});
							</script>
						</div>
						<?php endif; ?>
					</div>
					<div class="texto clearfix">
						<h1 class="titulo"><?= $producto->nombre_producto ?></h1>
						<p>
						<?= $producto->desc_producto ?>
						</p>
						<span class="codigo">Art. <?= $producto->id_mcp ?></span>
					</div>
				</div>
				<div class="text-center btn">
					<a href="<?=base_url();?>producto/<?=$producto->id_mcp?>" class="button expanded success">Ver detalles</a>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<?php endif ?>

<div class="titulo-site">
	<h1 class="titulo">Los consejos del rey</h1>
</div>
<div class="cien">
	<div class="row expanded lista-videos slick-videos">
		<?php foreach ($info_home["url_youtube"] as $url): ?>
			<div class="cont-videos">
				<a data-fancybox href=<?= $url->url_video ?>>
					<div class="img">
						<img src="<?= site_url('assets/img/fondo-video-home.png');?>" style="background-image: url('https://img.youtube.com/vi/<?= $url->url_video_id ?>/maxresdefault.jpg');" alt="">
						<div class="hover">
							<i class="far fa-play-circle"></i>
						</div>
					</div>
					<div class="texto">
						<p>
							<img src="<?= site_url('assets/img/iconos/push-pin.svg');?>" alt=""> Descubre las diferencias entre pisos y recubrimientos.
						</p>
					</div>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<?php if(!empty($info_home['home'][0]->img_terciario_dir)): ?>
<div class="banner banner-home-2">
	<div class="fondo">
		<img src="<?= site_url('assets/img/fondo-banner-home-22.png');?>" style="background-image: url('<?= $info_home['home'][0]->img_terciario_dir.$info_home['home'][0]->img_terciario_name.' (large).'.$info_home['home'][0]->img_terciario_ext?>');" alt="">
	</div>
</div>
<?php endif ?>

<script>
	$(document)
		.on('ready', function() {
			$('.slick-productos').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?= site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?= site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 4,
				slidesToScroll: 1,
				infinite: true,
				autoplay: true,
				arrows: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
				responsive: [
					{   breakpoint: 950,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true
						}
					},
					{	breakpoint: 700,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true,
							arrows: false,
						}
					},
					{	breakpoint: 400,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true,
							arrows: false,
						}
					},
				]
			});

			$('.slick-videos').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?= site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?= site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 2,
				slidesToScroll: 1,
				autoplay: false,
				arrows: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
				responsive: [
					{
					breakpoint: 750,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 3800,
							dots: true,
							arrows: false,
						}
					},
				]
				
			});

			$('.slick-empresas').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?= site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?= site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 4,
				slidesToScroll: 1,
				autoplay: true,
				arrows: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
				responsive: [
					{   breakpoint: 950,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
						}
					},
					{	breakpoint: 700,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							arrows: false,
						}
					},
					{	breakpoint: 400,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 6000,
							dots: true,
							arrows: false,
						}
					},
				]
			});
		});
</script>

<div class="row expanded quienes-somos-home">
	<div class="columna texto">
		<h1 class="titulo">¿quiÉnes somos?</h1>
		<p>
			A 18 años de ser creada, MCP. Mayoreo Cerámico de la Península S.A. de C.V. se ha consolidado como una de las empresas más importantes dentro del ramo de acabados cerámicos. Destacando en la venta pisos, azulejos, porcelánicos, llaves, tarjas y todo lo necesario para su baño y cocina.
		</p>
		<p>
			Actualmente cuenta con 3 sucursales en Mérida y más de 500 subdistribuidores asociados con presencia en Yucatán y en otros estados como lo son Campeche, Quintana Roo, Tabasco y Chiapas. Además, tiene presencia no sólo en México sino también en Belice y Guatemala.
		</p>
		<p>
			La calidad en sus productos le ha dado participación en proyectos muy importantes destacando plazas comerciales, hoteles, hospitales y complejos habitacionales.
		</p>
		<p>
			MCP es una organización orientada a cubrir las necesidades de sus clientes con la mayor calidad y el mejor servicio al cliente. ¡Le esperamos!
		</p>
	</div>
	<div class="columna img">

		<?php if(!empty($info_home['home'][0]->img_quienes_dir)): ?>
		<img src="<?= site_url('assets/img/fondo-quienes-somos.png');?>" style="background-image: url('<?= $info_home['home'][0]->img_quienes_dir.$info_home['home'][0]->img_quienes_name.' (large).'.$info_home['home'][0]->img_quienes_ext?>');" alt="">
		<?php endif ?>
	</div>
</div>
<div class="cien">
	<div class="row expanded lista-empresas slick-empresas">
		<?php for($i = 1; $i <= 5; $i++): ?>
			<div class="cont-empresas">
				<div class="img">
					<img src="<?= site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/empresas/'.$i.'.png');?>');" alt="">
				</div>
			</div>
		<?php endfor?>
	</div>
</div>
