<?php if($banner == 1): ?>
	<div class="swiper-container"  role="region">
		<div class="swiper-wrapper">
		<?php foreach($info_home['banners'] as $row): 
			$datos_extras_imagen = json_decode($row->json_input_extras)?>
			<div class="swiper-slide">
			<a href="<?= $datos_extras_imagen->url_banner?>" class="fondo" style="position:absolute;z-index:1;width:100%;height:100%"></a>
			<img class="img-banner-principal" src="<?php echo site_url('assets/img/fondo-banner-home-new.png');?>" style="background-image: url('<?= $row->dir_file.$row->name_file.'.'.$row->ext_file ?>');" alt="">
			</div>
		<?php endforeach?>
		</div>
		<?php if(count($info_home['banners']) > 1 ):?>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
		<?php endif; ?>
	</div>
<script>
	/*$(window).on('load', function() {
			$('.banner-home').slick({
				dots: true,
				infinite: true,
				speed: 500,
				fade: true,
				cssEase: 'linear',
				autoplay: true,
  				autoplaySpeed: 5000,
			});
		});*/
	$(document).ready(function() {
		var swiper = new Swiper('.swiper-container', {
			spaceBetween: 30,
			centeredSlides: true,
			loop: true,
			preventClicks: false,
			touchEventsTarget: 'container',
			preventClicksPropagation: false,
			autoplay: {
			delay: 6000,
			disableOnInteraction: false,
			},
			pagination: {
			el: '.swiper-pagination',
			clickable: true,
			},
			navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
			},
			on: {
			click: function(event) {
				let elementoClick = $(event.target);
				if(elementoClick.is("a")){
				let $elementoActivo = this.$el;
				$elementoActivo = $($elementoActivo);
				let anchor = $elementoActivo.find("a");
				let $url = anchor.attr("href");
				window.location.href = $url;
				}
			},
			}
		});
	});
</script>
<?php elseif($banner == 2): ?>
<div class="banner-categoria banner">
	<div class="fondo">
		<img src="<?= site_url('assets/img/fondo-banner-categoria.png');?>" style="background-image: url('<?= $banner_categoria ?>');" alt="">
		<div class="cont-titulo">
			<h1 class="titulo"><?= $texto_categoria ?></h1>
		</div>
	</div>
</div>
<?php elseif($banner == 3): ?>
<div class="banner-bolsa banner">
	<div class="fondo">
		<?php foreach($banner_bolsa as $banner): ?>
			<img src="<?= site_url('assets/img/fondo-banner-home.png');?>" style="background-image: url('<?= $banner->dir_file.$banner->name_file.' (large).'.$banner->ext_file ?>');" alt="">
		<?php endforeach?>
	</div>
</div>
<?php endif ?>