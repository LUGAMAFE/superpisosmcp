<div class="seccionSucursales">
    <div class="contenido">
        <h3 class="titulo">Nuestras sucursales</h3>
        <div class="sucursales">
            <div class="sucursal">
                <img src="<?= site_url('assets/img/sucursales/CEDIS.png');?>" alt="">
                <div class="texto">
                    <h4>Sucursal Cedis</h4>
                    <p>Estamos en el periferico sur a 100m de la academia de policias sobre
                        circuito colonias tenemos <br/><span>Comodo estacionamiento</span>
                    </p>
                </div>
            </div>
            <div class="sucursal">
                <img src="<?= site_url('assets/img/sucursales/NTE.png');?>" alt="">
                <div class="texto">
                    <h4>Sucursal Cedis</h4>
                    <p>Estamos en la calle 60 Norte sobre
                        circuito colonias tenemos <br/><span>Comodo estacionamiento</span>
                    </p>
                </div>
            </div>
            <div class="sucursal">
                <img src="<?= site_url('assets/img/sucursales/DRAGONES.png');?>" alt="">
                <div class="texto">
                    <h4>Sucursal Cedis</h4>
                    <p>Estamos en el Arco de Dragones sobre la calle 50 tenemos <br/><span>Comodo estacionamiento</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>