<meta property="og:type" content="website" />
<meta property="og:title" content="<?= isset($titulo) ? $titulo : 'Superpisos MCP' ?>" />
<meta property="og:description" content="<?= isset($desc) ? $desc : 'El Mayorista de Pisos más grande del sureste' ?>" />
<meta property="og:image" content="<?= isset($img) ? site_url($img) : site_url('assets/img/logo.svg');?>" />