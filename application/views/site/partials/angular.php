<script src="<?php echo site_url('assets/js/angular/angular.min.js');?>"></script>
<script src='<?php echo site_url('assets/js/angular/angular-sanitize.js');?>'></script>
<script src="<?php echo site_url('assets/js/angular/angular-select2/dist/angular-select2.min.js');?>"></script>
<script src="<?php echo site_url('assets/js/angular/ui-select/dist/select.js');?>"></script>
<link href="<?php echo site_url('assets/js/angular/ui-select/dist/select.css');?>" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.bootstrap2.css">
<!--<script src="<?php echo site_url('assets/js/angular/ionic-range-slider.js');?>"></script>-->
<script type="text/javascript" src="<?php echo site_url('assets/plugins/angular-foundation/angular-foundation.js');?>"></script>


<script type="text/javascript">
    var myApp = angular.module('app', ['mm.foundation.modal','rt.select2','ui.select', 'mm.foundation.pagination', 'ngSanitize']); 

    myApp.filter('ceil', function() {
	    return function(input) {
	        return Math.ceil(input);
	    };
	});

	myApp.filter('masuno', function() {
	    return function(input) {
	        return parseFloat(input) + parseFloat(0.1);
	    };
	});
    
	myApp.directive('numLinksDisplay', function ($timeout) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attr) {
	            if (scope.$last === true) {
	                $timeout(function () {
	                    scope.$emit(attr.numLinksDisplay);
	                });
	            }
	        }
	    }
    });
    
    myApp.directive('onFinishRender', function () {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attr) {
	            if (scope.$last === true) {
	                element.ready(function () {
	                    initFoundation();
	                });
	            }
	        }
	    }
	});

    function initFoundation()
	{
		$(".js-equalizer").attr("data-equalizer", "true").foundation(); 
	}

    myApp.directive('checkImage', function($http) {
	    return {
	        restrict: 'A',
	        link: function(scope, element, attrs) {
	        	//console.log(attrs);
	            attrs.$observe('ngSrc', function(ngSrc) {
	            	console.log(ngSrc);
	                $http.get(ngSrc).success(function(){
	                    //alert('image exist');
	                }).error(function(){
	                    //alert('image not exist');
	                    element.attr('src', '<?php echo site_url('assets/img/default.png');?>'); // set default image
	                });
	            });
	        }
	    };
	});
</script>