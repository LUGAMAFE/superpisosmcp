<?php
		date_default_timezone_set('Mexico/General');
		$date = new DateTime('NOW');
		$dateTime = $date->format('Y');
?>
<div class="row expanded formas-pago">
	<div class="row expanded pago-tienda">
		<h1 class="titulo-pago">formas de pago EN TIENDA:</h1>
		<div class="row expanded small-up-2 medium-up-4 large-up-4 lista-items">
		<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/efectivo.svg');?>');" alt="">
				</div>
				<div class="texto">
					<p>Efectivo</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/visa.svg');?>');" alt="">
				</div>
				<div class="texto">
					<p>Tarjeta de crédito</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/master.svg');?>');" alt="">
				</div>
				<div class="texto">
					<p>Tarjeta de crédito</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/american-express.svg');?>');" alt="">
				</div>
				<div class="texto">
					<p>American Express</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row expanded pago-credito">
		<h1 class="titulo-pago">OTRAS FORMAS DE PAGO:</h1>
		<div class="row expanded small-up-2 medium-up-3 large-up-5 lista-items">
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/mejoravit.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Mejoravit</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/fonacot.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Crédito FONACOT</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/bbva.svg');?>');" alt="">
				</div>
				<div class="texto">
					<p>Puntos BBVA</p>
				</div>
			</div>
			<!--<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/sistema-apartado.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Sistema de apartado</p>
				</div>
			</div>-->
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/si-vale.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Sí vale</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/broxel.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Broxel</p>
				</div>
			</div>
		</div>
	</div>
</div>

<footer class="footer-site clearfix">
	<div class="links">
		<div class="grupo">
			<h5 class="titulo-footer">Menú</h5>
			<ul class="vertical clearfix menu-categorias">
				<li>
					<a href="<?= site_url('bolsa-de-trabajo');?>">
						Bolsa de Trabajo
					</a>
				</li>
				<li>
					<a href="<?= site_url('asociados');?>">
						Subdistribuidores
					</a>
				</li>
				<li>
					<a href="<?= site_url('contacto');?>">
						Contacto
					</a>
				</li>
				<?php if($mostrarRemates) : ?>
					<li>
						<a href="<?= site_url('categoria/remates');?>">
							Remates
						</a>
					</li>
				<?php endif;?>
			</ul>
		</div>
		<div class="grupo">
			<h5 class="titulo-footer">Categorías</h5>
			<ul class="menu-categorias clearfix row expanded small-up-2 medium-up-3 large-up-5">
				<?php foreach ($categorias as $categoria):?>
					<?php if(!$categoria["nuevaCategoria"]): ?>
						<li class="column column-block">
							<a href="http://superpisosmcp.cm/categoria/<?= $categoria['idCategoria']."/".$categoria["nombreCategoria"] ?>"> 
								<?= $categoria["nombreCategoria"] ?>
							</a>
						</li>
					<?php endif; ?>
				<?php endforeach;?>
			</ul>
		</div>
	</div>
	<div class="info-contacto">
		<div class="texto">
			<p>
				Le esperamos en <br>
				nuestras sucursales:
			</p>
			<a href="tel:+529999460315" class="phone">
				Contáctanos al (999) 946 03 15
			</a>
			<p class="horario">
				Lunes a Viernes de 8:00 am -  7:00 pm
			</p>
			<p class="horario">
				Sabado a Domingo de 8:00 am - 4:00 pm
			</p>
			<div class="row">
				<ul class="contacto menu align-center">
					<li>
						<a href="https://www.facebook.com/mayoreoceramicodelapeninsula">
							<span>
								<i class="fab fa-facebook-f"></i>
							</span>
						</a>
					</li>
					<li>
						<a href="tel:+529999460315">
							<span>
								<i class="fas fa-phone"></i>
							</span>
						</a>
					</li>
					<li>
						<a href="mailto:dgrafico@superpisosmcp.com">
							<span>
								<i class="far fa-envelope"></i>
							</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="img-logo">
			<img src="<?php echo site_url('assets/img/logo-footer.svg');?>" alt="">
		</div>
	</div>
	<div class="copy">
		<p>© Copyright <?php echo $dateTime ?> | Mayoreo cerámico de la península S.A de C.V | TODOS LOS DERECHOS RESERVADOS | 
			<a href="">
				Términos y condiciones.
			</a>
			<a href="">
				Aviso de privacidad
			</a>
			<a href="" class="gant-soluciones-empresariales">
				<img src="<?php echo site_url('assets/img/iconos/gt.svg');?>" alt="">
			</a>
		</p>
	</div>
</footer>