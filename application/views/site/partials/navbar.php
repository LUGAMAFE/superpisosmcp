<header class="header-site" data-sticky-container style="z-index: 100;">
	<div class="header-sticky sticky" data-sticky data-margin-top="0" data-sticky-on="small">
		<div class="fila-uno-header">
			<a href="<?= base_url();?>" class="logo">
				<img src="<?= site_url('assets/img/logo.svg');?>" alt="">
			</a>

			<a href="#sucursales" class="ubicacion">
				<div class="icon">
					<img src="<?= site_url('assets/img/iconos/point.svg');?>" alt="">
				</div>
				<div class="texto">
					<p>
						Encuentra tu <br>
						sucursal más cercana
					</p>
				</div>
			</a>

			<div class="search-input">
				<form id="form-busqueda-global" action="buscar-termino" method="get">
					<div class="input-group">
						<input class="input-group-field" type="search" name="busqueda_global" placeholder="¿Qué estás buscando?">
						<div class="input-group-button">
							<button type="submit" class="button">
								<i class="fas fa-search"></i>
							</button>
						</div>
					</div>
				</form>
			</div>

			<div class="redes">
				<ul class="menu">
					<li>
						<a href="https://www.facebook.com/mayoreoceramicodelapeninsula" class="facebook" title="Escribenos tus opiniones y dudas">
							<i class="fab fa-facebook-square"></i>
						</a>
					</li>
					<li>
						<a href="https://www.youtube.com/channel/UCBbSj9LP2avuxdnwapLciFw" class="youtube" title="Escribenos tus opiniones y dudas">
							<i class="fab fa-youtube"></i>
						</a>
					</li>
				</ul>
			</div>

			<div class="menu-header">
				<ul class="first-list">
					<?php $mostrarCategorias = false; 
						if(count($categorias) > 0 ){
							foreach ($categorias as $categoria){
								if(!$categoria["nuevaCategoria"]){
									$mostrarCategorias = true;
									break;
								}
							}
						}
					?>
					<?php if($mostrarCategorias) : ?>
						<li class="first-list-item nav-productos">
							<a tabindex="-1">Productos</a>
							<img class="downArrow svg" src="<?= site_url('assets/img/flotantes/down-arrow.svg')?>" alt="">
							<ul class="second-list">
								<?php foreach ($categorias as $categoria):?>
									<?php if(!$categoria["nuevaCategoria"]): ?>
										<li><a href="categoria/<?= $categoria['idCategoria']."/".mb_strtolower($categoria["nombreCategoria"]) ?>"><?= $categoria["nombreCategoria"] ?></a>
											<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
											<ul class="last-list">
												<li class="seccion">Articulos</li>
												<?php $subcategorias = $categoria["subcategorias"]; foreach($subcategorias as $subcategoria):?>
													<li ><a href="categoria/<?= $categoria['idCategoria']."/".mb_strtolower($categoria["nombreCategoria"])."/".$subcategoria['idSubcategoria']."/".mb_strtolower($subcategoria["nombreSubcategoria"]) ?>"><?= $subcategoria["nombreSubcategoria"]?></a></li>
												<?php endforeach;?>
												<img class="flotante" src="<?= $categoria["imagenCategoriaMedium"] ?>" alt="">
											</ul>
										</li>
									<?php endif; ?>
								<?php endforeach;?>
							</ul>
						</li>
					<?php endif;?>
					<?php if($mostrarRemates) : ?>
						<li class="first-list-item">
							<a href="<?= site_url('categoria/remates');?>">
								Remates 
							</a>
						</li>
					<?php endif;?>
					<li class="first-list-item">
						<a href="<?= site_url('bolsa-de-trabajo');?>">
							Bolsa de trabajo
						</a>
					</li>
					<li class="first-list-item">
						<a href="<?= site_url('asociados');?>">
							Subdistribuidores
						</a>
					</li>
					<li class="first-list-item">
						<a href="<?= site_url('contacto');?>">
							Contacto
						</a>
					</li>
				</ul>
			</div>

			<a href="tel:9999460315" class="atencion">
				<div class="icon">
					<img src="<?= site_url('assets/img/iconos/support.svg');?>" alt="">
				</div>
				<div class="texto">
					<div class="telefono">
						<span>Atención a cliente</span>
						(999) 946 03 15
					</div>
				</div>
			</a>
			
			<div class="cart">
				<a href="<?= site_url('carrito');?>">
					<div class="icon">
						<img src="<?= site_url('assets/img/iconos/shopping.svg')?>" alt="">
						<span class="cantidad" id="cantidadCarrito"><?=$cantidad_carrito?></span>
					</div>
				</a>
				<a href="<?= site_url('carrito');?>" class="cotiza">
					Cotiza Aquí
				</a>
			</div>

			<a href="tel:9999460315" class="phone">
				<div class="circle">
					<i class="fas fa-phone"></i>
				</div>
			</a>

			<div class="burger">
				<a class="mburger" href="#menu">
					<b></b>
					<b></b>
					<b></b>
				</a>
			</div>
		</div>
		
		<?php if(isset($datos_header)&&!empty($datos_header)):?>
			<div class="header-footer">
				<div class="img marquee">
					<a href="" class="camioncito">
						<video id="camionVideo" class="gif" width="100%" loop autoplay muted>
							<source src="<?= $datos_header->video_barra_file;?>" type="video/mp4">
							Your browser does not support the video tag.
						</video>
						<img class="statica" src="<?= $datos_header->imagen_barra_file;?>" alt="">
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</header>

<nav id="menu">
    <ul>
        <li><a href="/">Home</a></li>
        <li><span>Productos</span>
			<ul>
				<?php foreach ($categorias as $categoria):?>
					<?php if(!$categoria["nuevaCategoria"]): ?>
						<li><a href="http://superpisosmcp.cm/categoria/<?= $categoria['idCategoria']."/".$categoria["nombreCategoria"] ?>"> <span><?= $categoria["nombreCategoria"] ?></span> </a>
							<ul>
								<?php $subcategorias = $categoria["subcategorias"]; foreach($subcategorias as $subcategoria):?>
									<li ><a href="#"><?= $subcategoria["nombreSubcategoria"]?></a></li>
								<?php endforeach;?>
								<div class="img-cat"><img class="flotante" src="<?= $categoria["imagenCategoriaMedium"] ?>" alt=""></div>
							</ul>
						</li>
					<?php endif; ?>
				<?php endforeach;?>
			</ul>
		</li>
		<li ><a href="<?= site_url('categoria/remates');?>">Remates </a></li>
		<li ><a href="<?= site_url('bolsa-de-trabajo');?>">Bolsa de trabajo</a></li>
		<li ><a href="<?= site_url('asociados');?>">Subdistribuidores</a></li>
		<li><a href="<?= site_url('contacto');?>">Contacto</a></li>
    </ul>
</nav>