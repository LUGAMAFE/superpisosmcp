<div class="swiper-container" role="region" aria-label="Ventana Informativa" data-fancybox="ventana" id="ventana_info">
  <div class="swiper-wrapper" style="align-items: center">
    <!--<div class="orbit-controls">
      <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
      <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
    </div>-->
    <?php foreach($ventana_imagen as $imagen){ 
        $datos_imagen=json_decode($imagen["json_input_extras"]);
        if($datos_imagen->activo_ventana != 0) { ?>
            <div class="swiper-slide">
                <a href="<?=$datos_imagen->url_ventana?>" style="position:absolute;z-index:1;width:100%;height:100%"></a>
                <img src="<?=site_url($imagen["full_route_file"]);?>">
          </div>
    <?php } 
    } ?>
  </div>
  <?php if(count($ventana_imagen) > 1 ):?>
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  <?php endif; ?>
</div>
