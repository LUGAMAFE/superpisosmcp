<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bolsa extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';
	protected $fineUploaders = array(
		array("name" => "imagen-bolsa-trabajo", "nameDB" => "id_file_img_bolsa", "folder" => "../public/assets/banners/bolsa", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);

	public function __construct(){
		parent::__construct();
		$this->class = mb_strtolower(get_class());
		$this->load->model("administracion/pagina/Bolsa_Model");
		$this->load->model("administracion/Archivos_Model");
	}

	public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Pagina Bolsa');
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('bolsa.js');
		$datos["action"] = site_url("administracion/pagina/bolsa-trabajo/modificar");
		$this->template->write_view('content', $this->folder.'/pagina/bolsa', $datos);
		$this->template->render();
	}
	
	public function archivosEntrada(){
		$idUploader = $this->input->get("uploaderId");
		$output = [];

        foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$directories = glob($folder . '/*' , GLOB_ONLYDIR);
				break;
			} 
		}

		foreach ($directories as $directorio) {
			$archivos = glob($directorio.'/*.*');
			foreach ($archivos as $archivo) {
				$name = pathinfo($archivo)["filename"];
				$ends = preg_match('/\(medium\)$/', $name);
				if($ends){
					break;
				}
			}
			$uuid = pathinfo($directorio)["basename"];
			$name = str_replace(" (medium)","",pathinfo($archivo)["basename"]);
			$thumb = str_replace( "../public/" , "", $archivo);
			$archivo = (object) ["name" => $name, "uuid" => $uuid, "thumbnailUrl" => $thumb];
			$output[] = $archivo;
			if(!$uploader["multiple"]){
				break;
			}
		}
		
		$json = json_encode($output, JSON_UNESCAPED_UNICODE);
		echo $json;
    }

	public function modificar(){
		$idsImagenBanner = $this->Bolsa_Model->obtenerImagenBolsa();
		$update = true;
		if(is_null($idsImagenBanner)){
			$update = false;
		}

		$deleteFiles = [];
		$decodeToDelete = [];

		foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				if(isset($idsImagenBanner[$uploader["nameDB"]])){
					$deleteFiles[] = $idsImagenBanner[$uploader["nameDB"]];
				}
				$datos[$uploader["nameDB"]] = $ids;
				}
			}
		}

		if(isset($datos)){
			$this->Bolsa_Model->guardarImagenBolsa($datos, $update);
		}

		foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}

		$this->alertSuccess("Pagina Bolsa de Trabajo Modificada Correctamente");
		redirect('administracion/pagina');
	}

	protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
	}
	
	public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}
}