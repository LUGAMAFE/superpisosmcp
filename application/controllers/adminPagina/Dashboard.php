<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';

	public function __construct(){
		parent::__construct();
		$this->class = mb_strtolower(get_class());
	}

	public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Pagina');
		$this->loadTemplatesComunes($datos);
		$this->template->write_view('content', $this->folder.'/pagina/dashboard', $datos);
		$this->template->render();
	}
}