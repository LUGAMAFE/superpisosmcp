<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';
	protected $fineUploaders = array(
		array("name" => "video-barra-promociones", "nameDB" => "id_file_video_barra", "folder" => "../public/assets/img/Home/video/", "multiple" => false, "extensions" => array("mp4"), "sizeLimit" => 1024 * 1024 * 100 ),
		array("name" => "imagen-barra-promociones", "nameDB" => "id_file_img_barra", "folder" => "../public/assets/img/Home/barra/", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
		array("name" => "imagen-banner-principal", "nameDB" => "id_file_img_principal", "folder" => "../public/assets/img/Home/principal/", "multiple" => true, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
		array("name" => "imagen-banner-secundario", "nameDB" => "id_file_img_secundario", "folder" => "../public/assets/img/Home/secundario/", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
		array("name" => "imagen-banner-terciario", "nameDB" => "id_file_img_terciario", "folder" => "../public/assets/img/Home/terciario/", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
		array("name" => "imagen-quienes-somos", "nameDB" => "id_file_img_quienes", "folder" => "../public/assets/img/Home/quienes/", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);

	public function __construct(){
		parent::__construct();
		$this->class = mb_strtolower(get_class());
		$this->load->model("administracion/pagina/Home_Model");
		$this->load->model("administracion/Archivos_Model");
	}

	public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Pagina Home');
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('homepage.js');
		$datos["action"] = site_url("administracion/pagina/home/modificar");
		$datos["youtube_urls"] =  $this->Home_Model->obtenerYoutubeUrlsHome();
		$this->template->write_view('content', $this->folder.'/pagina/home', $datos);
		$this->template->render();
	}
	
	public function archivosEntrada(){
		$idUploader = $this->input->get("uploaderId");
		$output = [];
		if($idUploader == "imagen-banner-principal"){
			$imagenes = $this->Home_Model->obtenerImagenesBannerHome();
			foreach($imagenes as $imagen){
				$uuid = $imagen["uuid_file"];
				$name = $imagen["name_file"].".".$imagen["ext_file"];
				$thumb = $imagen["dir_file"].$imagen["name_file"]." (medium)".".".$imagen["ext_file"];
				$inputExtras = $imagen["json_input_extras"];
				$archivo = (object) ["name" => $name, "uuid" => $uuid, "thumbnailUrl" => $thumb, "inputExtras" => $inputExtras];
				$output[] = $archivo;
			}
		}else{
			foreach ($this->fineUploaders as $uploader) {
				if($uploader["name"] === $idUploader){
					$folder = $uploader["folder"];
					$directories = glob($folder . '/*' , GLOB_ONLYDIR);
					break;
				} 
			}
	
			foreach ($directories as $directorio) {
				$archivos = glob($directorio.'/*.*');
				foreach ($archivos as $archivo) {
					$name = pathinfo($archivo)["filename"];
					$ends = preg_match('/\(medium\)$/', $name);
					if($ends){
						break;
					}
				}
				$uuid = pathinfo($directorio)["basename"];
				$name = str_replace(" (medium)","",pathinfo($archivo)["basename"]);
				$thumb = str_replace( "../public/" , "", $archivo);
				$archivo = (object) ["name" => $name, "uuid" => $uuid, "thumbnailUrl" => $thumb];
				$output[] = $archivo;
				if(!$uploader["multiple"]){
					break;
				}
			}
		}
		
		$json = json_encode($output, JSON_UNESCAPED_UNICODE);
		echo $json;
    }

	public function modificar(){
		$urls_youtube = $this->input->post("urls");

		$idsImagenesHome = $this->Home_Model->obtenerImagenesHome();
		$idsBannerHome = $this->Home_Model->obtenerImagenesBannerHome();

		$update = false;

		$deleteFiles = [];
		$decodeToDelete = [];

		foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if($uploaderVariable === "imagen_banner_principal"){
				if(!empty($$uploaderVariable)){
					$decode = json_decode($$uploaderVariable);
					foreach ($decode as $key => $value) {
						if($value->deleteReal != null){
							array_push($decodeToDelete, $value);
							unset($decode[$key]);
						}
					}
					if($decode != []){
					$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
					if(!$ids){
						return;
					}
					$inputExtras = [];
					$jsonInputExtras = [];
					foreach ($decode as $imagen) {
						if(isset($banner[$uploader["nameDB"]])){
							$deleteFiles[] = $banner[$uploader["nameDB"]];
						}
						$inputExtras["url_banner"] = $imagen->inputExtras->urlBanner;
						$jsonInputExtras[] = $inputExtras;
					}
					$this->Home_Model->guardarImagenesBanner($ids, $jsonInputExtras);
					}
				}
				continue;
			}
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				if(isset($idsImagenesHome[$uploader["nameDB"]])){
					$deleteFiles[] = $idsImagenesHome[$uploader["nameDB"]];
				}
				$datos[$uploader["nameDB"]] = $ids;
			}else{$update = true;} 
		}

		if(isset($datos)){
			$this->Home_Model->guardarImagenesHome($datos, $update);
		}

		/*foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}*/

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}

		$this->Home_Model->guardarUrlsYoutube($urls_youtube);

		$this->alertSuccess("Pagina Home Modificada Correctamente");
		redirect('administracion/pagina');
	}

	protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
	}
	
	public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}
}