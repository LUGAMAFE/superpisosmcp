<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ventana  extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';
	protected $fineUploaders = array(
		array("name" => "imagen-ventana-informativa", "nameDB" => "id_file_img_ventana", "folder" => "../public/assets/banners/ventana", "multiple" => true, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);

	public function __construct(){
		parent::__construct();
		$this->class = mb_strtolower(get_class());
		$this->load->model("administracion/pagina/Ventana_Model");
		$this->load->model("administracion/Archivos_Model");
	}

	public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Pagina Ventana');
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('ventana.js');
		$datos["action"] = site_url("administracion/pagina/ventana-info/modificar");
		$datos["is_activo"] = $this->Ventana_Model->obtenerIsActivo();
		$this->template->write_view('content', $this->folder.'/pagina/ventana', $datos);
		$this->template->render();
	}
	
	public function archivosEntrada(){
		$idUploader = $this->input->get("uploaderId");
		$output = [];

        $imagenes = $this->Ventana_Model->obtenerImagenesVentana();
        foreach($imagenes as $imagen){
            $uuid = $imagen["uuid_file"];
            $name = $imagen["name_file"].".".$imagen["ext_file"];
			$thumb = $imagen["dir_file"].$imagen["name_file"]." (medium)".".".$imagen["ext_file"];
			$inputExtras = $imagen["json_input_extras"];
            $archivo = (object) ["name" => $name, "uuid" => $uuid, "thumbnailUrl" => $thumb, "inputExtras" => $inputExtras];
            $output[] = $archivo;
        }
    
		$json = json_encode($output, JSON_UNESCAPED_UNICODE);
		echo $json;
    }

	public function modificar(){
		$idsImagenBanner = $this->Ventana_Model->obtenerImagenesVentana();
		$datos["is_activo"] = $this->input->post("all-activo-ventana") === "on" ? 1 : 0;
		$update = true;
		if(is_null($idsImagenBanner)){
			$update = false;
		}

		$this->Ventana_Model->guardarIsActivo($datos["is_activo"]);

		$deleteFiles = [];
		$decodeToDelete = [];

		foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$uploaderVariableV = $this->input->post($uploader["name"]."-info");
			if(!empty($uploaderVariableV)){
				$decode = json_decode($uploaderVariableV);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
                }
				$inputExtras = [];
				$jsonInputExtras = [];
                foreach ($decode as $imagen) {
                    if(isset($banner[$uploader["nameDB"]])){
                        $deleteFiles[] = $banner[$uploader["nameDB"]];
                    }
					$inputExtras["url_ventana"] = $imagen->inputExtras->url_ventana;
					$inputExtras["activo_ventana"] = $imagen->inputExtras->activo_ventana;
					$jsonInputExtras[] = $inputExtras;
                }
                $this->Ventana_Model->guardarImagenesVentana($ids, $jsonInputExtras);
				//$datos[$uploader["nameDB"]] = $ids;
				}
			}
		}

		/*if(isset($datos)){
			$this->Ventana_Model->guardarImagenesVentana($datos, $update);
		}*/

		foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}

		$this->alertSuccess("Pagina Pop-up Modificada Correctamente");
		redirect('administracion/pagina');
	}

	protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
	}
	
	public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}
}