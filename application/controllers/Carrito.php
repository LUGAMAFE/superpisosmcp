<?php

use Spipu\Html2Pdf\Html2Pdf;

$this->load->model("Carrito");
$productosCarrito = isset($_COOKIE['productos_carrito']) ? $_COOKIE['productos_carrito'] : null;
$productos_carrito = $this->Carrito->obtenerProductosCarrito($productosCarrito);
$cantidades_carrito = isset($_COOKIE['cantidades_carrito']) ? json_decode($_COOKIE['cantidades_carrito']) : [];

ob_start();
require_once '/site/carrito/cotizacion.php';
$html = ob_get_clean();

$html2pdf = new Html2Pdf('P','A4','es','true','UTF-8');
$html2pdf->writeHTML($html);
$html2pdf->output('cotizacion_mcp.pdf');