<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Spipu\Html2Pdf\Html2Pdf;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require '../vendor/autoload.php';

class Home extends MY_Controller {

	protected $class = '';
	protected $folder = '/site';
	protected $folder_set = '/site/partials/';
	protected $object;
	protected $url_recuperar;
	
	public function __construct() {
		parent::__construct();
		$this->class = mb_strtolower(get_class());
		$this->load->model("administracion/Categorias_Model");
		$this->load->model("administracion/pagina/Home_Model");
		$this->load->model("Carrito");
	}

	public function index($url = '') {
		$this->load->model("administracion/pagina/Ventana_Model");
		$this->load->model("administracion/Productos_Model");
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$datos['info_home'] = $this->Home_Model->get_home();
		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['ventana_imagen'] = $this->Ventana_Model->obtenerImagenesActivasVentana();
		$activarVentana = $this->Ventana_Model->obtenerIsActivo() === "1" ? true : false;
		$categoriasHome = $this->Categorias_Model->obtenerListaCategoriasParaHome();
		$datos['categorias_home'] = json_decode($categoriasHome);
		$pMasVendidos = $this->Productos_Model->obtenerListaProductosMasVendidosParaHome();
		$datos['p_mas_vendidos'] = json_decode($pMasVendidos);
		$pDelRey = $this->Productos_Model->obtenerListaProductosDelReyParaHome();
		$datos['p_del_rey'] = json_decode($pDelRey);
		$datos['class'] = $this->class;
		$banner['banner'] = 1;
		$datos['active'] = 'inicio';
		$this->template->write('title', 'Home');
		$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
		$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
		$this->template->asset_js('swiper/package/js/swiper.min.js');
		$this->template->asset_css('swiper/package/css/swiper.min.css');
		$this->template->asset_js('scrollbar/jquery.scrollbar.min.js');
		$this->template->asset_js('svg converter.js');
		$this->template->asset_css('scrollbar/jquery.scrollbar.css');
		$this->template->asset_js('jbox/dist/jBox.all.min.js');
		$this->template->asset_css('jbox/dist/jBox.all.min.css');
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $banner);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/home/list', $datos);
		$this->template->write_view('contacto', $this->folder_set.'contacto', $datos);
		if($activarVentana && !empty($datos['ventana_imagen']) && !isset($_COOKIE['viewed_ventana'])) {
			$ahora = strtotime("now");
			$limite = strtotime("tomorrow");
			$differenceInSeconds = $limite - $ahora;
			setcookie('viewed_ventana', true, time() + ($differenceInSeconds), "/");
			$this->template->asset_js('ventana.js');
			$this->template->write_view('ventana_info', $this->folder_set.'ventana_info', $datos);
		}
		$this->template->render();
	}

	public function categoria($idCategoria = "", $categoria = "", $idSubcategoria = "", $subcategoria = "") {
		$categoriaDatos["nombre"] = urldecode($categoria);
		$categoria = urldecode($categoria);
		$subcategoria = urldecode($subcategoria);
		$categoriaDatos["id"] = $idCategoria;
		$existeCategoria =  $this->Categorias_Model->existeCategoria($categoriaDatos);
		if($categoria != "remates" && !$existeCategoria){
			show_404();
		}
		$this->load->model("administracion/pagina/Remates_Model");
		$this->load->model("administracion/pagina/Ventana_Model");
		$this->load->model("administracion/Productos_Model");
		$datos['class'] = $this->class;
		$banner['banner'] = 2;
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['active'] = 'categoria';
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$this->load->model("administracion/pagina/Remates_Model");
		$this->load->model("administracion/pagina/Ventana_Model");
		$datos['ventana_imagen'] = $this->Ventana_Model->obtenerImagenesVentana();
		$activarVentana = $this->Ventana_Model->obtenerIsActivo() === "1" ? true : false;

		
		if($categoria == "remates"){
			$datos['banner_categoria'] = $this->Remates_Model->get_banner();
			if(is_null($datos['banner_categoria'])){
				show_404();
			}
			$datos['productos'] = $this->Productos_Model->getProductosEnRemate();
			$datos['datosCategoria']["nombreCategoria"] = "Remates";
			$datos['datosCategoria']["informacionCategoria"] = "Remates";
			$banner['texto_categoria'] = "Nuestros Remates";
		}else{
			foreach ($datos['categorias'] as $cat) {
				if(mb_strtolower($cat["nombreCategoria"]) == mb_strtolower($categoriaDatos["nombre"])){
					$datos['datosCategoria'] = $cat;
				}
			}
			$banner['texto_categoria'] = $datos["datosCategoria"]["infoBannerCategoria"];
			$datos['banner_categoria'] = $datos['datosCategoria']["imagenBannerOriginal"];
			if($idSubcategoria != ""){
				$existeSub = false;
				foreach($datos['datosCategoria']['subcategorias'] as $subcat){
					if($subcat['idSubcategoria'] == $idSubcategoria && mb_strtolower($subcat['nombreSubcategoria']) == mb_strtolower($subcategoria)){
						$existeSub = true;
					}
				}
				if(!$existeSub){
					show_404();
				}
				$datos['productos'] = $this->Productos_Model->getProductosSubcategoria($subcategoria);
			} else {
				$datos['productos'] = $this->Productos_Model->getProductosCategoria($categoria);
			}
			$datos['productos'] = $this->Productos_Model->obtenerFiltros($datos['productos']);
			$datos['filtrosJson'] = json_encode($datos['datosCategoria']['filtros_categoria']);
		}

		$pRecomendados = $this->Productos_Model->obtenerListaProductosRecomendados();
		$datos['productos_recomendados'] = json_decode($pRecomendados);
		$datos['productosJson'] = json_encode($datos['productos']);

		$this->template->write('title', 'Categoria');
		$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
		$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
		$this->template->asset_js('swiper/package/js/swiper.min.js');
		$this->template->asset_css('swiper/package/css/swiper.min.css');
		$this->template->asset_js('jbox/dist/jBox.all.min.js');
		$this->template->asset_css('jbox/dist/jBox.all.min.css');
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $banner);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/categorias/list', $datos);

		if($activarVentana && !empty($datos['ventana_imagen']) && !isset($_COOKIE['viewed_ventana'])) {
			$ahora = strtotime("now");
			$limite = strtotime("tomorrow");
			$differenceInSeconds = $limite - $ahora;
			setcookie('viewed_ventana', true, time() + ($differenceInSeconds), "/");
			$this->template->asset_js('ventana.js');
			$this->template->write_view('ventana_info', $this->folder_set.'ventana_info', $datos);
		}
		$this->template->render();
	}

	public function info_producto($idProducto = "", $nombreProducto = "")
	{
		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		// $idProducto = urldecode ($idProducto);
		// $nombreProducto = urldecode ($nombreProducto);
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['class'] = $this->class;
		$datos['active'] = 'producto';
		$datos['id'] = $idProducto;
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$this->load->model("administracion/Productos_Model");
		$productoExists = $this->Productos_Model->getProducto($datos);
		if(empty($productoExists)){
			$this->template->write('title', 'Producto');
			$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
			$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
			$this->template->write_view('footer', $this->folder_set.'footer', $datos);
			$this->template->write_view('content', $this->folder.'/producto/unavailable', $datos);
		} else {
			$datos['datosProducto'] = $this->Productos_Model->obtenerInfoProductoCompleta($idProducto);
			if(empty($datos['datosProducto'])){
				$this->template->write('title', 'Producto');
				$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
				$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
				$this->template->write_view('footer', $this->folder_set.'footer', $datos);
				$this->template->write_view('content', $this->folder.'/producto/unavailable', $datos);
			} else {
				$datos['detallesProducto'] = $this->Productos_Model->obtenerDetallesProducto($datos['datosProducto']);
				$datos['imagenesExtra'] = $this->Productos_Model->getImagenesOrdenadasProducto($idProducto);
				$datos['imagenesUsos'] = $this->Productos_Model->getImagenesCasosProducto($idProducto);
				$idsMcp = json_decode($datos['datosProducto']['productos_asociados']);
				$datos['productosRelacionados'] = $this->Productos_Model->obtenerProductosRelacionadosParaInfoProducto($idsMcp);
				$meta['titulo'] = $datos['datosProducto']['nombre_producto'];
				$meta['desc'] = $datos['datosProducto']['desc_prod'];
				$meta['img'] = $datos['datosProducto']['imagen_producto'];
				$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
				$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
				$this->template->asset_js('swiper/package/js/swiper.min.js');
				$this->template->asset_css('swiper/package/css/swiper.min.css');
				$this->template->asset_css('drift/drift-basic.css');
				$this->template->asset_js('drift/Drift.js');
				$this->template->asset_js('jbox/dist/jBox.all.min.js');
				$this->template->asset_css('jbox/dist/jBox.all.min.css');
				$this->template->asset_js('producto.js');
				$this->template->write('title', 'Producto');
				$this->template->write_view('metadatos', $this->folder_set.'metadatos', $meta);
				$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
				$this->template->write_view('footer', $this->folder_set.'footer', $datos);
				$this->template->write_view('content', $this->folder.'/producto/list', $datos);
			}
		}
		$this->template->render();
	}

	public function bolsa_trabajo()
	{
		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['class'] = $this->class;
		$datos['active'] = 'bolsa';
		$banner['banner'] = 3;
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$this->load->model("administracion/pagina/Bolsa_Model");
		$this->load->model("administracion/pagina/Ventana_Model");
		$datos['banner_bolsa'] = $this->Bolsa_Model->get_banner();
		$datos['ventana_imagen'] = $this->Ventana_Model->obtenerImagenesVentana();
		$activarVentana = $this->Ventana_Model->obtenerIsActivo() === "1" ? true : false;
		$this->template->write('title', 'Bolsa de Trabajo');
		$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
		$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
		$this->template->asset_js('swiper/package/js/swiper.min.js');
		$this->template->asset_css('swiper/package/css/swiper.min.css');
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $banner);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('sucursales', $this->folder_set.'sucursales', $datos);
		$this->template->write_view('content', $this->folder.'/bolsa/list', $datos);
		if($activarVentana && !empty($datos['ventana_imagen']) && !isset($_COOKIE['viewed_ventana'])) {
			$ahora = strtotime("now");
			$limite = strtotime("tomorrow");
			$differenceInSeconds = $limite - $ahora;
			setcookie('viewed_ventana', true, time() + ($differenceInSeconds), "/");
			$this->template->asset_js('ventana.js');
			$this->template->write_view('ventana_info', $this->folder_set.'ventana_info', $datos);
		}
		$this->template->render();
	}

	public function socios()
	{
		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['class'] = $this->class;
		$datos['active'] = 'socios';
		$this->load->model("administracion/pagina/Subdist_Model");
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$this->load->model("administracion/pagina/Ventana_Model");
		$datos['ventana_imagen'] = $this->Ventana_Model->obtenerImagenesVentana();
		$activarVentana = $this->Ventana_Model->obtenerIsActivo() === "1" ? true : false;
		$datos['imagen_socios'] = $this->Subdist_Model->obtenerImagenSubdist();
		$this->template->write('title', 'Socios');
		$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
		$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
		$this->template->asset_js('swiper/package/js/swiper.min.js');
		$this->template->asset_css('swiper/package/css/swiper.min.css');
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('sucursales', $this->folder_set.'sucursales', $datos);
		$this->template->write_view('content', $this->folder.'/socios/list', $datos);
		if($activarVentana && !empty($datos['ventana_imagen']) && !isset($_COOKIE['viewed_ventana'])) {
			$ahora = strtotime("now");
			$limite = strtotime("tomorrow");
			$differenceInSeconds = $limite - $ahora;
			setcookie('viewed_ventana', true, time() + ($differenceInSeconds), "/");
			$this->template->asset_js('ventana.js');
			$this->template->write_view('ventana_info', $this->folder_set.'ventana_info', $datos);
		}
		$this->template->render();
	}

	public function contacto()
	{
		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['class'] = $this->class;
		$datos['active'] = 'contacto';
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$this->load->model("administracion/pagina/Ventana_Model");
		$datos['ventana_imagen'] = $this->Ventana_Model->obtenerImagenesVentana();
		$activarVentana = $this->Ventana_Model->obtenerIsActivo() === "1" ? true : false;
		$this->template->write('title', 'Contacto');
		$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
		$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
		$this->template->asset_js('swiper/package/js/swiper.min.js');
		$this->template->asset_css('swiper/package/css/swiper.min.css');
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('contacto', $this->folder_set.'contacto', $datos);
		if($activarVentana && !empty($datos['ventana_imagen']) && !isset($_COOKIE['viewed_ventana'])) {
			$ahora = strtotime("now");
			$limite = strtotime("tomorrow");
			$differenceInSeconds = $limite - $ahora;
			setcookie('viewed_ventana', true, time() + ($differenceInSeconds), "/");
			$this->template->asset_js('ventana.js');
			$this->template->write_view('ventana_info', $this->folder_set.'ventana_info', $datos);
		}
		$this->template->render();
	}

	public function carrito(){
		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$this->load->model("Carrito");
		if(isset($_POST['guardar_carrito'])) {
			$guardado = $this->Carrito->guardarProductoCarrito("'".$_POST['id_producto']."'",$_POST['cantidad_producto']);
			echo $this->Carrito->obtenerCantidadCarrito($guardado,false);
	   	} else if(isset($_POST['eliminar_carrito'])) {
			$eliminado = $this->Carrito->eliminarProductoCarrito("'".$_POST['id_producto']."'");
			echo $this->Carrito->obtenerCantidadCarrito(false,$eliminado);
	   	} else {
			$datos['class'] = $this->class;
			$datos['active'] = 'carrito';
			$this->load->model("administracion/pagina/Cotizacion_Model");
			$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
			$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
			$datos['banner_carrito'] = $this->Cotizacion_Model->obtenerImagenCotizacion();
			$productosCarrito = isset($_COOKIE['productos_carrito']) ? $_COOKIE['productos_carrito'] : null;
			$datos['productos_carrito'] = $this->Carrito->obtenerProductosCarrito($productosCarrito);
			$datos['cantidades_carrito'] = isset($_COOKIE['cantidades_carrito']) ? json_decode($_COOKIE['cantidades_carrito']) : [];
			$datos['productos_relacionados_carrito'] = $this->Carrito->obtenerProductosRelacionadosCarrito($datos['productos_carrito']);
			$this->template->write('title', 'Carrito');
			$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
			$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
			$this->template->asset_js('jbox/dist/jBox.all.min.js');
			$this->template->asset_css('jbox/dist/jBox.all.min.css');
			$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
			$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
			$this->template->write_view('footer', $this->folder_set.'footer', $datos);
			$this->template->write_view('content', $this->folder.'/carrito/list', $datos);
			$this->template->render();
		}
	}

	public function admin(){
		$this->load->view('categorias');
	}

	private function mostrarRemates(){
		$this->load->model("administracion/Productos_Model");
		$this->load->model("administracion/pagina/Remates_Model");
		return ($this->Productos_Model->countProductosEnRemate()->COUNT > 0) && !is_null($this->Remates_Model->get_banner());
	}

	public function eliminar_tildes($cadena){

		//Codificamos la cadena en formato utf8 en caso de que nos de errores
		//$cadena = utf8_encode($cadena);
	
		//Ahora reemplazamos las letras
		$cadena = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$cadena
		);
	
		$cadena = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$cadena );
	
		$cadena = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$cadena );
	
		$cadena = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$cadena );
	
		$cadena = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$cadena );
	
		$cadena = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C'),
			$cadena
		);
	
		return $cadena;
	}

	public function enviarCotizacion(){

		$this->load->model("Carrito");
		$productosCarrito = isset($_COOKIE['productos_carrito']) ? $_COOKIE['productos_carrito'] : null;
		$datos['productos_carrito'] = $this->Carrito->obtenerProductosCarrito($productosCarrito);
		$datos['cantidades_carrito'] = isset($_COOKIE['cantidades_carrito']) ? json_decode($_COOKIE['cantidades_carrito']) : [];

		$html = $this->load->view($this->folder.'/carrito/cotizacion.php', $datos, TRUE);

		$html2pdf = new Html2Pdf('P','A4','es','true','UTF-8');
		$html2pdf->writeHTML($html);
		$pdf = $html2pdf->Output('cotizacion_MCP.pdf', 'S');
		
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom("info@superpisosmcp.com", "Cotización MCP");
		$email->setSubject("Cotización MCP");
		$email->addTo("dgrafico@superpisosmcp.com", "Superpisos MCP");
		$email->addTo("cotizaciones@superpisosmcp.com", "Superpisos MCP");
		$email->addContent(
			"text/html", "<h1>Nueva Cotización MCP</h1><div>
            <h4>Datos de Contacto de Cliente: </h4>
            Nombre:  ".$_POST['nombre']."<br>
            Apellidos:  ".$_POST['apellido']."<br>
            E-mail:  ".$_POST['correo']."<br>
            Teléfono: ".$_POST['telefono']."
        </div>"
		);

		$file_encoded = base64_encode($pdf);
		$email->addAttachment(
			$file_encoded,
			"application/pdf",
			"Cotizacion_MCP.pdf",
			"attachment"
		);

		$sendgrid = new \SendGrid('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');
		try {
			$response = $sendgrid->send($email);
		} catch (Exception $e) {
			echo 'Caught exception: '.  $e->getMessage(). "\n";
		}

		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$datos['class'] = $this->class;
		$datos['active'] = 'enviarCotizacion';
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/carrito/gracias', $datos);
		$this->template->render();
	}

	public function enviarContacto(){

		$this->load->model("Carrito");
		$productosCarrito = isset($_COOKIE['productos_carrito']) ? $_COOKIE['productos_carrito'] : null;
		$datos['productos_carrito'] = $this->Carrito->obtenerProductosCarrito($productosCarrito);
		$datos['cantidades_carrito'] = isset($_COOKIE['cantidades_carrito']) ? json_decode($_COOKIE['cantidades_carrito']) : [];

		$html = $this->load->view($this->folder.'/contacto/contacto.php', $datos, TRUE);
		
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom("info@superpisosmcp.com", "Contacto MCP");
		$email->setSubject("Contacto MCP");
		$email->addTo("dgrafico@superpisosmcp.com", "Superpisos MCP");
		$email->addContent(
			"text/html", $html
		);

		$sendgrid = new \SendGrid('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');
		try {
			$response = $sendgrid->send($email);
		} catch (Exception $e) {
			echo 'Caught exception: '.  $e->getMessage(). "\n";
		}

		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$datos['class'] = $this->class;
		$datos['active'] = 'enviarContacto';
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/contacto/gracias', $datos);
		$this->template->render();
	}

	public function enviarSocio(){

		$this->load->model("Carrito");
		$productosCarrito = isset($_COOKIE['productos_carrito']) ? $_COOKIE['productos_carrito'] : null;
		$datos['productos_carrito'] = $this->Carrito->obtenerProductosCarrito($productosCarrito);
		$datos['cantidades_carrito'] = isset($_COOKIE['cantidades_carrito']) ? json_decode($_COOKIE['cantidades_carrito']) : [];

		$html = $this->load->view($this->folder.'/socios/socio.php', $datos, TRUE);
		
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom("info@superpisosmcp.com", "Subdistribuidor MCP");
		$email->setSubject("Solicitud Subdistribuidor MCP");
		$email->addTo("alberto.abimerhi@superpisosmcp.com", "Superpisos MCP");
		$email->addContent(
			"text/html", $html
		);

		$sendgrid = new \SendGrid('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');
		try {
			$response = $sendgrid->send($email);
		} catch (Exception $e) {
			echo 'Caught exception: '.  $e->getMessage(). "\n";
		}

		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$datos['class'] = $this->class;
		$datos['active'] = 'enviarSocio';
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/socios/gracias', $datos);
		$this->template->render();
	}
	
	public function enviarTrabajo(){

		$this->load->model("Carrito");
		$productosCarrito = isset($_COOKIE['productos_carrito']) ? $_COOKIE['productos_carrito'] : null;
		$datos['productos_carrito'] = $this->Carrito->obtenerProductosCarrito($productosCarrito);
		$datos['cantidades_carrito'] = isset($_COOKIE['cantidades_carrito']) ? json_decode($_COOKIE['cantidades_carrito']) : [];

		$html = $this->load->view($this->folder.'/bolsa/reclutamiento.php', $datos, TRUE);
		
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom("info@superpisosmcp.com", "Bolsa MCP");
		$email->setSubject("Solicitud Trabajo MCP");
		$email->addTo("rh@superpisosmcp.com", "Superpisos MCP");
		$email->addContent(
			"text/html", $html
		);

		$curriculum = file_get_contents($_FILES["curiculum"]["tmp_name"]);

		$file_encoded = base64_encode($curriculum);
		$email->addAttachment(
			$file_encoded,
			"application/pdf",
			$_FILES['curiculum']['name'],
			"attachment"
		);

		$sendgrid = new \SendGrid('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');
		try {
			$response = $sendgrid->send($email);
		} catch (Exception $e) {
			echo 'Caught exception: '.  $e->getMessage(). "\n";
		}

		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$datos['class'] = $this->class;
		$datos['active'] = 'enviarTrabajo';
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/bolsa/gracias', $datos);
		$this->template->render();
	}

	public function buscarTermino(){
		$datos['datos_header'] = $this->Home_Model->obtenerArchivosHeader();
		$datos['mostrarRemates'] =  $this->mostrarRemates();
		$datos['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
		$datos['cantidad_carrito'] = $this->Carrito->obtenerCantidadCarrito();
		$datos['class'] = $this->class;
		$datos['active'] = 'buscarTermino';
		$datos['termino'] = isset($_GET['busqueda_global']) ? $_GET['busqueda_global'] : "";
		//$datos['termino'] = $this->eliminar_tildes($datos['termino']);
		$datos['productos'] = $this->Productos_Model->getProductosBusqueda($datos['termino']);
		$this->template->asset_js('jbox/dist/jBox.all.min.js');
		$this->template->asset_css('jbox/dist/jBox.all.min.css');
		$this->template->write_view('metadatos', $this->folder_set.'metadatos', $datos);
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/buscar/list', $datos);
		$this->template->render();
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
