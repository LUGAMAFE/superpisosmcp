<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends MY_Controller {

	protected $class = '';
	protected $folder = '/site';
	protected $folder_set = '/site/partials/';
	protected $object;
    protected $url_recuperar;

    public function __construct() {
		parent::__construct();
		$this->class = mb_strtolower(get_class());
		$this->load->model('Usuarios_Model');
    }
    
    public function index($url = ''){
		$this->load->model("administracion/pagina/Home_Model");
		$datos['info_home'] = $this->Home_Model->get_home();
		$datos['class'] = $this->class;
		$banner['banner'] = 1;
		$datos['active'] = 'inicio';
		$this->template->write('title', 'Home');
		$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
		$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
		$this->template->asset_js('scrollbar/jquery.scrollbar.min.js');
		$this->template->asset_js('svg converter.js');
		$this->template->asset_css('scrollbar/jquery.scrollbar.css');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $banner);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/home/list', $datos);
		$this->template->render();
	}
    
}