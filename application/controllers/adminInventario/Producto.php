<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Producto extends MY_Admin {

    protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';
	protected $fineUploaders = array(
		array("name" => "imagenes-producto", "nameDB" => "id_file_img_producto", "folder" => "../public/assets/img/productos/producto", "multiple" => true, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
		array("name" => "imagenes-uso", "nameDB" => "id_file_img_producto", "folder" => "../public/assets/img/productos/casos", "multiple" => true, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);

	public function __construct(){
		parent::__construct();
		$this->class = mb_strtolower(get_class());
        $this->load->model("administracion/Archivos_Model");
        $this->load->model("administracion/Productos_Model");
	}

    public function index(){
		$datos['id'] = $this->input->get('id');
		$datos["name"] = $this->input->get('name');
		if(empty($datos['id']) || empty($datos["name"])){
			show_404();
		}
		$datos['datos'] = $this->Productos_Model->getProducto($datos);
		$datos['filtrosPrevios'] = json_decode($datos['datos']['filtros_producto']);
		$datos['filtros'] = $this->Productos_Model->obtenerFiltrosProducto($datos["id"]);
		$datos['class'] = $this->class;
		$datos['productosRelacionados'] = $this->Productos_Model->obtenerProductosRelacionados(json_decode($datos["datos"]["productos_asociados"]), $datos["id"]);
		$datos['productosListaParaRelacionados'] = $this->Productos_Model->obtenerListaProductosParaRelacionados($datos['id']);
		$this->template->write('title', 'Admin Producto');
        $this->loadTemplatesComunes($datos);
        $this->template->asset_css('summernote/summernote-bs4.css');
        $this->template->asset_js('summernote/summernote-bs4.min.js');
		$this->template->asset_js('summernote/lang/summernote-es-ES.js');
		$this->template->asset_js('node_modules/fuse.js/dist/fuse.js');
        $this->template->asset_js('adminproducto.js');
        $datos['action'] = "administracion/inventario/productos/producto/modificar?id=".$datos["id"];
        $datos['cancelar'] = "administracion/inventario/productos";
		$this->template->write_view('content', $this->folder.'/productos/producto', $datos);
		$this->template->render();
    }

    public function archivosEntrada(){
        $idUploader = $this->input->get("uploaderId");
        $idProducto = $this->input->get("idProducto");
        $output = [];
        switch ($idUploader) {
            case 'imagenes-producto':
                $archivos = $this->Productos_Model->getImagenesProducto($idProducto);
                break;
            case 'imagenes-uso':
                $archivos = $this->Productos_Model->getImagenesCasosProducto($idProducto);
                break;
        }
        foreach ( $archivos as $archivo) {
			$ruta = $archivo["dir_file"] . $archivo["name_file"] . " (medium).". $archivo["ext_file"];
			$inputExtras = $archivo["json_input_extras"];
            $file = (object) ["name" => $archivo["name_file"].".".$archivo["ext_file"], "uuid" => $archivo["uuid_file"], "thumbnailUrl" => $ruta, "inputExtras" => $inputExtras];
            $output[] = $file;
        }
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

	public function modificar(){
		$id = $this->input->post("id-producto");
		$datos["nombre_producto"] = $this->input->post("nombre");
		$datos["descripcion_producto"] = $this->input->post("descripcion");
		$datos["informacion_extra_producto"] = $this->input->post("informacion");
		$datos["especificaciones_producto"] = $this->input->post("especificaciones");
		$datos["filtros_producto"] = $this->input->post("datos-filtro");
		$datos["visible_prod"] = $this->input->post("input-producto-mostrar") === "on" ? 1 : 0;
		$datos["producto_rey"] = $this->input->post("input-producto-rey")  === "on" ? 1 : 0;
		$datos["producto_mas_vendido"] = $this->input->post("input-producto-mas-vendido")  === "on" ? 1 : 0;
		$datos["descuento_activado_producto"] = $this->input->post("input-producto-descuento")  === "on" ? 1 : 0;
		$datos["nuevo_prod"] = "0";
		if($datos["descuento_activado_producto"] == 1){
			$datos["descuento_producto"] = $this->input->post("descuento");
		}
		$datos["productos_asociados"] = $this->input->post("input-productos-asociados");
		$deleteFiles = [];
		$decodeToDelete = [];

		foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$algo = $this->input->post($uploader["name"]."-info");
			if(!empty($algo)){
				$decode = json_decode($algo);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
					$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
					if(!$ids){
						return;
					}
					$inputExtras = [];
					$jsonInputExtras = [];
					if($uploaderVariable === "imagenes_producto"){
						foreach ($decode as $imagen) {
							$inputExtras = $imagen->inputExtras;
							$jsonInputExtras[] = $inputExtras;
						}
						$this->Productos_Model->emptyImagenesProducto($id);
						$this->Productos_Model->guardarImagenesProducto($id, $ids, $jsonInputExtras);
					}else{
						$this->Productos_Model->emptyImagenesCasosProducto($id);
						$this->Productos_Model->guardarImagenesCasos($id, $ids, $jsonInputExtras);
					}
				}
			}
		}

		if(isset($datos)){
			$this->Productos_Model->actualizarProducto($datos, $id);
		}

		foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}

		$this->alertSuccess("Producto $id Modificado Correctamente");
		redirect('administracion/inventario/productos');
    }
    
    protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
	}

	
	public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}
}