<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function key_compare_func($a, $b){
    $keyA = $a["Articulo"];
    $keyB = $b["Articulo"];

    if ( $keyA === $keyB ) {
        return 0;
    }

    return strnatcmp($keyA, $keyB);
}

function key_compare_func2($a, $b){
    if ( $a["Articulo"] === $b["Articulo"] && $a["Grupo"] === $b["Grupo"] && $a["Descripcion1"] === $b["Descripcion1"] && $a["Subcategoria"] === $b["Subcategoria"]) {
        return 0;
    }else{
        $strcmpArticulo = strnatcmp($a["Articulo"], $b["Articulo"]);
        if($strcmpArticulo === 0){
            return strnatcmp($a["Descripcion1"], $b["Descripcion1"]);
        }        
        return $strcmpArticulo;
    }
}

class Productos extends MY_Admin {

    public function __construct(){
        parent::__construct();
        $this->load->model('administracion/Productos_Model');
        $this->load->model('administracion/Actualizacion_Model');
        $this->load->model('administracion/Subcategorias_Model');
        $this->load->helper('url_helper');
    }

    public function datatable(){
        $post = $this->input->post();
        $total = $this->Productos_Model->count();
        $filters = array();
        if(isset($post["start"])){
            $filters = array(
                "offset" => $post["start"],
                "length" => $post["length"],
            );
        }
        
        if(isset($post["search"]["value"])){
            trim($post["search"]["value"]);
            if($post["search"]["value"] !== ""){
                $filters["search"] = preg_replace('!\s+!', ' ', $post["search"]["value"]);
            }
        }
        $columnas = array(
            'id_mcp',
            'desc_mcp',
            'nombre_categoria',
            'nombre_subcategoria',
            'visible_prod',
            'nuevo_prod',
        );
        if(isset($post["order"][0]["column"])){
            $columnNumber = $post["order"][0]["column"];
            if(isset($columnas[$columnNumber])){
                $filters["order"]["column"] = $columnas[$columnNumber];
                $filters["order"]["dir"] = strtoupper($post["order"][0]["dir"]);
            }
        }
        
        $registros  = $this->Productos_Model->obtenerProductosTabla($filters, $columnas);
        $productos = array();
        $id = 0;
        foreach($registros as $registro){
            $producto = new stdClass();
            $producto->articulo        = $registro["id_mcp"];
            $producto->nombre_articulo = $registro["desc_mcp"];
            $producto->categoria       = $registro["nombre_categoria"];
            $producto->subcategoria           = $registro["nombre_subcategoria"];
            $datacheck = $registro["visible_prod"] === "Mostrar" ? 'datachecked' : '';
            $textMostrar = $registro["visible_prod"] === "Mostrar" ? 'mostrar' : 'ocultar';
            $producto->mostrar         = "<input data-check-id= '$id' type='checkbox' class='check-mostrar $datacheck'> <span class='mostrar'> $textMostrar </span>";
            $producto->nuevo_prod = ($registro['nuevo_prod'] == 0) ? "<div class='text-success'><i class='fas fa-check'></i><span> Con Datos</span></div>" : "<div class='text-danger'> <i class='fas fa-times'></i> <span> Sin Datos</span></div>";
            $idMCP = $producto->articulo;
            $nombreArt = $producto->nombre_articulo;
            $urlEdit = site_url('administracion/inventario/productos/producto').'?id='.htmlspecialchars(trim($idMCP)).'&name='.htmlspecialchars(trim($nombreArt));
            $producto->info            = "<a href='$urlEdit'><button attr-id-prod='$idMCP' class='btnEditar btn btn-info'>Editar Producto</button></a>";
            $productos[] = $producto;
            $id++;
        }

        $respuesta = array(
            "data" => $productos,
        );
        header('Content-Type: application/json');
        $json = json_encode($respuesta, JSON_UNESCAPED_UNICODE);
        echo $json;
    }


    public function actualizarRegistros() {
        $this->load->model('administracion/Categorias_Model');
        //productos = @file_get_contents('http://slim.test/productosInfo');
        $productos = @file_get_contents('http://189.202.21.252:3600/slim/public/productosInfo');
        if($productos == FALSE){
            return array(
                "error" => TRUE,
                "mensaje" => "Imposible Actualizar Los Productos desde el WebService", 
                "title" => "Puedes Seguir Trabajando e intentar de nuevo Luego",
                "type" => "error",
            );
        }
        //$time_pre = microtime(true);
        $productos = json_decode($productos, true);
        if($productos["error"] == TRUE){
            return array(
                "error" => TRUE,
                "mensaje" => "Imposible Interpretar los datos del WebService", 
                "title" => "Puedes Seguir Trabajando e intentar de nuevo Luego",
                "type" => "error",
            );
        }
        $productos = $productos["resultado"];
        $productosDB = $this->Productos_Model->getProductosInfo();

        $nuevosProductos = array_udiff($productos, $productosDB, "key_compare_func");
        $this->Productos_Model->insertarNuevosProductos($nuevosProductos);
        
        $interseccion = array_uintersect($productos, $productosDB, "key_compare_func");
        
        $productosCambios = $interseccion;
        foreach ($productosCambios as $key => $value) {
            $clave = array_search($value, $productosDB);
            if($clave != false){
                unset($productosCambios[$key]);
            }
        }
        //$productosCambios = array_udiff($interseccion, $productosDB, "key_compare_func2");

        $this->Productos_Model->actualizarProductos($productosCambios);
        //$time_post = microtime(true);
        //$exec_time = date('i:s', (int) $time_post - $time_pre);
        $this->Actualizacion_Model->actualizar();
        return validarSubcategorias();        
    }

    public function actualizarRegistrosAjax(){
        $respuesta = $this->actualizarRegistros();
        if($respuesta["error"]){
            switch ($respuesta["type"]) {
                case "success":
                  $color = "green";
                  break;
                case "error":
                  $color = "red";
                  break;
                case "warning":
                  $color = "yellow";
                  break;  
                default:
                  $color = "blue";
                  break;
              }
              $respuesta["color"] = $color;
        }
        $output = $respuesta;
        header('Content-Type: application/json');
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function home($url = ''){
        //$this->actualizarRegistros();
        $datos['class'] = $this->class;
        $this->template->write('title', 'Admin Home');
        $this->loadTemplatesComunes($datos);
        $time = strtotime($this->Actualizacion_Model->getUltimaFechaAct());
        if($time != false){     
            $datos['ultimaFecha'] = strftime("%A, %d de %B del %Y a las %I:%M", $time)." ".(($time%86400) < 43200 ? 'am' : 'pm');
        } else {
            $datos['ultimaFecha'] = "Nunca se ha actualizado";
        }
        $this->template->asset_css('switcher/css/switcher.css');
        $this->template->asset_js('switcher/js/jquery.switcher.js');
        $this->template->asset_css('scrollbar/jquery.scrollbar.css');
        $this->template->asset_js('scrollbar/jquery.scrollbar.min.js');
        $this->template->asset_js('productos.js');
        
        $this->loadDataTables();
        $this->template->write_view('content', $this->folder.'/productos/list', $datos);
        $this->template->render();
    }

    public function actualizarVisibilidadProducto(){
        $idProducto = $this->input->post('idProducto');
        $estado = $this->input->post('estado');

        $error = $this->Productos_Model->actualizarVisibilidadProducto($idProducto, $estado);

        $output = ["error" => !$error];

        header('Content-Type: application/json');
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }
}