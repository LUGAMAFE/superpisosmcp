<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categorias extends MY_Admin {

    public function __construct() {
        parent::__construct();
        $this->load->model('administracion/Categorias_Model');
        $this->load->model('administracion/Subcategorias_Model');
    }

    public function home($url = ''){
        $validarSubcategorias = validarSubcategorias();
        if(isset($validarSubcategorias["error"]) && $validarSubcategorias["error"]){
            $this->alertWarning($validarSubcategorias["mensaje"], $validarSubcategorias["title"]);
        }
        $data['categorias'] = $this->Categorias_Model->getCategoriasConSubcategorias();
        $data['subcategoriasPendientes'] = $this->Subcategorias_Model->getSubcategoriasPendientes();
        $categoriasADesplegar = 3;
        $_categorias = $data['categorias'];
        $categoriasDesplegadas = [];
        if(count($_categorias) >= $categoriasADesplegar){
            for ($i=0; $i < $categoriasADesplegar; $i++) { 
                $categoriasDesplegadas[] = array_shift($_categorias);
            }
        }else{
            $categoriasDesplegadas = $_categorias;
            $_categorias = [];
        }
        $data['categoriasDesplegadas'] = $categoriasDesplegadas;
        $data['_categorias'] = $_categorias;
        $data['class'] = $this->class;
        $this->template->write('title', 'Admin Home');
        $this->loadTemplatesComunes($data);
        
        $this->template->asset_css('custominputfile/css/jquery.Jcrop.min.css');
        $this->template->asset_css('custominputfile/css/custominputfile.min.css');
        $this->template->asset_js('custominputfile/js/jquery.Jcrop.min.js');
        $this->template->asset_js('custominputfile/js/custominputfile.js');

        $this->template->asset_css('jquerymodal/jquery.modal.min.css');
        $this->template->asset_js('jquerymodal/jquery.modal.min.js');

        $this->template->asset_css('scrollbar/jquery.scrollbar.css');
        $this->template->asset_js('scrollbar/jquery.scrollbar.min.js');

        $this->template->asset_js('sortablejs/Sortable.js');
        
        $this->template->asset_js('categorias.js');
        
        $this->template->write_view('content', $this->folder.'/categorias/list', $data);
        $this->template->render();
    }

    public function subcategoriasPendientesACategoria(){
        $subcategorias = $this->input->post('subcategorias');
        $categoria = $this->input->post('categoria');

        $error = $this->Categorias_Model->subcategoriasPendientesACategoria($categoria, $subcategorias);
        $output = ["error" => !$error];

        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function subcategoriaCategoriaAsubcategoriasPendientes(){
        $subcategorias = $this->input->post('subcategorias');
        $categoria = $this->input->post('categoria');

        $error = $this->Categorias_Model->subcategoriaCategoriaAsubcategoriasPendientes($categoria, $subcategorias);
        $output = ["error" => !$error];

        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function subcategoriaCategoriaAsubcategoriaCategoria(){
        $subcategorias = $this->input->post('subcategorias');
        $categoriaOrigen = $this->input->post('categoriaOrigen');
        $categoriaDestino = $this->input->post('categoriaDestino');

        $error = $this->Categorias_Model->subcategoriaCategoriaAsubcategoriaCategoria($categoriaOrigen, $categoriaDestino, $subcategorias);
        $output = ["error" => !$error];

        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function nuevaCategoria(){
        $categoria = $this->input->post('categoria');
        $existeCategoria = $this->Categorias_Model->existeCategoriaNombre($categoria);
        if($existeCategoria){
            $output = ["error" => true, "respuesta" => "ya existe categoria"];
        }else{
            $insercion = $this->Categorias_Model->nuevaCategoria($categoria);
            if($insercion):
                $categoria = $this->Categorias_Model->getCategoria($categoria);
                // $respuesta = ob_get_contents();
                // ob_clean();
                $output = ["error" => false, "respuesta" => $categoria];
            else:
                $output = ["error" => true];
            endif;
        }
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function eliminarCategoria(){
        $categoria = $this->input->post('categoria');
        $error = $this->Categorias_Model->eliminarCategoria($categoria);
        $output = ["error" => !$error];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }
}