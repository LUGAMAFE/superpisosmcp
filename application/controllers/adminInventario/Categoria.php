<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categoria extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';
	protected $fineUploaders = array(
		array("name" => "imagen-banner-categoria", "nameDB" => "id_file_img_banner_categoria", "folder" => "../public/assets/banners/categorias", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
		array("name" => "imagen-home-categoria", "nameDB" => "id_file_img_home_categoria", "folder" => "../public/assets/img/categorias/home", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);

	public function __construct(){
		parent::__construct();
        $this->class = mb_strtolower(get_class());
        $this->load->model('administracion/Categorias_Model');
		$this->load->model("administracion/Archivos_Model");
	}

	public function index(){
        $datos['id'] = $this->input->get("id");
        $datos['datos'] = $this->Categorias_Model->getCategoria($datos);
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Categoria');
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('categoria.js');
        $datos["action"] = site_url("administracion/inventario/categorias/categoria/modificar");
		$datos["cancelar"] = site_url("administracion/inventario/categorias");
		$this->template->asset_css('x-editable/bootstrap4-editable/css/bootstrap-editable.css');
		$this->template->asset_js('x-editable/bootstrap4-editable/js/bootstrap-editable.min.js');
		$this->template->asset_js('filters-creator.js');
		$this->template->write_view('content', $this->folder.'/categorias/categoria', $datos);
		$this->template->render();
	}
	
	public function archivosEntrada(){
		$idCategoria = $this->input->get("idCategoria");
		$idUploader = $this->input->get("uploaderId");
		$output = [];
        switch ($idUploader) {
            case 'imagen-banner-categoria':
				$archivo = $this->Categorias_Model->getBannerCategoria($idCategoria);
                break;
            case 'imagen-home-categoria':
				$archivo = $this->Categorias_Model->getImagenHomeCategoria($idCategoria);
                break;
        }

        if($archivo !== null){
			$ruta = $archivo["dir_file"] . $archivo["name_file"] . ".". $archivo["ext_file"];
			$archivo = (object) ["name" => $archivo["name_file"], "uuid" => $archivo["uuid_file"], "thumbnailUrl" => $ruta];
			$output = [$archivo];
		}
		
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

	public function modificar(){
		$id = $this->input->post("id-categoria");
		$datos["filtros_categoria"] = $this->input->post("filters-config");
        $datos["texto_informativo_categoria"] = $this->input->post("texto-informativo");
		$datos["publicidad_categoria"] = $this->input->post("publicidad");
		$datos["nueva_categoria"] = "0";

		$deleteFiles = [];

		foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				$datos[$uploader["nameDB"]] = $ids;
			}
		}

		if(isset($datos)){
			$this->Categorias_Model->actualizarCategoria($datos, $id);
		}

		foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		$this->alertSuccess("Categoria Modificada Correctamente");
		redirect('administracion/inventario/categorias');
    }
    
    protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
	}

	
	public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}
}