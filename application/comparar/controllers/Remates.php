<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Remates extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';
	protected $fineUploaders = array(
		array("name" => "imagen-banner-remates", "nameDB" => "id_file_img_remates", "folder" => "../public/assets/banners/remates", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);

	public function __construct(){
		parent::__construct();
		$this->class = strtolower(get_class());
		$this->load->model("administracion/pagina/Remates_Model");
		$this->load->model("administracion/Archivos_Model");
	}

	public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Pagina ');
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('remates.js');
		$datos["action"] = site_url("administracion/pagina/home/modificar");
		$datos["youtube_urls"] =  $this->Home_Model->obtenerYoutubeUrlsHome();
		$this->template->write_view('content', $this->folder.'/pagina/home', $datos);
		$this->template->render();
	}
	
	public function archivosEntrada(){
		$idUploader = $this->input->get("uploaderId");
		$output = [];

        foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$directories = glob($folder . '/*' , GLOB_ONLYDIR);
				break;
			} 
		}

		foreach ($directories as $directorio) {
			$archivo = glob($directorio.'/*.*')[0];
			$uuid = pathinfo($directorio)["basename"];
			$name = pathinfo($archivo)["basename"];
			$thumb = str_replace( "../public/" , "", $archivo);
			$archivo = (object) ["name" => $name, "uuid" => $uuid, "thumbnailUrl" => $thumb];
			$output[] = $archivo;
			if(!$uploader["multiple"]){
				break;
			}
		}
		
		$json = json_encode($output, JSON_UNESCAPED_UNICODE);
		echo $json;
    }

	public function modificar(){
		$urls_youtube = $this->input->post("urls");

		$idsImagenesHome = $this->Home_Model->obtenerImagenesHome();
		$idsBannerHome = $this->Home_Model->obtenerImagenesBannerHome();

		$update = false;

		foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if($uploaderVariable === "imagen_banner_principal"){
				if(!empty($$uploaderVariable)){
					$decode = json_decode($$uploaderVariable);
					$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
					if(!$ids){
						return;
					}
					$this->Home_Model->guardarImagenesBanner($ids);
				}
				continue;
			}
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				$datos[$uploader["nameDB"]] = $ids;
			}else{$update = true;} 
		}

		if(isset($datos)){
			$this->Home_Model->guardarImagenesHome($datos, $update);
		}

		$this->Home_Model->guardarUrlsYoutube($urls_youtube);

		$this->alertSuccess("Pagina Home Modificada Correctamente");
		redirect('administracion/pagina');
	}

	protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
	}
	
	public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}
}