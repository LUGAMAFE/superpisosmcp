<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_Model extends MY_Model {

	public function __construct(){
        parent::__construct("home_page_data");	
    }

    public function get_home()
    {
        $url = $this->db->get('home_page_youtube_urls');
        $banners = $this->db
                            ->join('archivos as img', 'img.id_file = home_page_banner_principal.id_file_img_principal', 'left')
                            ->get('home_page_banner_principal');
        $banners = $this->db
                            ->join('archivos as img', 'img.id_file = home_page_banner_principal.id_file_img_principal', 'left')
                            ->get('home_page_banner_principal');

        $home = $this->db
                        ->select(
                            '
                            video_barra.dir_file AS video_barra_dir,
                            video_barra.name_file AS video_barra_name,
                            video_barra.ext_file AS video_barra_ext,

                            img_barra.dir_file AS img_barra_dir,
                            img_barra.name_file AS img_barra_name,
                            img_barra.ext_file AS img_barra_ext,

                            img_secundario.dir_file AS img_secundario_dir,
                            img_secundario.name_file AS img_secundario_name,
                            img_secundario.ext_file AS img_secundario_ext,

                            img_terciario.dir_file AS img_terciario_dir,
                            img_terciario.name_file AS img_terciario_name,
                            img_terciario.ext_file AS img_terciario_ext,

                            img_quienes.dir_file AS img_quienes_dir,
                            img_quienes.name_file AS img_quienes_name,
                            img_quienes.ext_file AS img_quienes_ext,

                            '
                        )
                            ->join('archivos as video_barra', 'video_barra.id_file = home_page_data.id_file_video_barra', 'left')
                            ->join('archivos as img_barra', 'img_barra.id_file = home_page_data.id_file_img_barra', 'left')
                            ->join('archivos as img_secundario', 'img_secundario.id_file = home_page_data.id_file_img_secundario', 'left')
                            ->join('archivos as img_terciario', 'img_terciario.id_file = home_page_data.id_file_img_terciario', 'left')
                            ->join('archivos as img_quienes', 'img_quienes.id_file = home_page_data.id_file_img_quienes', 'left')
                            ->get('home_page_data');
        return array(
            'url_youtube' => $url->result(),
            'banners' => $banners->result(),
            'home' => $home->result()
        );
    }

    public function guardarUrlsYoutube($data){
        if($this->db->count_all('home_page_youtube_urls') > 0){
            $this->db->empty_table('home_page_youtube_urls'); 
        }
        foreach ($data as $video) {
            $dato = ["url_video" => $video];
            $this->db->insert("home_page_youtube_urls", $dato);
        }
        return $resultado;
    }

    public function guardarImagenesHome($data, $update){
        if($update){
            $this->db->where($this->llave_primaria, 1);
            $resultado = $this->db->update('home_page_data', $data);
        }else{
            $resultado = $this->db->insert('home_page_data', $data);
        }
        return $resultado;
    }

    public function guardarImagenesBanner($data){
        if($this->db->count_all('home_page_banner_principal') > 0){
            $this->db->empty_table('home_page_banner_principal'); 
        }
        foreach ($data as $imagen) {
            $dato = ["id_file_img_principal" => $imagen];
            $this->db->insert("home_page_banner_principal", $dato);
        }
    }

    public function obtenerImagenesHome(){
        return $this->getFirst();
    }

    public function obtenerImagenesBannerHome(){
        $this->db->select('*');
        $this->db->from('home_page_banner_principal');
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerYoutubeUrlsHome(){
        $this->db->select('*');
        $this->db->from('home_page_youtube_urls');
        $query = $this->db->get();  
        return $query->result_array();
    }

    // -----------------------------------------

    public function obtenerImagenesEntradaPorId($idEntrada){
        $this->db->select('*');
        $this->db->from('entradas_blog');
        $this->db->where("id_entrada = '$idEntrada'");
        $query = $this->db->get();
        $idImg = $query->row_array();
        return ["img_big" => $idImg["imagen_entrada_big"], "img_small" => $idImg["imagen_entrada_small"]];
    }

    public function eliminarEntradaPorId($id){
        $this->db->where('id_entrada', $id);
        $resultado = $this->db->delete('entradas_blog');
        return $resultado;
    }

    public function existeEntradaId($idEntrada){
        $this->db->select('*');
        $this->db->from('entradas_blog');
        $this->db->where("id_entrada = '$idEntrada'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function existeEntrada($datos, $id = NULL){
        $nombre_entrada = $datos["nombre"];
        $titulo_entrada = $datos["titulo"];
        $this->db->select('*');
        $this->db->from('entradas_blog');
        if(is_null($id)){
            $this->db->where("nombre_entrada = '$nombre_entrada' OR titulo_entrada = '$titulo_entrada'");
        }else{
            $this->db->where("id_entrada !='$id' AND (nombre_entrada = '$nombre_entrada' OR titulo_entrada = '$titulo_entrada')");
        }
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function obtenerEntradasTabla(){
        $this->db->select('id_entrada, nombre_entrada, titulo_entrada, autor_entrada, fecha_creacion_entrada,fecha_modificacion_entrada');
        $this->db->from('entradas_blog');
        $this->db->order_by("id_entrada", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerInfoEntradaModificarPorId($idEntrada){
        $this->db->select('autor_entrada, nombre_entrada, titulo_entrada, subtitulo_entrada, resumen_entrada, informacion_entrada');
        $this->db->from('entradas_blog');
        $this->db->where("id_entrada = '$idEntrada'");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function countentradas(){
        $this->db->select('COUNT(*)');
        $this->db->from('entradas_blog');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function resetEntradas(){
        $query = $this->db->query("ALTER TABLE entradas_blog AUTO_INCREMENT =  1");
    }

    public function actualizarEntrada($datos, $idEntrada){
        $data = array(
            "nombre_entrada" => ucwords($datos["nombre"]),
            "autor_entrada" => $datos["autor"],
            "titulo_entrada" => ucwords($datos["titulo"]),
            "subtitulo_entrada" =>$datos["subtitulo"],
            "resumen_entrada" => $datos["resumen"],
            "informacion_entrada" => $datos["informacion"],
            "fecha_modificacion_entrada" => date('Y/m/d H:i:s'),
        );
        if(isset($datos["id_img_small"])){
            $data["imagen_entrada_small"] = $datos["id_img_small"];
        }
        if(isset($datos["id_img_big"])){
            $data["imagen_entrada_big"] = $datos["id_img_big"];
        }
        $this->db->where('id_entrada', $idEntrada);
        $resultado = $this->db->update('entradas_blog', $data);
        
        return $resultado;
    }

    public function guardarEntrada($datos){
        
        $data = array(
            "nombre_entrada" => ucwords(strtolower($datos["nombre"])),
            "autor_entrada" => $datos["autor"],
            "titulo_entrada" => ucwords(strtolower($datos["titulo"])),
            "subtitulo_entrada" =>$datos["subtitulo"],
            "resumen_entrada" => $datos["resumen"],
            "informacion_entrada" => $datos["informacion"],
            "imagen_entrada_big" => $datos["id_img_big"],
            "imagen_entrada_small" => $datos["id_img_small"],
            "fecha_creacion_entrada" => date('Y/m/d H:i:s'),
            "fecha_modificacion_entrada" => date('Y/m/d H:i:s'),
        );
        $resultado = $this->db->insert('entradas_blog', $data);
        
        return $resultado;
    }

    //FUNCIONES PARA VISTA BLOG
    public function getEntradasBlog(){
        $this->db->select('
        id_entrada,
        nombre_entrada,
        autor_entrada,
        titulo_entrada,
        subtitulo_entrada,
        resumen_entrada,
        informacion_entrada,
        fecha_creacion_entrada,
        fecha_modificacion_entrada,
        imagen_grande.id_img AS big_id_img,
        imagen_grande.folder_img AS big_folder_img,
        imagen_grande.uuid_img AS big_uuid_img,
        imagen_grande.name_img AS big_name_img,
        imagen_grande.ext_img AS big_ext_img,
        imagen_grande.dir_img AS big_dir_img,
        imagen_grande.full_route_img AS big_full_route_img,
        imagen_pequena.id_img AS small_id_img AS,
        imagen_pequena.folder_img AS small_folder_img,
        imagen_pequena.uuid_img AS small_uuid_img,
        imagen_pequena.name_img AS small_name_img,
        imagen_pequena.ext_img AS small_ext_img,
        imagen_pequena.dir_img AS small_dir_img,
        imagen_pequena.full_route_img AS small_full_route_img,
        ');
        $this->db->from('entradas_blog');
        $this->db->join('imagenes as imagen_grande', 'entradas_blog.imagen_entrada_big = imagen_grande.id_img', 'left');
        $this->db->join('imagenes as imagen_pequena', ' entradas_blog.imagen_entrada_small = imagen_pequena.id_img', 'left');
        $this->db->order_by("fecha_modificacion_entrada", "desc");
        $query = $this->db->get();  
        return $query->result_array();
    }
    
    public function getEntrada($idEntrada){
        $this->db->select('
        id_entrada,
        nombre_entrada,
        autor_entrada,
        titulo_entrada,
        subtitulo_entrada,
        resumen_entrada,
        informacion_entrada,
        fecha_creacion_entrada,
        fecha_modificacion_entrada,
        imagen_grande.id_img AS big_id_img,
        imagen_grande.folder_img AS big_folder_img,
        imagen_grande.uuid_img AS big_uuid_img,
        imagen_grande.name_img AS big_name_img,
        imagen_grande.ext_img AS big_ext_img,
        imagen_grande.dir_img AS big_dir_img,
        imagen_grande.full_route_img AS big_full_route_img,
        imagen_pequena.id_img AS small_id_img AS,
        imagen_pequena.folder_img AS small_folder_img,
        imagen_pequena.uuid_img AS small_uuid_img,
        imagen_pequena.name_img AS small_name_img,
        imagen_pequena.ext_img AS small_ext_img,
        imagen_pequena.dir_img AS small_dir_img,
        imagen_pequena.full_route_img AS small_full_route_img,
        ');
        $this->db->from('entradas_blog');
        $this->db->join('imagenes as imagen_grande', 'entradas_blog.imagen_entrada_big = imagen_grande.id_img', 'left');
        $this->db->join('imagenes as imagen_pequena', ' entradas_blog.imagen_entrada_small = imagen_pequena.id_img', 'left');
        $this->db->where("id_entrada = '$idEntrada'");
        $this->db->order_by("fecha_modificacion_entrada", "desc");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function getEntradasBlogLimit($offset, $limit){
        $this->db->select('imagen_grande.full_route_img as ruta_big, imagen_pequena.full_route_img as ruta_small, titulo_entrada, subtitulo_entrada, resumen_entrada, id_entrada');
        $this->db->from('entradas_blog');
        $this->db->join('imagenes as imagen_grande', 'entradas_blog.imagen_entrada_big = imagen_grande.id_img', 'left');
        $this->db->join('imagenes as imagen_pequena', ' entradas_blog.imagen_entrada_small = imagen_pequena.id_img', 'left');
        $this->db->order_by("fecha_modificacion_entrada", "desc");
        $this->db->limit($limit, $offset);
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function getEntradasBlogLimitRand($offset, $limit){
        $this->db->select('imagen_grande.full_route_img as ruta_big, imagen_pequena.full_route_img as ruta_small, titulo_entrada, subtitulo_entrada, resumen_entrada, id_entrada');
        $this->db->from('entradas_blog');
        $this->db->join('imagenes as imagen_grande', 'entradas_blog.imagen_entrada_big = imagen_grande.id_img', 'left');
        $this->db->join('imagenes as imagen_pequena', ' entradas_blog.imagen_entrada_small = imagen_pequena.id_img', 'left');
        $this->db->order_by("rand()");
        $this->db->limit($limit, $offset);
        $query = $this->db->get();  
        return $query->result_array();
    }
}    