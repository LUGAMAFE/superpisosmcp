<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Casos Exito Admin <i class="nav-icon fas fa-images"></i></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item active">Galeria Admin</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="card card-secondary">
                        <div class="card-header" style="marginbottom: 1rem;">
                            <h3 class="card-title">Productos</h3>
                        </div>
                        <!-- /.card-header -->

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row --> 
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

            <div class="contenedorProductos">
                <div class="despliegueTablaProductos">
                    <div class="tablaProductos">
                        <table id="table_id" class="display responsive" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Articulo</th>
                                    <th>Nombre Articulo</th>
                                    <th>Categoria</th>
                                    <th>Grupo</th>
                                    <th>Mostrar</th>
                                    <th>Info</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $id=0; foreach ($productos as $producto):?>
                                    <tr>
                                        <td class="id-producto"> <?= $producto["id_mcp"];?> </td>
                                        <td> <?= $producto["Descripcion1"];?> </td>
                                        <td> <?= is_null($producto["nombre_categoria"]) ? "Sin Categorizar" : $producto["nombre_categoria"]; ?> </td>
                                        <td> <?= $producto["nombre_grupo"];?> </td>
                                        <td> <input data-check-id="<?= $id++ ?>" type="checkbox" class="check-mostrar <?=$producto["visible_prod"] === "1" ? 'datachecked' : '';?>"> <span class="mostrar"><?=$producto["visible_prod"] === "1" ? 'mostrar' : 'ocultar';?></span> </td>
                                        <td> 
                                            <a href="<?php echo site_url('adminproducto/home').'/'.htmlspecialchars(trim($producto["id_mcp"]));?>"><button attr-id-prod="<?= $producto["id_mcp"];?>" class="btnEditar">Editar Producto</button></a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
</div>