<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct()
    {
        $dotenv = new Dotenv\Dotenv(BASEPATH .'/../');
        $dotenv->load();
        parent::__construct();

        /*$fb = new Facebook\Facebook([
            'app_id' => '251852438960949', // Replace {app-id} with your app id
            'app_secret' => '93a26bc9dc28d4ab723117344a2719f9',
            'default_graph_version' => 'v2.12',
        ]);
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email', 'public_profile', 'publish_actions']; // Optional permissions
        $this->loginUrl = $helper->getLoginUrl(site_url('clientes/callback'), $permissions);
        /* datos repetidos */
        /*$this->load->model('categorias/categoria');
        $this->load->model('comentarios/comentario');
        $this->listacategoria = $this->categoria->order_by('orden_categoria', 'ASC')->get();*/
        //$this->listacomentarios = $this->comentario->get();
    }
    
    public function setFlashData($nombre, $datos){
        $this->session->set_flashdata($nombre, $datos);
    }

    private function alertSession($type, $message, $description = null, $jBox, $title = null){
        $datos = [
            "title" => $title,
            "type" => $type,
            "message" =>  $message,
            "description" => $description,
            "jBox" => $jBox,
        ];  
        $this->setFlashData("admin-message", $datos);
    }    

    public function alertSuccess($message, $description = null, $jBox = FALSE, $title = "Exito!"){
        $this->alertSession("success", $message, $description, $jBox, $title);
    }

    public function alertError($message, $description = null, $jBox = FALSE, $title = "Error!" ){
        $this->alertSession("error", $message, $description, $jBox, $title);
    }

    public function alertInfo($message,  $description = null, $jBox = FALSE, $title = "Info" ){
        $this->alertSession("info", $message, $description, $jBox, $title);
    }

    public function alertWarning($message,  $description = null, $jBox = FALSE, $title = "Alerta!" ){
        $this->alertSession("warning", $message, $description, $jBox, $title);
    }   

    public function alertQuestion($message,  $description = null, $jBox = FALSE, $title = "¿?" ){
        $this->alertSession("question", $message, $description, $jBox, $title);
    }
}

class CLI_Controller extends MY_Controller {
  
    public function __construct()
    {
        parent::__construct();
    }
}

class MY_Admin extends MY_Controller {
    
    protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';

    private   $publicas   = array('administracion/login', 'administracion/logout', 'administracion/recuperar', 'administracion/verificar');
    protected $permitidos = array(0);

    public function __construct(){
        parent::__construct();
        $this->template->set_template('admin');
        /* Verificamos si está permitido el acceso */
        $this->verificar();
    }

    private function verificar(){
        $actual  = $this->uri->segment(1, '') . '/' . $this->uri->segment(2, '');
        $publica = in_array($actual, $this->publicas);

        if( !$publica AND !$this->session->userdata('logged_in') ) {
            redirect('administracion/login');
        } 
    }

    protected function loadDataTables(){
		// Datatables
		$this->template->asset_css('datatables/datatables.min.css');
		$this->template->asset_js('datatables/datatables.min.js');
		$this->template->asset_js('datatables-config.js');
	}

	protected function loadTemplatesComunes($datos){
        $this->template->write('bodyclasses', 'sidebar-mini layout-fixed');
        $this->template->asset_js('overlayScrollBody.js');
		$this->template->asset_js('../dist/js/personalizacion.js');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
    }
    
    protected function loadFineUploaderHelper($folderImgs, $extensions, $sizeLimit){
        $this->load->library('handlerfineuploader');
        $uploader = new Handlerfineuploader();
        // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $uploader->allowedExtensions = $extensions; // all files types allowed by default
        // Specify max file size in bytes.
        $uploader->sizeLimit = $sizeLimit;
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = $folderImgs.'chunks';
        $this->uploader = $uploader;
    }
    
    public function subirArchivo($folderImgs, $extensions, $sizeLimit){
        $this->loadFineUploaderHelper($folderImgs, $extensions, $sizeLimit);

        $method = get_request_method();

        if ($method == "POST") {
            header("Content-Type: text/plain");

            // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
            // For example: /myserver/handlers/endpoint.php?done
            if (isset($_GET["done"])) {
                $result = $this->uploader->combineChunks($folderImgs);
            }
            // Handles upload requests
            else {
                // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
                $result = $this->uploader->handleUpload($folderImgs);

                // To return a name used for uploaded file you can use the following line.
                $result["uploadName"] = $this->uploader->getUploadName();

                // if($result["success"]){
                //     $this->handleSubidaImagenEntrada($result["uuid"], $result["uploadName"]);
                // }
            }

            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        // for delete file requests
        else if ($method == "DELETE") {
            $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $tokens = explode('/', $url);
            $uuid = $tokens[sizeof($tokens)-1];
            if($this->Archivos_Model->existeArchivo($uuid)){
                $result = array("success" => false,
                    "error" => "File not found! Unable to delete.".$url,
                    "path" => $uuid
                );
            }else{
                $result = $this->uploader->handleDelete($folderImgs);
            }
            
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }
}
