<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Representa a una tabla de la base de datos y las acciones permitidas.
 * @property string $table Nombre de la tabla sobre la cual se realizan acciones.
 * @property string $table_prefix Especifica el prefijo de la base de datos en los queries SQL.
 * @property string $llave_primaria Atributo de la base de datos que funciona como llave primaria.
 * @property array  $atributosMetadata arreglo con todos los atributos de los campos de una tabla.
 */
class MY_Model extends CI_Model{

    protected $table;
    protected $table_prefix; 
    protected $llave_primaria;
    protected $atributosMetadata;
    protected $lastQuery;

    const STR_BD_TB = 'information_schema.COLUMNS';


    public function __construct($table) {
        parent::__construct();
        $this->load->database();
        $this->table = (string) $table;
        $this->llave_primaria = $this->get_llave_primaria();
        $this->atributosMetadata = $this->get_atributos_BD();
    }

    /**
     * Consulta generica para ejecutar un query.
     * @return array
     */
    public function query($query){
        $this->lastQuery = $this->db->query($query);
        return $this->lastQuery->result_array();
    }

    /**
     * Método para obtener las llaves primarias de la tabla.
     * @return $result array asociativo de longitud 1
     */
    public function get_llave_primaria(){
        $database = $this->db->database;
        $strSQL = 'SELECT ' . self::STR_BD_TB . '.COLUMN_NAME' .
                    ' FROM '. self::STR_BD_TB .
                    ' WHERE ' . self::STR_BD_TB . '.COLUMN_KEY = \'PRI\' AND ' .
                        self::STR_BD_TB . ".TABLE_NAME = '$this->table' AND ".
                        self::STR_BD_TB . '.TABLE_SCHEMA = \'' . $database."'";
        //var_dump($strSQL);
        $this->query($strSQL);
        $result = $this->lastQuery->row_array();
        return $result['COLUMN_NAME'];
    }

    /**
    * Método para recuperar los atributos y su información para construir un
    * formulario genérico.
    * @return $result array asociativo de longitud variable.
    */    
    public function get_atributos_BD(){
        $database = $this->db->database;
        $strSQL = 'SELECT ' . self::STR_BD_TB . '.COLUMN_NAME,' .
                    self::STR_BD_TB . '.COLUMN_KEY,' . 
                    self::STR_BD_TB . '.IS_NULLABLE,' . 
                    self::STR_BD_TB . '.DATA_TYPE,' .
                    self::STR_BD_TB . '.NUMERIC_PRECISION,' .
                    self::STR_BD_TB . '.COLUMN_COMMENT,' .
                    self::STR_BD_TB . '.COLUMN_DEFAULT,' .
                    self::STR_BD_TB . '.CHARACTER_MAXIMUM_LENGTH' .

                    ' FROM ' .self::STR_BD_TB .
                        ' WHERE ' .self::STR_BD_TB . ".TABLE_NAME = '$this->table' AND ".
                        self::STR_BD_TB . ".TABLE_SCHEMA = '$database'";
        //var_dump($strSQL);
        return $this->query($strSQL);
    }

    /**
     * Consulta para obtener el ultimo error que ha ocurrido en las ejecuciones sql.
     * @return array
     */
    public function getError(){
        return $this->db->error();
    }

    /**
     * Consulta generica para recuperar todos los registros de la tabla
     * @return array
     */
    public function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->order_by($this->llave_primaria, "DESC");
        $this->lastQuery = $this->db->get();  
        return $this->lastQuery->result_array();
    }

    /**
     * Consulta generica para recuperar el primer regstro de la tabla
     * @return array
     */
    public function getFirst() {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->order_by($this->llave_primaria, "DESC");
        $this->lastQuery = $this->db->get();  
        return $this->lastQuery->row_array();
    }

    public function getQueryBuild() {
        return $this->db->last_query();
    }

    /**
     * Consulta generica para recuperar un registro identificado por su llave primaria.
     * @param int $id
     * @return array registro
     */
    public function getById($id) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->llave_primaria, $id);
        //$this->db->order_by($this->llave_primaria, "DESC");
        $this->lastQuery = $this->db->get();  
        return $this->lastQuery->row_array();
    }

    /**
     * Consulta generica para eliminar un registro identificado por su llave primaria.
     * @param int $id
     * @return boolean resultado eliminacion
     */
    public function deleteById($id) {
        $this->db->where($this->llave_primaria, $id);
        $resultado = $this->db->delete($this->table);
        if($resultado && $this->count() <= 0){
            $this->db->query("ALTER TABLE ".$this->table." AUTO_INCREMENT =  1");
        }
        return $resultado;
    }

    /**
     * Consulta generica para obtener el numero de filas de un a tabla.
     * @return int numero registros.
     */
    public function count(){
        $this->db->select('COUNT(*)');
        $this->db->from($this->table);
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function update($id, $data){
        $this->db->where($this->llave_primaria, $id);
        $this->db->update($this->table, $data);
    }

    /**
     * Metodo publico para obtener la llave primaria de la tabla.
     * @return int
     */
    public function getLlavePrimaria() {
        return $this->llave_primaria;
    }
}
?>