<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['contacto'] = 'home/contacto';
$route['carrito'] = 'home/carrito';
$route['asociados'] = 'home/socios';
$route['bolsa-de-trabajo'] = 'home/bolsa_trabajo';
$route['enviar-cotizacion'] = 'home/enviarCotizacion';
$route['enviar-contacto'] = 'home/enviarContacto';
$route['enviar-socio'] = 'home/enviarSocio';
$route['enviar-trabajo'] = 'home/enviarTrabajo';
$route['buscar-termino'] = 'home/buscarTermino';
$route['usuarios'] = 'usuarios';

$route['categoria'] = 'home/categoria';
$route['categoria/remates'] = 'home/categoria/0/remates';
$route['categoria/(:any)/(:any)'] = 'home/categoria/$1/$2';
$route['categoria/(:any)/(:any)/(:any)/(:any)'] = 'home/categoria/$1/$2/$3/$4';

$route['producto/(:any)'] = 'home/info_producto/$1';
$route['producto/(:any)/(:any)'] = 'home/info_producto/$1/$2';

$route['contacto'] = 'home/contacto';
$route['contacto/enviar'] = 'home/enviar';
$route['contacto/enviar-puesto'] = 'home/enviar_puesto';
$route['contacto/enviar-informacion'] = 'home/enviar_informacion';
$route['contacto/enviado'] = 'home';

$route['administracion/inventario/productos/datatable'] = 'adminInventario/productos/datatable';
$route['administracion/inventario/productos'] = 'adminInventario/productos/home';
$route['administracion/inventario/productos/actualizarRegistrosAjax'] = 'adminInventario/productos/actualizarRegistrosAjax';
$route['administracion/inventario/productos/actualizarVisibilidadProducto'] = 'adminInventario/productos/actualizarVisibilidadProducto';
$route['administracion/inventario/productos/producto'] = 'adminInventario/producto';
$route['administracion/inventario/productos/producto/modificar'] = 'adminInventario/producto/modificar';
$route['administracion/inventario/productos/producto/archivosEntrada'] = 'adminInventario/producto/archivosEntrada';
$route['administracion/inventario/productos/producto/subirArchivos'] = 'adminInventario/producto/subirArchivos';

$route['administracion/inventario/categorias'] = 'adminInventario/categorias/home';
$route['administracion/inventario/categorias/nuevaCategoria'] = 'adminInventario/categorias/nuevaCategoria';
$route['administracion/inventario/categorias/eliminarCategoria'] = 'adminInventario/categorias/eliminarCategoria';
$route['administracion/inventario/categorias/subcategoriasPendientesACategoria'] = 'adminInventario/categorias/subcategoriasPendientesACategoria';
$route['administracion/inventario/categorias/subcategoriaCategoriaAsubcategoriasPendientes'] = 'adminInventario/categorias/subcategoriaCategoriaAsubcategoriasPendientes';
$route['administracion/inventario/categorias/subcategoriaCategoriaAsubcategoriaCategoria'] = 'adminInventario/categorias/subcategoriaCategoriaAsubcategoriaCategoria';

$route['administracion/inventario/categorias/categoria'] = 'adminInventario/categoria';
$route['administracion/inventario/categorias/categoria/modificar'] = 'adminInventario/categoria/modificar';
$route['administracion/inventario/categorias/categoria/archivosEntrada'] = 'adminInventario/categoria/archivosEntrada';
$route['administracion/inventario/categorias/categoria/subirArchivos'] = 'adminInventario/categoria/subirArchivos';

$route['administracion/pagina'] = 'adminPagina/dashboard';
$route['administracion/pagina/home'] = 'adminPagina/home';
$route['administracion/pagina/home/archivosEntrada'] = 'adminPagina/home/archivosEntrada';
$route['administracion/pagina/home/subirArchivos'] = 'adminPagina/home/subirArchivos';
$route['administracion/pagina/home/subirArchivos/(:any)'] = 'adminPagina/home/subirArchivos';
$route['administracion/pagina/home/modificar'] = 'adminPagina/home/modificar';

$route['administracion/pagina'] = 'adminPagina/dashboard';
$route['administracion/pagina/remates'] = 'adminPagina/remates';
$route['administracion/pagina/remates/archivosEntrada'] = 'adminPagina/remates/archivosEntrada';
$route['administracion/pagina/remates/subirArchivos'] = 'adminPagina/remates/subirArchivos';
$route['administracion/pagina/remates/subirArchivos/(:any)'] = 'adminPagina/remates/subirArchivos';
$route['administracion/pagina/remates/modificar'] = 'adminPagina/remates/modificar';

$route['administracion/pagina'] = 'adminPagina/dashboard';
$route['administracion/pagina/bolsa-trabajo'] = 'adminPagina/bolsa';
$route['administracion/pagina/bolsa-trabajo/archivosEntrada'] = 'adminPagina/bolsa/archivosEntrada';
$route['administracion/pagina/bolsa-trabajo/subirArchivos'] = 'adminPagina/bolsa/subirArchivos';
$route['administracion/pagina/bolsa-trabajo/subirArchivos/(:any)'] = 'adminPagina/bolsa/subirArchivos';
$route['administracion/pagina/bolsa-trabajo/modificar'] = 'adminPagina/bolsa/modificar';

$route['administracion/pagina'] = 'adminPagina/dashboard';
$route['administracion/pagina/ventana-info'] = 'adminPagina/ventana';
$route['administracion/pagina/ventana-info/archivosEntrada'] = 'adminPagina/ventana/archivosEntrada';
$route['administracion/pagina/ventana-info/subirArchivos'] = 'adminPagina/ventana/subirArchivos';
$route['administracion/pagina/ventana-info/subirArchivos/(:any)'] = 'adminPagina/ventana/subirArchivos';
$route['administracion/pagina/ventana-info/modificar'] = 'adminPagina/ventana/modificar';

$route['administracion/pagina'] = 'adminPagina/dashboard';
$route['administracion/pagina/cotizacion'] = 'adminPagina/cotizacion';
$route['administracion/pagina/cotizacion/archivosEntrada'] = 'adminPagina/cotizacion/archivosEntrada';
$route['administracion/pagina/cotizacion/subirArchivos'] = 'adminPagina/cotizacion/subirArchivos';
$route['administracion/pagina/cotizacion/subirArchivos/(:any)'] = 'adminPagina/cotizacion/subirArchivos';
$route['administracion/pagina/cotizacion/modificar'] = 'adminPagina/cotizacion/modificar';

$route['administracion/pagina'] = 'adminPagina/dashboard';
$route['administracion/pagina/subdistribucion'] = 'adminPagina/subdistribucion';
$route['administracion/pagina/subdistribucion/archivosEntrada'] = 'adminPagina/subdistribucion/archivosEntrada';
$route['administracion/pagina/subdistribucion/subirArchivos'] = 'adminPagina/subdistribucion/subirArchivos';
$route['administracion/pagina/subdistribucion/subirArchivos/(:any)'] = 'adminPagina/subdistribucion/subirArchivos';
$route['administracion/pagina/subdistribucion/modificar'] = 'adminPagina/subdistribucion/modificar';



