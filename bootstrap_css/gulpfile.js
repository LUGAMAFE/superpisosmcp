var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();
local_data = {
    "proxy": ""
}
gulp.task('browser-sync', function() {
    browserSync.init({
        proxy  : local_data.proxy,
        online : false,
        open   : false,
    });
});

gulp.task('admin', function() {
    return gulp.src('adminlte/adminlte.scss')
        .pipe($.sass({
            outputStyle: 'compressed' // if css compressed **file size**
        })
        .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('../public/assets/css'));
});

gulp.task('default', ['admin', 'browser-sync'], function() {
    gulp.watch(['adminlte/**/*.scss'], ['admin']);
    gulp.watch('adminlte/**/*.scss').on('change', browserSync.reload);
    gulp.watch('scss/**/*.scss').on('change', browserSync.reload);
});
