-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 02-12-2019 a las 15:57:27
-- Versión del servidor: 5.6.32-78.1
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gantmxgy_mcp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actualizacion`
--

CREATE TABLE `actualizacion` (
  `id_actualizacion` int(255) UNSIGNED NOT NULL,
  `fecha_modificacion_actualizacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Estructura de tabla para la tabla `archivos`
--

CREATE TABLE `archivos` (
  `id_file` int(255) UNSIGNED NOT NULL,
  `folder_file` varchar(300) NOT NULL,
  `uuid_file` varchar(100) NOT NULL,
  `name_file` varchar(150) NOT NULL,
  `ext_file` varchar(10) NOT NULL,
  `dir_file` varchar(200) NOT NULL,
  `full_route_file` varchar(400) NOT NULL,
  `fecha_creacion_file` datetime NOT NULL,
  `fecha_modificacion_file` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura Stand-in para la vista `archivos_vista`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `archivos_vista` (
`id_file` int(255) unsigned
,`archivo_original` varchar(361)
,`archivo_large` varchar(369)
,`archivo_medium` varchar(370)
,`archivo_small` varchar(369)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bolsa_page_data`
--

CREATE TABLE `bolsa_page_data` (
  `id_bolsa` int(255) UNSIGNED NOT NULL,
  `id_file_img_bolsa` int(255) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_cat` int(11) NOT NULL,
  `nombre_categoria` varchar(255) NOT NULL,
  `nueva_categoria` int(2) UNSIGNED NOT NULL DEFAULT '1',
  `id_file_img_banner_categoria` int(255) UNSIGNED DEFAULT NULL,
  `id_file_img_home_categoria` int(255) UNSIGNED DEFAULT NULL,
  `texto_informativo_categoria` varchar(100) NOT NULL,
  `publicidad_categoria` varchar(100) NOT NULL,
  `filtros_categoria` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `categorias_subcategorias`
--

CREATE TABLE `categorias_subcategorias` (
  `id_categorias_subcategorias` int(11) NOT NULL,
  `categorias_id_cat` int(11) NOT NULL,
  `subcategorias_id_subcategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `home_page_banner_principal`
--

CREATE TABLE `home_page_banner_principal` (
  `id_banner_principal` int(255) UNSIGNED NOT NULL,
  `id_file_img_principal` int(255) UNSIGNED NOT NULL,
  `json_input_extras` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `home_page_data`
--

CREATE TABLE `home_page_data` (
  `id_home` int(255) UNSIGNED NOT NULL,
  `id_file_video_barra` int(255) UNSIGNED NOT NULL,
  `id_file_img_barra` int(255) UNSIGNED NOT NULL,
  `id_file_img_secundario` int(255) UNSIGNED NOT NULL,
  `id_file_img_terciario` int(255) UNSIGNED NOT NULL,
  `id_file_img_quienes` int(255) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `home_page_youtube_urls`
--

CREATE TABLE `home_page_youtube_urls` (
  `id_url` int(255) UNSIGNED NOT NULL,
  `url_video` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `imagenes_productos`
--

CREATE TABLE `imagenes_productos` (
  `id_img_producto` int(255) UNSIGNED NOT NULL,
  `id_producto_asoc_producto` varchar(255) NOT NULL,
  `id_file_asoc_producto` int(255) UNSIGNED NOT NULL,
  `tipo_imagen_producto` varchar(100) NOT NULL,
  `json_input_extras` text,
  `imagen_principal` int(2) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_prod` int(11) UNSIGNED NOT NULL,
  `desc_mcp` varchar(300) NOT NULL,
  `id_mcp` varchar(255) NOT NULL,
  `visible_prod` int(2) UNSIGNED NOT NULL,
  `nuevo_prod` int(2) UNSIGNED NOT NULL,
  `grupo_mcp` varchar(355) NOT NULL,
  `subcategoria_mcp` varchar(355) NOT NULL,
  `nombre_producto` varchar(50) NOT NULL,
  `descripcion_producto` varchar(300) NOT NULL,
  `informacion_extra_producto` varchar(500) NOT NULL,
  `especificaciones_producto` text NOT NULL,
  `producto_rey` tinyint(2) UNSIGNED NOT NULL,
  `producto_mas_vendido` tinyint(2) NOT NULL DEFAULT '0',
  `descuento_activado_producto` tinyint(2) UNSIGNED NOT NULL,
  `descuento_producto` varchar(300) NOT NULL,
  `filtros_producto` text NOT NULL,
  `productos_asociados` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura Stand-in para la vista `productos_tabla_vista`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `productos_tabla_vista` (
`id_prod` int(11) unsigned
,`id_mcp` varchar(255)
,`nombre_categoria` varchar(255)
,`nombre_subcategoria` varchar(355)
,`visible_prod` varchar(7)
,`desc_mcp` varchar(300)
,`nuevo_prod` int(2) unsigned
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `remates_page_data`
--

CREATE TABLE `remates_page_data` (
  `id_remates` int(255) UNSIGNED NOT NULL,
  `id_file_img_remates` int(255) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `subcategorias`
--

CREATE TABLE `subcategorias` (
  `id_subcategoria` int(11) NOT NULL,
  `nombre_subcategoria` varchar(355) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(255) UNSIGNED NOT NULL,
  `contrasena_usuario` varchar(300) NOT NULL,
  `nombre_usuario` varchar(300) NOT NULL,
  `correo_usuario` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `contrasena_usuario`, `nombre_usuario`, `correo_usuario`) VALUES
(1, '$2y$10$D51PdEvXOob5k/2Hye84QeDq7d1ZdXpXLbBonSfvx.hQ4BbdrojEu', 'lugamafe', 'luisjavier004@hotmail.com'),
(2, '$2y$10$xS00zgXpmUX13EGvRcE7F.Q/96uSQEmtynJGw9mKYxOXF9C.RFws2', 'Admin', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventana_page_data`
--

CREATE TABLE `ventana_page_data` (
  `id_ventana` int(11) NOT NULL,
  `id_file_img_ventana` int(255) UNSIGNED NOT NULL,
  `json_input_extras` text NOT NULL,
  `is_activo` int(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `ventana_page_info`
--

CREATE TABLE `ventana_page_info` (
  `id_ventana` int(11) NOT NULL,
  `is_activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Estructura para la vista `archivos_vista`
--
DROP TABLE IF EXISTS `archivos_vista`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `archivos_vista`  AS  select `archivos`.`id_file` AS `id_file`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),'',concat('.',`archivos`.`ext_file`))) AS `archivo_original`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),' (large)',concat(' (large).',`archivos`.`ext_file`))) AS `archivo_large`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),' (medium)',concat(' (medium).',`archivos`.`ext_file`))) AS `archivo_medium`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),' (small)',concat(' (small).',`archivos`.`ext_file`))) AS `archivo_small` from `archivos` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `productos_tabla_vista`
--
DROP TABLE IF EXISTS `productos_tabla_vista`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `productos_tabla_vista`  AS  select `productos`.`id_prod` AS `id_prod`,`productos`.`id_mcp` AS `id_mcp`,ifnull(`categorias`.`nombre_categoria`,'Sin Categorizar') AS `nombre_categoria`,`subcategorias`.`nombre_subcategoria` AS `nombre_subcategoria`,(case when (`productos`.`visible_prod` = 1) then 'Mostrar' when (`productos`.`visible_prod` = 0) then 'Ocultar' end) AS `visible_prod`,`productos`.`desc_mcp` AS `desc_mcp`,`productos`.`nuevo_prod` AS `nuevo_prod` from (((`productos` join `subcategorias` on((`productos`.`subcategoria_mcp` = `subcategorias`.`nombre_subcategoria`))) left join `categorias_subcategorias` on((`subcategorias`.`id_subcategoria` = `categorias_subcategorias`.`subcategorias_id_subcategoria`))) left join `categorias` on((`categorias_subcategorias`.`categorias_id_cat` = `categorias`.`id_cat`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actualizacion`
--
ALTER TABLE `actualizacion`
  ADD PRIMARY KEY (`id_actualizacion`);

--
-- Indices de la tabla `archivos`
--
ALTER TABLE `archivos`
  ADD PRIMARY KEY (`id_file`),
  ADD UNIQUE KEY `uuid_file_unique` (`uuid_file`);

--
-- Indices de la tabla `bolsa_page_data`
--
ALTER TABLE `bolsa_page_data`
  ADD PRIMARY KEY (`id_bolsa`),
  ADD KEY `id_file_img_bolsa` (`id_file_img_bolsa`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_cat`),
  ADD UNIQUE KEY `nombre` (`nombre_categoria`),
  ADD KEY `id_file_img_banner_categoria` (`id_file_img_banner_categoria`);

--
-- Indices de la tabla `categorias_subcategorias`
--
ALTER TABLE `categorias_subcategorias`
  ADD PRIMARY KEY (`id_categorias_subcategorias`),
  ADD UNIQUE KEY `grupos_id_grupo` (`subcategorias_id_subcategoria`),
  ADD KEY `fk_categorias_grupos_categorias_idx` (`categorias_id_cat`),
  ADD KEY `fk_categorias_grupos_grupos1_idx` (`subcategorias_id_subcategoria`);

--
-- Indices de la tabla `home_page_banner_principal`
--
ALTER TABLE `home_page_banner_principal`
  ADD PRIMARY KEY (`id_banner_principal`),
  ADD KEY `id_file_img_principal` (`id_file_img_principal`);

--
-- Indices de la tabla `home_page_data`
--
ALTER TABLE `home_page_data`
  ADD PRIMARY KEY (`id_home`),
  ADD KEY `id_file_video_barra` (`id_file_video_barra`),
  ADD KEY `id_file_img_barra` (`id_file_img_barra`),
  ADD KEY `id_file_img_secundario` (`id_file_img_secundario`),
  ADD KEY `id_file_img_terciario` (`id_file_img_terciario`),
  ADD KEY `if_file_img_quienes` (`id_file_img_quienes`);

--
-- Indices de la tabla `home_page_youtube_urls`
--
ALTER TABLE `home_page_youtube_urls`
  ADD PRIMARY KEY (`id_url`);

--
-- Indices de la tabla `imagenes_productos`
--
ALTER TABLE `imagenes_productos`
  ADD PRIMARY KEY (`id_img_producto`),
  ADD KEY `id_producto_asoc_producto` (`id_producto_asoc_producto`),
  ADD KEY `id_file_asoc_producto` (`id_file_asoc_producto`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_prod`),
  ADD UNIQUE KEY `id_mcp` (`id_mcp`);

--
-- Indices de la tabla `remates_page_data`
--
ALTER TABLE `remates_page_data`
  ADD PRIMARY KEY (`id_remates`),
  ADD KEY `id_file_img_remates` (`id_file_img_remates`);

--
-- Indices de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD PRIMARY KEY (`id_subcategoria`),
  ADD UNIQUE KEY `nombre` (`nombre_subcategoria`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `unique_nombre_usuario` (`nombre_usuario`),
  ADD UNIQUE KEY `unique_correo_usuario` (`correo_usuario`);

--
-- Indices de la tabla `ventana_page_data`
--
ALTER TABLE `ventana_page_data`
  ADD PRIMARY KEY (`id_ventana`),
  ADD KEY `id_file_img_ventana` (`id_file_img_ventana`);

--
-- Indices de la tabla `ventana_page_info`
--
ALTER TABLE `ventana_page_info`
  ADD PRIMARY KEY (`id_ventana`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actualizacion`
--
ALTER TABLE `actualizacion`
  MODIFY `id_actualizacion` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `archivos`
--
ALTER TABLE `archivos`
  MODIFY `id_file` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `bolsa_page_data`
--
ALTER TABLE `bolsa_page_data`
  MODIFY `id_bolsa` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_cat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `categorias_subcategorias`
--
ALTER TABLE `categorias_subcategorias`
  MODIFY `id_categorias_subcategorias` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `home_page_banner_principal`
--
ALTER TABLE `home_page_banner_principal`
  MODIFY `id_banner_principal` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `home_page_data`
--
ALTER TABLE `home_page_data`
  MODIFY `id_home` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `home_page_youtube_urls`
--
ALTER TABLE `home_page_youtube_urls`
  MODIFY `id_url` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `imagenes_productos`
--
ALTER TABLE `imagenes_productos`
  MODIFY `id_img_producto` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_prod` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `remates_page_data`
--
ALTER TABLE `remates_page_data`
  MODIFY `id_remates` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  MODIFY `id_subcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ventana_page_data`
--
ALTER TABLE `ventana_page_data`
  MODIFY `id_ventana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `ventana_page_info`
--
ALTER TABLE `ventana_page_info`
  MODIFY `id_ventana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bolsa_page_data`
--
ALTER TABLE `bolsa_page_data`
  ADD CONSTRAINT `bolsa_page_data_ibfk_1` FOREIGN KEY (`id_file_img_bolsa`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD CONSTRAINT `categorias_ibfk_1` FOREIGN KEY (`id_file_img_banner_categoria`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `categorias_subcategorias`
--
ALTER TABLE `categorias_subcategorias`
  ADD CONSTRAINT `fk_categorias_grupos_categorias` FOREIGN KEY (`categorias_id_cat`) REFERENCES `categorias` (`id_cat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_categorias_grupos_grupos1` FOREIGN KEY (`subcategorias_id_subcategoria`) REFERENCES `subcategorias` (`id_subcategoria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `home_page_banner_principal`
--
ALTER TABLE `home_page_banner_principal`
  ADD CONSTRAINT `home_page_banner_principal_ibfk_1` FOREIGN KEY (`id_file_img_principal`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `home_page_data`
--
ALTER TABLE `home_page_data`
  ADD CONSTRAINT `home_page_data_ibfk_1` FOREIGN KEY (`id_file_video_barra`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `home_page_data_ibfk_2` FOREIGN KEY (`id_file_img_barra`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `home_page_data_ibfk_3` FOREIGN KEY (`id_file_img_secundario`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `home_page_data_ibfk_4` FOREIGN KEY (`id_file_img_terciario`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `home_page_data_ibfk_5` FOREIGN KEY (`id_file_img_quienes`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `imagenes_productos`
--
ALTER TABLE `imagenes_productos`
  ADD CONSTRAINT `imagenes_productos_ibfk_1` FOREIGN KEY (`id_file_asoc_producto`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `imagenes_productos_ibfk_2` FOREIGN KEY (`id_producto_asoc_producto`) REFERENCES `productos` (`id_mcp`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ventana_page_data`
--
ALTER TABLE `ventana_page_data`
  ADD CONSTRAINT `ventana_page_data_ibfk_1` FOREIGN KEY (`id_file_img_ventana`) REFERENCES `archivos` (`id_file`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
