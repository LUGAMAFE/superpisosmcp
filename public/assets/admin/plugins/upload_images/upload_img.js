$.fn.fileUploader = function (filesToUpload, sectionIdentifier) {
    var fileIdCounter = 0;
    $dataClass = $('#'+sectionIdentifier).data('class');
    this.closest("."+$dataClass).change(function (evt) {
    	$dataClass = $('#'+sectionIdentifier).data('class');
    	var files = evt.target.files;
        var output = [];

        for (var i = 0, f; f = files[i]; i++) {
            fileIdCounter++;
            console.log(fileIdCounter);
            var file = evt.target.files[i];
            var fileId = sectionIdentifier + fileIdCounter;
            var reader  = new FileReader();
            filesToUpload[sectionIdentifier].push({
                id: fileId,
                file: file
            });
            var pdf = [];
			reader.onload = (function(theFile) {
				return function(e) {
					console.log(theFile);
					
					console.log(e.target.result);
					//document.querySelector('imgFoto-files21').src = e.target.result;
					var removeLink = "<a class=\"removeFile button tiny\" data-fileid=\"" + fileId + "\">Eliminar</a>";
                    if(theFile.type == 'application/pdf') {
                        var img = "<canvas id=\""+ fileId +"\" width=\"300px\"></canvas> <label>Page: </label><label id=\"page"+ fileId +"\"></label>";
                        var aux = {id: fileId, file: e.target.result};
                        pdf.push(aux);
                    } else {
                        var img = "<img src=\""+e.target.result+"\" id=\""+ fileId +"\">";
                    }
		            
		            var input_hidden_img = '<input type="hidden" name="'+sectionIdentifier+'[img][]" value="'+e.target.result+'">';
		            var input_hidden_type = '<input type="hidden" name="'+sectionIdentifier+'[type][]" value="'+theFile.type+'">';
		            var input_hidden_name = '<input type="hidden" name="'+sectionIdentifier+'[name][]" value="'+theFile.name+'">';
		            var input_hidden_size = '<input type="hidden" name="'+sectionIdentifier+'[size][]" value="'+theFile.size+'">';
		    		
					output.push("<div class=\"column column-block\">", img,"<p><strong>", file.name.replace('%20', ' '), "</strong> - ", file.size, " bytes.</p> <div class=\"row\">", removeLink, "", input_hidden_img, input_hidden_type, input_hidden_name, input_hidden_size,"</div></div> ");
				};
			})(f);
			reader.readAsDataURL(f)
        }
        
        setTimeout(function(){
        	console.log(output);
        	$(".fileList[data-class="+$dataClass+"]")
            			.append(output.join(""));
            //console.log(pdf);
            if($.isArray(pdf)) {
                $.each(pdf, function( index, value ) {
                    //setTimeout(RenderPDF(0, value.id, file.file), 2000);
                    //console.log(value);
                    RenderPDF(0, value.id, value.file);
                });
            }
        }, 300);
        //setTimeout(RenderPDF(0, pdfProducto1, e.target.result), 2000);
        
        //reset the input to null - nice little chrome bug!
        evt.target.value = null;
    });

    function RenderPDF(pageNumber, canvasId, base64) {
        console.log(base64);
        var pdfData = atob(base64.replace('data:application/pdf;base64,', ''));
        var canvas = document.getElementById(canvasId);
        console.log(canvas);
        pdfjsLib.disableWorker = true;

        // Get current global page number, defaults to 1
        displayNum = parseInt($('#page'+canvasId).html())
        pageNumber = parseInt(pageNumber)

        var loadingTask = pdfjsLib.getDocument({data: pdfData});
        loadingTask.promise.then(function(pdf) {
            // Gets total page length of pdf
            size = pdf.numPages
            // Handling for changing pages
            if(pageNumber == 1) {
                pageNumber = displayNum + 1;
            }
            if(pageNumber == -1) {
                pageNumber = displayNum - 1;
            }
            if(pageNumber == 0) {
                pageNumber = 1;
            }
          // If the requested page is outside the document bounds
            if(pageNumber > size || pageNumber < 1) {
                throw "bad page number";
            }
            // Changes the cheeky global to our valid new page number
            $('#page'+canvasId).text(pageNumber)
            pdf.getPage(pageNumber).then(function(page) {
                var scale = 0.4;
                var viewport = page.getViewport(scale);
                var context = canvas.getContext('2d');
                canvas.height = viewport.height;
                canvas.width = viewport.width;
                var renderContext = {
                  canvasContext: context,
                  viewport: viewport
                };
                page.render(renderContext);
            });
        }).catch(e => {});
    }

    $(document).on("click", ".removeFile", function (e) {
        e.preventDefault();
        var fileId = $(this).parent().children("a").data("fileid");
        console.log(fileId);

        // loop through the files array and check if the name of that file matches FileName
        // and get the index of the match
        for (var i = 0; i < filesToUpload[sectionIdentifier].length; ++i) {
            if (filesToUpload[sectionIdentifier][i].id === fileId)
                filesToUpload[sectionIdentifier].splice(i, 1);
        }

        $(this).parent().parent().remove();
    });

    this.clear = function () {
        for (var i = 0; i < filesToUpload[sectionIdentifier].length; ++i) {
            if (filesToUpload[sectionIdentifier][i].id.indexOf(sectionIdentifier) >= 0)
                filesToUpload[sectionIdentifier].splice(i, 1);
        }

        $(this).children(".fileList").empty();
    }

    return this;
};