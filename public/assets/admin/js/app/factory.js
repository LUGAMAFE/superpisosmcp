angular.module('app.controllers').factory('notify', ['$window', function(win) {
	return function(type, message) {
		angular.element('.alertas').html('<div class="'+type+' callout" data-closable="slide-out-up"><p>'+message+'</p><button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button></div>');
	};
}]);

angular.module('app.controllers').factory('validate', ['$window', function(win) {
	var msgs = [];
	return function(msg) {
		msgs.push(msg);
		if (msgs.length === 1) {
			win.alert(msgs.join('\n'));
			msgs = [];
		}
	};
}]);