Dropzone.autoDiscover = false;
/*function changeOpcionFoto($tipo, $id) {
	$url = '/ordenes/subir_fotografia/'+$id+'/'+$tipo;
	Dropzone.options.myDropzone = {
	 	init: function() {
	    	this.on("processing", function(file) {
	    		this.options.url = $url;
	    	});
	  	}
	};
}*/
var ctrl = angular.module('app.controllers', ['thatisuday.dropzone']);
ctrl.controller('fotosVehiculo', ['$scope', '$filter', '$element', 'services', function ($scope, $filter, $element, services) {
	$scope.id_orden = '';
	$scope.fotos = [];
	$scope.tipo_foto = '';
	
	$scope.getFotos = function($id_orden) {
		$scope.id_orden = $id_orden;
		services.getFotos($id_orden).success(function(response) {
			$scope.fotos = [];
			$scope.fotos.push.apply($scope.fotos, response);
			console.log($scope.fotos);
		});
		$scope.dzOptions = {
			url : '/ordenes/subir_fotografia/'+$scope.id_orden+'/'+angular.element('#tipo_foto').val(),
			paramName : 'photo',
			maxFilesize : '10',
			acceptedFiles : 'image/jpeg, images/jpg, image/png',
			addRemoveLinks : true,
		};

	}
	//Handle events for dropzone
	//Visit http://www.dropzonejs.com/#events for more events
	$scope.dzCallbacks = {
		'addedfile' : function(file){
			console.log(file);
			$scope.newFile = file;
		},
		'success' : function(file, xhr){
			//console.log(file, xhr);
			console.log($scope.id_orden);
			$scope.getFotos($scope.id_orden);
		},
	};
	
	//Apply methods for dropzone
	//Visit http://www.dropzonejs.com/#dropzone-methods for more methods
	$scope.dzMethods = {};
	$scope.removeNewFile = function(){
		$scope.dzMethods.removeFile($scope.newFile); //We got $scope.newFile from 'addedfile' event callback
	}

}]);