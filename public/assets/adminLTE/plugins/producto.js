$(document).on("ready", function(){
    function scanScrolls() {
        $('.scrollbar-inner').scrollbar();
    }
    scanScrolls();

    $(".guardar").on("click", function(){
        $("#btn-guardar").trigger("click");
    });

    $(".cancelar").on("click", function(){
        $("#btn-cancelar").trigger("click");
    });

    $("#btn-guardar").on("click", function(){
        uploader.uploadStoredFiles();
    });

    $("#btn-cancelar").on("click", function(){
        window.history.back();
    });
});