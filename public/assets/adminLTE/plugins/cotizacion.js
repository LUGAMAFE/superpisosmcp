//FINE UPLOADERS LOGIC
let $loaders = $(".overlay"); 
let uploadersManager = new FineUploadersManager({
    onLoadingFinished: function(){
        $loaders.toggleClass("d-none");
    },
    onAllUploaded: function () {
        $loaders.toggleClass("d-none");
        document.getElementById("form").submit();
    },
    onUploading: function(){
        $loaders.removeClass("d-none");
    },
    onErrorUploading(){
        $loaders.addClass("d-none");
    }
});

uploadersManager.changeInitConfiguration({
    multiple: false,
    alertOverride: true,
    filesType: "image",
    validation: {
        allowedExtensions: "jpg jpeg png",
    }
});

uploadersManager.addUploadGroup(".upload-area.individual", {
    validation: {
        image:{
            minWidth: 275,
            minHeight: 951,
            maxWidth: 3840,
            maxHeight: 1200,
        }
    }
});

/*uploadersManager.addUploadGroup(".upload-area.multiple", {
    multiple: true,
    validation: {
        itemLimit: 100,
        image:{
            minWidth: 724,
            minHeight: 724,
            maxWidth: 3840,
            maxHeight: 1600,
        }
    },
    callbacks: {
        onSessionRequestComplete: function(response, success, xhrOrXdr){
            let elems = this.getUploads();
            response.forEach((element, index) => {
                let inputExtras = JSON.parse(element.inputExtras);
                fileItemContainer = this.getItemByFileId(index);
                $(fileItemContainer).find(".qq-file-info")
                .append(`<div class="extras-uploader">
                            <div class="form-group">
                                <label>Url Ventana</label>
                                <input type="url" class="form-control form-control-sm" data-name-extra="url_ventana" placeholder="Url Ventana..." value="${inputExtras.url_ventana}">
                            </div>
                            <hr>
                            <div class="form-group checkbox-options">
                                <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" data-name-extra="activo_ventana" id="activoVentana-${element.uuid}" ${(inputExtras.activo_ventana != 0) ? "checked" : ""}>
                                <label class="custom-control-label" for="activoVentana-${element.uuid}">Activo</label>
                                </div>
                            </div>
                        </div>`
                );
            });
        },
        onSubmitted: function(id, name){
            fileItemContainer = this.getItemByFileId(id);
            let uuid = this.getUuid(id);
                $(fileItemContainer).find(".qq-file-info")
                .append(`<div class="extras-uploader">
                            <div class="form-group">
                                <label>Url Ventana</label>
                                <input type="url" class="form-control form-control-sm" data-name-extra="url_ventana" placeholder="Url Ventana...">
                            </div>
                            <hr>
                            <div class="form-group checkbox-options">
                                <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" data-name-extra="activo_ventana" id="activoVentana-${uuid}">
                                <label class="custom-control-label" for="activoVentana-${uuid}">Activo</label>
                                </div>
                            </div>
                        </div>`
                );
        }
    }
});*/

uploadersManager.createUploaders();

$('#submit-form').click(function(e) {
    let form = document.getElementById("form");
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;
    let uploaders = uploadersManager.getUnfilledUploaders();
    uploaders.forEach(element => {
        let closestRequerido = $("#"+element.id).tooltip({
            title: "Falta Una Imagen para el banner",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    });

    if(!error){
        uploadersManager.uploadAll();
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});
