$(document).ready(function(){
    let maxField = 10; //Input fields increment limitation
    let addButton = $('.add_button_urls'); //Add button selector
    let removeButtons = ".remove-button-url";
    let wrapper = $('.urls-container'); //Input field wrapper
    let fieldHTML = `<div class="input-group mt-2">
                        <input type="url" class="form-control" name="urls[]" placeholder="Url Video Youtube" required value="">
                        <span class="input-group-append">
                        <button type="button" class="btn btn-danger text-light btn-flat remove-button-url"><i class="fas fa-times"></i></button>
                        </span>
                    </div>`
    let x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', removeButtons, function(e){
        e.preventDefault();
        $(this).parent().parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});


//FINE UPLOADERS LOGIC
let $loaders = $(".overlay"); 
let uploadersManager = new FineUploadersManager({
    onLoadingFinished: function(){
        $loaders.toggleClass("d-none");
    },
    onAllUploaded: function () {
        $loaders.toggleClass("d-none");
        document.getElementById("form").submit();
    },
    onUploading: function(){
        $loaders.removeClass("d-none");
    },
    onErrorUploading(){
        $loaders.addClass("d-none");
    }
});

uploadersManager.changeInitConfiguration({
    multiple: false,
    alertOverride: true,
    filesType: "image",
    validation: {
        allowedExtensions: "jpg jpeg png",
    }
});

uploadersManager.addUploadGroup(".upload-area.video", {
    filesType: "video",
    validation: {
        itemLimit: 1,
        allowedExtensions: "mp4",
        sizeLimit: 1 * 1024 * 1024 * 40,  
    },
    messages: {
        onLeave: 'Los Archivos estan siendo subidos, si abandonas ahora las subidas seran canceladas.',
        typeError: 'El archivo {file} Tiene una extension invalida. </br>Extensiones Valida(s): {extensions}.',
        sizeError: 'El archivo {file} es demasiado grande. </br> El tamaño maximo por video es de {sizeLimit}.',
        tooManyItemsError: 'Demasiados videos Seleccioandos. </br> Solo Puedes subir uno.',
    },
});


uploadersManager.addUploadGroup(".upload-area.multiple.1920x800", {
    multiple: true,
    validation: {
        itemLimit: 500,
        image:{
            minWidth: 1920,
            minHeight: 800,
            maxWidth: 3840,
            maxHeight: 1600,
        }
    },
    callbacks: {
        onSessionRequestComplete: function(response, success, xhrOrXdr){
            let elems = this.getUploads();
            response.forEach((element, index) => {
                let inputExtras = JSON.parse(element.inputExtras);
                fileItemContainer = this.getItemByFileId(index);
                $(fileItemContainer).find(".qq-file-info")
                .append(`<div class="extras-uploader">
                            <div class="form-group">
                                <label>Url Banner</label>
                                <input type="url" class="form-control form-control-sm" data-name-extra="urlBanner" placeholder="Url Banner..." value="${inputExtras.url_banner}">
                            </div>
                        </div>`
                );
            });
        },
        onSubmitted: function(id, name){
            fileItemContainer = this.getItemByFileId(id);
                $(fileItemContainer).find(".qq-file-info")
                .append(`<div class="extras-uploader">
                            <div class="form-group">
                                <label>Url Banner</label>
                                <input type="url" class="form-control form-control-sm" data-name-extra="urlBanner" placeholder="Url Banner...">
                            </div>
                        </div>`
                );
        }
    }
});

uploadersManager.addUploadGroup(".upload-area.individual.1920x74", {
    validation: {
        image:{
            minWidth: 1920,
            minHeight: 74,
            maxWidth: 3840,
            maxHeight: 148,
        }
    }
});

uploadersManager.addUploadGroup(".upload-area.individual.1900x400", {
    validation: {
        image:{
            minWidth: 1900,
            minHeight: 400,
            maxWidth: 3800,
            maxHeight: 800,
        }
    }
});

uploadersManager.addUploadGroup(".upload-area.individual.900x900", {
    validation: {
        image:{
            minWidth: 900,
            minHeight: 900,
            maxWidth: 1800,
            maxHeight: 1800,
        }
    }
});

uploadersManager.createUploaders();

$('#submit-form').click(function(e) {
    let form = document.getElementById("form");
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;
    let uploaders = uploadersManager.getUnfilledUploaders();
    uploaders.forEach(element => {
        let closestRequerido = $("#"+element.id).tooltip({
            title: "Falta Una Imagen para el banner",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    });

    if(!error){
        uploadersManager.uploadAll();
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});
