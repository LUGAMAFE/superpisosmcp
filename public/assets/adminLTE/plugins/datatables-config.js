$.extend( true, $.fn.dataTable.defaults, {
    "language": {
        "emptyTable":       "No hay datos por mostrar en la tabla",
        "lengthMenu":       "Mostrar _MENU_ Registros por pagina.",
        "zeroRecords":      "Ningun registro encontrado - Lo sentimos!",
        "info":             "Mostrando Pagina _PAGE_ de _PAGES_",
        "infoEmpty":        "No hay registros para mostrar.",
        "infoFiltered":     "(Filtrado de _MAX_ registros totales)",
        "loadingRecords":   "Cargando...",
        "processing":       "Procesando...",
        "search":           "Buscar:",
        "paginate": {
            "first":        "First",
            "last":         "Last",
            "next":         "Siguiente",
            "previous":     "Anterior"
        },
        "select": {
            "rows": {
                _: "   %d Filas seleccionadas",
                0: "",
                1: "   Una fila seleccioanda"
            }
        }
    },
    "dom": '<"row"<"col-12 col-md-6"l><"col-12 col-md-6"f><"col-12"p>><"table-wrapper"t><"row"<"col-12 col-lg-6"i><"col-12 col-lg-6"p>>'
} );