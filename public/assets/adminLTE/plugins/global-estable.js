$(".requerido").attr('tabindex', '0');

function hideToolTips() {
    $(this).tooltip('hide');
}

function checkInformFormValidation(form){
    if (!form.checkValidity()) {
    // Create the temporary button, click and remove it
    var tmpSubmit = document.createElement('button')
    form.appendChild(tmpSubmit)
    tmpSubmit.click()
    form.removeChild(tmpSubmit)
    return true;
    } 
    return false;
}

function searchSVG(){
    $('img.svg').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');
            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');
    });
}

$(".cerrar-sesion").click( function(){  
    new jBox('Confirm', {
        content: 'Estas seguro que realmente deseas cerrar la sesion?',
        confirmButton: 'Si Cerrar',
        cancelButton: 'No Cancelar',
        confirm: function(){
            window.location.replace(baseURL + "administracion/cerrarSesion");
        },
    }).open();
});

function errorReload(){
    alert("Ha Ocurrido un error de logica en el programa. La pagina se recargara para proteger la integridad de los datos del sistema.");
    location.reload();
}

function sendPostJsonAjax(url, datos, success, error, before) {
    if(typeof error === 'undefined'){
        error = function (e) {
            console.log(e)
        };
    }
    if(typeof before === 'undefined'){
        before = function () {
        };
    }

    if(typeof success === 'undefined'){
        success = function (respuesta) {
            if (respuesta.error) {
                alert("Error");
                errorReload();
            }
        }
    }

    $.ajax({
        type: "POST",
        url: url,
        data: datos,
        dataType: "json",
        beforeSend: before,
        success: success,
        error: error
    });
}


const FineUploadersManager = function(config = {}){
    const isObject = function(item){
        return (item && typeof item === 'object' && !Array.isArray(item));
    }
    
    const isFunction = function(item){
        return (item && typeof item === 'function');
    }
    
    const isBoolean = function(item){
        return (typeof item === "boolean");
    }

    const isString = function(item){
        return (typeof item === 'string' || item instanceof String);
    }
    
    const isNull = function(item){
        return (typeof item === "null");
    }
    
    const isUndefined = function(item){
        return (typeof item === "undefined");
    }
    
    const isset = function(item){
        let type = typeof item;
        return (type != "undefined" && type != "null");
    }
    
    const createElementFromHTML = function(htmlString) {
        var div = document.createElement('div');
        div.innerHTML = htmlString.trim();
        // Change this to div.childNodes to support multiple top-level nodes
        return div; 
    }
    // const uuidv4Fine = function() {
    //     return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    //         var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    //         return v.toString(16);
    //     });
    // }
    if(!isObject(config)){
        throw new TypeError("Error creating FineUploadersManager The configuration parameter to create the FineUploadersManager is invalid. Expected Object.");
    }
    let _onUploading = _onLoading = _onLoadingFinished = _onErrorUploading = function(){},  _onAllUploaded, fineUploaders = [], uploaderGroups = [];
    const addUploader = function(uploader,utilities){
        fineUploaders.unshift({
            "uploader" : uploader,
            "utilities" : utilities,
        });
    };
    const UploadersGroupConfig = function(config){
        this.groupConfig = config;
        this.uploaders = [];
        this.addUploaderconfig = function(element, id){
            this.uploaders.push({
                element: element,
            });
        }
    };
    const validateUploader = function(uploaderValidar, elemento){
        let uploadspasubir = uploaderValidar.getUploads({
            status: [qq.status.SUBMITTED]
        }).length;
    
        let uploadsyasubidas = uploaderValidar.getUploads({
            status: [qq.status.UPLOAD_SUCCESSFUL]
        }).length;
    
        if(uploadspasubir <= 0 && uploadsyasubidas <= 0){
            return false;
        }
        return true;
    }
    Object.defineProperty(this, 'onUploading', {
        get: function() { return _onUploading; },        
        set: function(value) {        
            if(!isFunction(value)){
                throw new TypeError(`Error creating FineUploadersManager The configuration parameter property config.onUploading is invalid. Expected Function.`);
            }
         _onUploading = value;        
        },    
        configurable: false            
    });
    Object.defineProperty(this, 'onErrorUploading', {
        get: function() { return _onErrorUploading; },        
        set: function(value) {        
            if(!isFunction(value)){
                throw new TypeError(`Error creating FineUploadersManager The configuration parameter property config.onErrorUploading is invalid. Expected Function.`);
            }
         _onErrorUploading = value;        
        },    
        configurable: false            
    });
    Object.defineProperty(this, 'onAllUploaded', {
        get: function() { return _onAllUploaded; },        
        set: function(value) {        
            if(!isFunction(value)){
                throw new TypeError(`Error creating FineUploadersManager The configuration parameter property config.onAllUploaded is invalid. Expected Function.`);
            }
         _onAllUploaded = value;        
        },    
        configurable: false            
    });
    Object.defineProperty(this, 'onLoading', {
        get: function() { return _onLoading; },          
        set: function(value) {           
            if(!isFunction(value)){
                throw new TypeError(`Error creating FineUploadersManager The configuration parameter property config.onLoading is invalid. Expected Function.`);
            }
         _onLoading = value;        
        },               
        configurable: false 
    });
    Object.defineProperty(this, 'onLoadingFinished', {
        get: function() { return _onLoadingFinished; },       
        set: function(value) {         
            if(!isFunction(value)){
                throw new TypeError(`Error creating FineUploadersManager The configuration parameter property config.onLoadingFinished is invalid. Expected Function.`);
            }
         _onLoadingFinished = value;        
        },               
        configurable: false 
    });
    if(isset(config.onUploading)){
        this.onUploading = config.onUploading;
    }
    if(isset(config.onErrorUploading)){
        this.onErrorUploading = config.onErrorUploading;
    }
    if(isset(config.onAllUploaded)){
        this.onAllUploaded = config.onAllUploaded;
    }
    if(isset(config.onLoading)){
        this.onLoading = config.onLoading;
    }
    if(isset(config.onLoadingFinished)){
        this.onLoadingFinished = config.onLoadingFinished;
    }
    this.getUploadersCreated = function(){
        let uploaders = fineUploaders.map(function(upl){
            return {
                "uploader": upl.uploader,
                "id": upl.utilities.id,
            };
        });
        return uploaders;
    }
    this.getUnfilledUploaders = function(){
        let uploaders = this.getUploadersCreated();
        let uncompleted = [];
        uploaders.forEach(element => {
            if(!validateUploader(element.uploader, element.id)){
                uncompleted.push(element);
            }
        });
        return uncompleted;
    }
    this.uploadAll = function(){
        this.onUploading();
        fineUploaders.forEach(element => {
            let uploadspasubir = element.uploader.getUploads({
                status: [qq.status.SUBMITTED]
            }).length;
            let uploadsyasubidas = element.uploader.getUploads({
                status: [qq.status.UPLOAD_SUCCESSFUL]
            }).length;
            if(uploadsyasubidas > 0 && uploadspasubir <= 0){
                element.utilities.deferredReady.resolve();
            }else{
                element.uploader.uploadStoredFiles();
            }
        });
    }
    this.createUploaders = function(){
        if(!isset(this.onAllUploaded)){
            throw "Cannot createUploaders Uploaders because onAllUploaded Function is not defined in the FineUploadersManager Object.";
        }
        if(uploaderGroups.length <= 0){
            throw "There are not left uploaders to create.";
        }
        this.onLoading();
        let _this = this;
        while (uploaderGroups.length > 0) {
            let group = uploaderGroups.pop();
            group.uploaders.forEach(futureUpl => {
                let groupConfig = _.cloneDeep(group.groupConfig);
                let validator = new Validator(futureUpl.element, groupConfig),
                    validated = validator.getConfiguration();
                    validated.utilities.onErrorUploading = _this.onErrorUploading;
                let defaults = new FineUploaderDefaults(validated.utilities);
                let configuracion = _.merge(defaults, validated.config);
                let uploader = new qq.FineUploader(configuracion);
                addUploader(uploader,validated.utilities);
            });
        }
        let defferedsLoaded = fineUploaders.map(upl => upl.utilities.deferredLoaded);
        $.when.apply($, defferedsLoaded).done(function(){
            _this.onLoadingFinished();
        });
        let defferedsReady = fineUploaders.map(upl => upl.utilities.deferredReady);
        $.when.apply($, defferedsReady).done(function(){
            _this.onAllUploaded();
        });
    };
    this.addUploadGroup = function(selector, config = {}){
        if(typeof selector !== 'string'){
            throw new TypeError("Error in addUploadGroup function, The selector parameter granted to create the FineUploaders elements is invalid. Expected String.");
        }
        if(!isObject(config)){
            throw new TypeError("Error in addUploadGroup function, The configuration parameter to create the FineUploaders elements is invalid. Expected Object.");
        }
        initConfig = _.merge(_.cloneDeep(initUploaderConfiguration), config);
        let group = new UploadersGroupConfig(initConfig);
        
        $(selector).each(function(index, element) {
            let uploaderId = this.id;
            if(!uploaderId){
                throw "Error in addUploadGroup function, One of selected items does not contain an id to associate with a FineUploader object";
            }
            group.addUploaderconfig(element, uploaderId);
        });
        uploaderGroups.push(group);
    };

    this.changeInitConfiguration = function(config){
        if( !isObject(config) ){
            throw new TypeError("Error in changeDefaults function, The configuration parameter is invalid. Expected Object.");
        }
        initUploaderConfiguration = _.merge(config, initUploaderConfiguration);
    };

    let initUploaderConfiguration = {
        warnBeforeUnload: true,
        autoUpload: false,
        debug: false,
        session: {
            url : null,
            params : {},
            methodName : "archivosEntrada",
        },
        request: {
            url : null,
            params : {},
            methodName : "subirArchivos",
        },
        deleteFile: {
            enabled: true,
            url : null,
            params : {},
            methodName : "subirArchivos",
        },
        scaling: {
            hideScaled: true,
            sizes: [
                {name: "small", maxSize: 200},
                {name: "medium", maxSize: 600},
                {name: "large", maxSize: 1200}
            ]
        },
        resume: {
            enabled: true,
        },
        text:  {
            defaultResponseError: 'Un error desconocido a sucedido.'
        },
        messages: {
            onLeave: 'Los Archivos estan siendo subidos, si abandonas ahora las subidas seran canceladas.',
            typeError: 'El archivo {file} Tiene una extension invalida. </br>Extensiones Valida(s): {extensions}.',
            sizeError: 'El archivo {file} es demasiado grande. </br> El tamaño maximo por imagen es de {sizeLimit}.',
            tooManyItemsError: 'Demasiadas Imagenes Seleccioandas. </br> Solo Puedes subir una imagen.',
            confirmMessage: 'Estas Seguro que quieres eliminar el archivo {filename} del servidor?',
            minWidthImageError: 'La imagen no es lo suficientemente ancha',
            minHeightImageError: 'La imagen no es lo suficientemente alta',
            maxWidthImageError: 'La imagen es muy ancha',
            maxHeightImageError: 'La imagen es muy alta',
        },
        showMessage: function(message) {
            let modal = new jBox('Modal', {
                // width: 300,
                // height: 100,
                showCountdown: true,
                autoClose: 10000,
                delayOnHover: true,
                title: 'Error!',
                content: message, 
                onClose: function() {
                    setTimeout(() => {
                        modal.destroy();
                    }, 500);
                }
            }).open();
        },
        callbacks: {
            onError: function(id, name, errorReason, xhrOrXdr) {
                if(xhrOrXdr){
                    let modal = new jBox('Modal', {
                        // width: 300,
                        // height: 100,
                        showCountdown: true,
                        autoClose: 10000,
                        delayOnHover: true,
                        title: 'Error!',
                        content: qq.format("Error en archivo numero {} - {}. </br>Razon: {}", id, name, errorReason), 
                        onClose: function() {
                            setTimeout(() => {
                                modal.destroy();
                            }, 500);
                        }
                    }).open();
                    this.cancelAll();
                }
            }
        }
    }; 

    const Validator = function(uploaderHtmlElement, config){
        this.element = uploaderHtmlElement; //Objeto Uploader.
        this.id = this.element.id; //Id del uploader.
        this.stringIdError = `Error in uploader element with the id: ${this.id}.`;
        this.config = config; //Configuracion del usuario del plugin.
        this.alertOverride = false; //Informa en caso de que alguno de los atributos definidos en el html haya sido sobrescrito por la configuracion del javascript.
        this.inputName = null; //Nombre que tendra el input hidden, con la informacion de las imagenes subidas por el plugin.
        this.extensions = []; //Extensiones permitidas por el plugin.
        this.multiple = true; //Define si pueden subirse multiples archivos o no.
        this.limit = 0; //Define el limite de archivos que pueden subirse en caso de que la propiedad multiple este aciva.
        this.filesType = "file"; //Define el tipo de archivo ha ser subido esto ayuda a definir idioma del template y su contenido.
        this.fileSize = { //Define las limitaciones de tamaño que puedan tener los archivos subidos por el uploader.
            minSize: 0,
            maxSize: 0,
        }
        this.image = { //Define las limitaciones de tamaño de las imagenes que puedan ser subidas por el uploder.
            minWidth: 0,
            minHeight: 0,
            maxWidth: 0,
            maxHeight: 0,
        }
        //Clase para crear una configuracion de atributo.
        const AttrConfig = function(name, propertyName, type){
            this.name = name;
            this.propertyName = propertyName;
            this.type = type;
        };
        //Nombre de los atributos definidos y utilizables por el plugin.
        this.attrNames = { 
            inputName: new AttrConfig("upl-input-name", null, "string"),
            multipleFiles: new AttrConfig("upl-multiple","multiple", "boolean"),
            filesType: new AttrConfig("upl-files-type","filesType", "string"),
            extensions: new AttrConfig("upl-extensions","validation.allowedExtensions", "string"),
            itemLimitNumber: new AttrConfig("upl-files-limit","validation.itemLimit", "integer"),
            fileSize: {
                minSize: new AttrConfig("upl-min-size","validation.minSizeLimit", "integer"),
                maxSize: new AttrConfig("upl-max-size","validation.sizeLimit", "integer"),
            },
            image: {
                minWidth: new AttrConfig("upl-min-width","validation.image.minWidth", "integer"),
                minHeight: new AttrConfig("upl-min-height","validation.image.minHeight", "integer"),
                maxWidth: new AttrConfig("upl-max-width","validation.image.maxWidth", "integer"),
                maxHeight: new AttrConfig("upl-max-height","validation.image.maxHeight", "integer"),
            }
        };

        const UplConversor = function(properties, config){
            this.customProps = properties;
            this.config = config;
            
            this.assembleTemplate = function(fileType, multiple, lenguage = "spanish"){
                let drop, buttonUpload, processing, cancel, retry, deletefile, close, yes, no, ok;
                switch (lenguage) {
                    default:
                        cancel = "Cancelar", retry ="Reintentar", deletefile ="Eliminar", close = "Cerrar", yes = "Si", no = "No", ok = "Okey";
                        switch (fileType) {
                            case "image":
                                if(multiple){
                                    drop = "Suelta Tus Imagenes Aqui";
                                    buttonUpload = "Subir Imagenes";
                                    processing = "Procesando Imagenes Seleccionadas...";
                                }else{
                                    drop = "Suelta Tu Imagen Aqui";
                                    buttonUpload = "Subir Imagen";
                                    processing = "Procesando Imagen Seleccionada...";
                                }
                                break;
                            case "video":
                                if(multiple){
                                    drop = "Suelta Tus Videos Aqui";
                                    buttonUpload = "Subir Videos";
                                    processing = "Procesando Videos Seleccionados...";
                                }else{
                                    drop = "Suelta Tu Video Aqui";
                                    buttonUpload = "Subir Video";
                                    processing = "Procesando Video Seleccionado...";
                                }
                                break;
                            default:
                                if(multiple){
                                    drop = "Suelta Tus Archivos Aqui";
                                    buttonUpload = "Subir Archivos";
                                    processing = "Procesando Archivos Seleccionados...";
                                }else{
                                    drop = "Suelta Tu Archivo Aqui";
                                    buttonUpload = "Subir Archivo";
                                    processing = "Procesando Archivo Seleccionado...";
                                }
                                break;
                        }
                        break;
                }
                let template;
                if (fileType === "image"){
                    template = `<div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="${drop}" style="overflow-x: hidden;">
                    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
                    </div>
                        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                            <span class="qq-upload-drop-area-text-selector"></span>
                        </div>
                        <div class="qq-upload-button-selector qq-upload-button">
                            <div>${buttonUpload}</div>
                        </div>
                        <span class="qq-drop-processing-selector qq-drop-processing">
                            <span>${processing}</span>
                            <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                        </span>
                        <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
                            <li>
                                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                                <div class="qq-progress-bar-container-selector">
                                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                                </div>
                                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                                <div class="qq-thumbnail-wrapper">
                                    <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                                </div>
                                <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                                <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                                    <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                                    ${retry}
                                </button>
    
                                <div class="qq-file-info">
                                    <div class="qq-file-name">
                                        <span class="qq-upload-file-selector qq-upload-file"></span>
                                        <span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>
                                    </div>
                                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                                    <span class="qq-upload-size-selector qq-upload-size"></span>
                                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                                        <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                                    </button>
                                    <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                                        <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                                    </button>
                                    <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                                        <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                                    </button>
                                </div>
                            </li>
                        </ul>
    
                        <dialog class="qq-alert-dialog-selector">
                            <div class="qq-dialog-message-selector"></div>
                            <div class="qq-dialog-buttons">
                                <button type="button" class="qq-cancel-button-selector">${close}</button>
                            </div>
                        </dialog>
    
                        <dialog class="qq-confirm-dialog-selector">
                            <div class="qq-dialog-message-selector"></div>
                            <div class="qq-dialog-buttons">
                                <button type="button" class="qq-cancel-button-selector">${no}</button>
                                <button type="button" class="qq-ok-button-selector">${yes}</button>
                            </div>
                        </dialog>
    
                        <dialog class="qq-prompt-dialog-selector">
                            <div class="qq-dialog-message-selector"></div>
                            <input type="text">
                            <div class="qq-dialog-buttons">
                                <button type="button" class="qq-cancel-button-selector">${cancel}</button>
                                <button type="button" class="qq-ok-button-selector">${ok}</button>
                            </div>
                        </dialog>
                    </div>`;
                }else{
                    template = `<div class="qq-uploader-selector qq-uploader" qq-drop-area-text="${drop}" style="overflow-x: hidden;">
                    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
                    </div>
                    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                        <span class="qq-upload-drop-area-text-selector"></span>
                    </div>
                    <div class="qq-upload-button-selector qq-upload-button">
                        <div>${buttonUpload}</div>
                    </div>
                        <span class="qq-drop-processing-selector qq-drop-processing">
                            <span>${processing}</span>
                            <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                        </span>
                    <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                        <li>
                            <div class="qq-progress-bar-container-selector">
                                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                            </div>
                            <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                            <span class="qq-upload-file-selector qq-upload-file"></span>
                            <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                            <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                            <span class="qq-upload-size-selector qq-upload-size"></span>
                            <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">${cancel}</button>
                            <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">${retry}</button>
                            <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">${deletefile}</button>
                            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                        </li>
                    </ul>

                    <dialog class="qq-alert-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">${close}</button>
                        </div>
                    </dialog>

                    <dialog class="qq-confirm-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">${no}</button>
                            <button type="button" class="qq-ok-button-selector">${yes}</button>
                        </div>
                    </dialog>

                    <dialog class="qq-prompt-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <input type="text">
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">${cancel}</button>
                            <button type="button" class="qq-ok-button-selector">${ok}</button>
                        </div>
                    </dialog>
                </div>`;
                }
                return createElementFromHTML(template);
            }

            this.getUtilities = function(){
                let utilities = {};
                if(this.customProps.inputName == null){
                    this.customProps.inputName = this.customProps.id + "-info";
                }
                utilities.inputName = this.customProps.inputName;
                utilities.id = this.customProps.id;
                utilities.element = this.customProps.element;
                utilities.numberOfScales = 0;

                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", utilities.inputName);
                utilities.element.parentNode.insertBefore(input,  utilities.element.nextSibling);

                if (isset(this.config.scaling)) {
                    if (isset(this.config.scaling.sizes)) {
                        utilities.numberOfScales = this.config.scaling.sizes.length;
                    }
                    if (isset(this.config.scaling.hideScaled)) {
                        utilities.hideScaled = this.config.scaling.hideScaled;
                    }else{
                        utilities.hideScaled = false;
                    }
                }
                if (isset(this.config.callbacks.onError)) {
                    utilities.onError = this.config.callbacks.onError;
                }else{
                    utilities.onError = function(){};
                }
                if (isset(this.config.callbacks.onSessionRequestComplete)) {
                    utilities.onSessionRequestComplete = this.config.callbacks.onSessionRequestComplete;
                }else{
                    utilities.onSessionRequestComplete = function () {};
                }
                if (isset(this.config.callbacks.onAllComplete)) {
                    utilities.onAllComplete = this.config.callbacks.onAllComplete;
                }else{
                    utilities.onAllComplete = function () {};
                }
                if (isset(this.config.callbacks.onCancel)) {
                    utilities.onCancel = this.config.callbacks.onCancel;
                }else{
                    utilities.onCancel= function () {};
                }
                if (isset(this.config.callbacks.onDeleteComplete)) {
                    utilities.onDeleteComplete = this.config.callbacks.onDeleteComplete;
                }else{
                    utilities.onDeleteComplete = function () {};
                }
                this.config.callbacks = _.omit(this.config.callbacks, ['onError', 'onSessionRequestComplete', 'onAllComplete', 'onCancel', 'onDeleteComplete']);
                utilities.deferredLoaded = $.Deferred(),
                utilities.deferredReady = $.Deferred(),
                utilities.deferredUploading = $.Deferred(),
                utilities.primeramodificacion = true;
                return utilities;
            }
            this.getConversion = function(){
                let objConfig = {};
                objConfig.multiple = this.customProps.multiple;
                objConfig.validation = {
                    itemLimit:  this.customProps.itemLimit,
                    allowedExtensions: this.customProps.extensions,
                    sizeLimit: this.customProps.fileSizes.maxSize,
                    minSizeLimit: this.customProps.fileSizes.minSize,
                    image: {
                        maxHeight: this.customProps.image.maxHeight,
                        maxWidth: this.customProps.image.maxWidth,
                        minHeight: this.customProps.image.minHeight,
                        minWidth:  this.customProps.image.minWidth,
                    }
                };
                if(!isset(this.config.template)){
                    objConfig.template = this.assembleTemplate(this.customProps.filesType, this.customProps.multiple);
                }
                this.getUrlConfiguration(objConfig);
                _.merge(this.config, objConfig);
                return this.config;
            }

            this.getUrlConfiguration = function(objConfig){
                objConfig.session = {};
                objConfig.request = {};
                objConfig.deleteFile = {};
                objConfig.session.params = objConfig.request.params = objConfig.deleteFile.params = {
                    uploaderId: this.customProps.id,
                };
                if(isset(config.sharedParams)){
                    _.merge(objConfig.session.params, config.sharedParams);
                }
                let url = window.location.href.split('?')[0];
                if(!isset(this.config.session.endpoint) && isset(this.config.session.methodName) ){
                    objConfig.session.endpoint = `${url}/${this.config.session.methodName}`;
                }
                if(!isset(this.config.request.endpoint) && isset(this.config.request.methodName) ){
                    objConfig.request.endpoint = `${url}/${this.config.request.methodName}`;
                }
                if(!isset(this.config.deleteFile.endpoint) && isset(this.config.deleteFile.methodName) ){
                    objConfig.deleteFile.endpoint = `${url}/${this.config.deleteFile.methodName}`;
                }
            }
        };

        this.getConfiguration = function(){
            this.getOverrideConfig();
            this.getInputName();
            this.getExtensions();
            this.getMultiplicity();
            this.getFilesType();
            this.getSizeLimits();
            this.getImageLimits();

            let neededProperties = {
                element: this.element,
                id: this.id,
                inputName: this.inputName,
                extensions: this.extensions,
                multiple: this.multiple,
                itemLimit: this.limit,
                fileSizes: this.fileSize,
                filesType: this.filesType,
                image: this.image,
            };
            let conversor = new UplConversor(neededProperties, this.config);
            let conversion = {};
            conversion.utilities = conversor.getUtilities();
            conversion.config = conversor.getConversion();
            if(conversion.utilities.numberOfScales > 0 && conversion.config.validation.itemLimit > 0){
                conversion.config.validation.itemLimit = conversion.config.validation.itemLimit * (conversion.utilities.numberOfScales + 1);
            }
            return conversion;
        }
        /**
         *  Funcion que encuentra en el objeto de configuracion si esta contiene cierta propiedad y la regresa.
         *  Puede navegar dentro de las propiedades de el objeto de configuracion.
         * */
        this.config.getNestedProperty = function(propertyPath){
            if(!propertyPath){
                return null;
            }
            var properties = propertyPath.split('.');
            var obj = this;
            for (var i = 0; i < properties.length; i++) {
                var prop = properties[i];
                if(!obj || !obj.hasOwnProperty(prop)){
                    return null;
                } else {
                    obj = obj[prop];
                }
            }
            return obj;
        }
        /**
         *  Funcion que encuentra en el objeto de configuracion si esta contiene cierta propiedad.
         *  Puede navegar dentro de las propiedades de el objeto de configuracion.
         * */
        this.config.hasOwnNestedProperty = function(propertyPath){
            if(!propertyPath){
                return false;
            }
            var properties = propertyPath.split('.');
            var obj = this;
            for (var i = 0; i < properties.length; i++) {
                var prop = properties[i];
                if(!obj || !obj.hasOwnProperty(prop)){
                    return false;
                } else {
                    obj = obj[prop];
                }
            }
            return true;
        };
        this.existConfigProperty = function(attrInput, atributeConfig){
            let property = this.config.getNestedProperty(atributeConfig.propertyName),
            existsProperty = this.config.hasOwnNestedProperty(atributeConfig.propertyName),
            returnConfig = {};
            if(!existsProperty){
                return false;
            }else if(this.alertOverride && attrInput !== null && existsProperty){
                throw `Attribute ${atributeConfig.name} is being overrided by the property in configuration object: ${atributeConfig.propertyName}. ${this.stringIdError}`;
            }else if(existsProperty){
                if(atributeConfig.type != null && typeof property != atributeConfig.type){
                    throw new TypeError(`Config uploader property ${atributeConfig.propertyName} expected ${atributeConfig.type}. ${this.stringIdError}`);
                }
                returnConfig.property = property;
                return returnConfig;
            }
            return false;
        }
        /**
         * Funcion para obtener un atributo especifico.
         */
        this.getAttr = function(atribute){
            let attr = this.element.getAttribute(atribute.name);
            if(attr !== null){
                attr = attr.trim();
                if(attr.length <= 0){
                    throw `Attribute ${atribute.name} value is empty. ${this.stringIdError}`;
                }
                return attr;
            }
            return null;
        }
        this.getValue = function(attributeConfig){
            let attr = this.getAttr(attributeConfig);
            let existsProperty = this.config.hasOwnNestedProperty(attributeConfig.propertyName);
            let property, val;
            if(existsProperty){
                property = this.config.getNestedProperty(attributeConfig.propertyName);
            }
            if(attr === null && !existsProperty){
                return null;
            }else if(this.alertOverride && attr !== null && existsProperty){
                throw `Attribute: ${attributeConfig.name}, is being overrided by the property in configuration object: ${attributeConfig.propertyName}. ${this.stringIdError}`;
            } else if (attr !== null && !existsProperty){
                val = {
                    input: "Attributte",
                    name: attributeConfig.name,
                    value: attr,
                }
            } else{
                val = {
                    input: "Property",
                    name: attributeConfig.propertyName,
                    value: property,
                }
            }
            if(attributeConfig.type != null){
                switch (attributeConfig.type) {
                    case "string":
                        if(typeof val.value != attributeConfig.type){
                            throw new TypeError(`${val.input}: ${val.name}, invalid type expected ${attributeConfig.type}. ${this.stringIdError}`);
                        }  
                        break;
                    case "number":
                        val.value = + val.value;
                        if(isNaN(val.value)){
                            throw new TypeError(`${val.input}: ${val.name}, invalid type expected ${attributeConfig.type}. ${this.stringIdError}`);
                        }
                        break;
                    case "float":
                        val.value = + val.value;
                        if(isNaN(val.value)){
                            throw new TypeError(`${val.input}: ${val.name}, invalid type expected ${attributeConfig.type}. ${this.stringIdError}`);
                        }
                        let isFloat = val.value % 1 !== 0;
                        if(!isFloat){
                            throw new TypeError(`${val.input}: ${val.name}, invalid type expected ${attributeConfig.type}. ${this.stringIdError}`);
                        }
                        break;
                    case "integer":
                        let intParse = parseInt(val.value);
                        if(Number.isInteger(intParse)){
                            if(intParse < 0){
                                throw `${val.input}: ${val.name}, cannot be negative. ${this.stringIdError}`;
                            }
                            val.value = intParse;
                        }else{
                            throw `${val.input}: ${val.name}, is not a integer number. ${this.stringIdError}`;
                        }
                        break;
                    case "boolean":
                        if(isString(val.value) && ( val.value.toLowerCase() === 'true' || val.value.toLowerCase() === 'false') ){
                            val.value = (val.value.toLowerCase() === 'true');
                        }else if(typeof val.value != attributeConfig.type){
                            throw new TypeError(`${val.input}: ${val.name}, invalid type expected ${attributeConfig.type}. ${this.stringIdError}`);
                        }
                        break;
                    default:
                        throw `${val.input}: ${val.name}, invalid type founded. ${this.stringIdError}`;
                }
            }
            return val.value;
        }
        this.getOverrideConfig = function(){
            if (this.config.hasOwnProperty("alertOverride")) {
                if (!isBoolean(this.config.alertOverride)) {
                    throw new TypeError(`Config uploader property alertOverride expected boolean. ${this.stringIdError}`);
                }
                this.alertOverride = this.config.alertOverride;
            }
        }
        this.getInputName = function(){
            let inputName = this.getValue(this.attrNames.inputName);
            if( inputName != null ){
                this.inputName = inputName;
            }
        }
        this.getExtensions = function(){
            let extensions = this.getValue(this.attrNames.extensions);
            if(extensions !== null && extensions.length > 0){
                let tokens = extensions.split(/(\s+)/).filter( e => e.trim().length > 0);
                this.extensions = tokens;
            }
        }
    
        this.getMultiplicity = function(){
            let multiple = this.getValue(this.attrNames.multipleFiles);
            if(multiple !== null){
                this.multiple = multiple;
            }
            
            let itemLimit = this.getValue(this.attrNames.itemLimitNumber);

            if(itemLimit !== null){
                if(itemLimit > 1){
                    if(this.multiple == false){
                        throw `${this.attrNames.multipleFiles.propertyName} does not match logic limitations of ${this.attrNames.itemLimitNumber.propertyName} property. ${this.stringIdError}`;
                    }
                }
                this.limit = itemLimit;
            }
            if(this.multiple == false){
                this.limit = 1;
            }
        }
    
        this.getFilesType = function() {
            let filesType = this.getValue(this.attrNames.filesType);
            if(filesType !== null){
                switch (filesType) {
                    case "file":
                    case "image":
                    case "video":
                        this.filesType = filesType;
                        break;    
                    default:
                        throw `Property ${this.attrNames.filesType.propertyName} value is not recognized. ${this.stringIdError}`;
                }
            }
        }
    
        this.getSizeLimits = function(){
            let minSize = this.getValue(this.attrNames.fileSize.minSize),
                maxSize = this.getValue(this.attrNames.fileSize.maxSize);
            if(minSize !== null){
                this.fileSize.minSize
            }
            if(maxSize !== null){
                this.fileSize.maxSize
            }
        }
    
        this.getImageLimits = function(){
            let minWidth = this.getValue(this.attrNames.image.minWidth),
                minHeight = this.getValue(this.attrNames.image.minHeight),
                maxWidth = this.getValue(this.attrNames.image.maxWidth),
                maxHeight = this.getValue(this.attrNames.image.maxHeight);   
            if(minWidth !== null){
                this.image.minWidth = minWidth;
            }
            if(minHeight !== null){
                this.image.minHeight = minHeight;
            }
            if(maxWidth !== null){
                this.image.maxWidth = maxWidth;
            }
            if(maxHeight !== null){
                this.image.maxHeight = maxHeight;
            }   
        }
    };

    const FineUploaderDefaults = function(utilities){
        this.element = utilities.element;
        this.callbacks = {
            onSessionRequestComplete : function(response, success){
                if(response === null || response.length <= 0){
                    utilities.primeramodificacion = false;
                }
                utilities.deferredLoaded.resolve();
                utilities.onSessionRequestComplete.call(this, response, success);
            },
            onAllComplete : function(succeeded, failed){
                if(utilities.primeramodificacion === true){
                    utilities.primeramodificacion = false;
                    return;
                }
                if(failed.length <= 0 && succeeded.length > 0){
                    let parentId;
                    let jsonImgInfo = [];
                    while(succeeded.length > 0){
                        parentId = this.getParentId(succeeded[0]);
                        if(parentId == null){
                            parentId = succeeded[0];
                        }
                        let deleteItems = [];
                        for(let j = 0; j < succeeded.length; j++){
                            let actualId = this.getParentId(succeeded[j]);
                            if(actualId == null){
                                actualId = succeeded[j];
                            }
                            if(parentId == actualId){
                                deleteItems.push(succeeded[j]);
                            }
                        }
    
                        for (let x = 0; x < deleteItems.length; x++) {
                            var indexDelete = succeeded.indexOf(deleteItems[x]);
                            if (indexDelete !== -1) succeeded.splice(indexDelete, 1);
                        }
    
                        jsonImgInfo.push({ uuid: this.getUuid(parentId), name: this.getName(parentId)});
                    }
                    if(parentId === null){
                        return;
                    }
                    let data = JSON.stringify(jsonImgInfo);
                    $(`input[name="${utilities.inputName}"]`).val(data);
                    utilities.deferredReady.resolve();
                }else if(succeeded.length > 0) {
                    succeeded.forEach(element => {
                        this.deleteFile(element);
                    });
                    this.cancelAll();
                }    
                utilities.deferredUploading.resolve();
            },
            onCancel : function(id, name){
                let idElement = id;
                let parent = this.getParentId(idElement);
                if(parent === null){
                    let uploads = this.getUploads({
                        status: [qq.status.SUBMITTED]
                    })
                    for(let i = 0; i < uploads.length; i++){
                        let actualId = this.getParentId(uploads[i]["id"]);
                        if(actualId == idElement){
                            this.cancel(uploads[i]["id"]);
                        }
                    }
                }
                utilities.onCancel(id, name);
            },
            onDeleteComplete: function (id) {
                let actualId = this.getParentId(id);
                if(actualId == null){
                    actualId = id;
                }
                let uploads = this.getUploads({
                    status: [qq.status.UPLOAD_SUCCESSFUL, qq.status.SUBMITTED]
                });
                uploads.forEach(element => {
                    let elementId = this.getParentId(element.id);
                    if(elementId == null){
                        elementId = element.id;
                    }
                    if(elementId == actualId){
                        this.cancel(element.id);
                    }
                });
                utilities.onDeleteComplete.call(this,id);
            },
            onError: function(id, name, errorReason, xhrOrXdr) {
                utilities.onError.call(this, id, name, errorReason, xhrOrXdr);
                utilities.onErrorUploading();
            }
        };
    };
};
