let $loaders = $(".overlay"); 
let uploadersManager = new FineUploadersManager({
    onLoadingFinished: function(){
        $loaders.toggleClass("d-none");
    },
    onAllUploaded: function () {
        $loaders.toggleClass("d-none");
        if(getFiltersConfiguration()){
            document.getElementById("form").submit();
        }
    },
    onUploading: function(){
        $loaders.removeClass("d-none");
    },
    onErrorUploading(){
        $loaders.addClass("d-none");
    }
});

uploadersManager.addUploadGroup("#imagen-home-categoria", {
    multiple: false,
    filesType: "image",
    validation: {
        allowedExtensions: "jpg jpeg png",
        itemLimit: 1,
        image:{
            minWidth: 250,
            minHeight: 250,
            maxWidth: 1000,
            maxHeight: 1000,
        }
    },
    sharedParams: {
        idCategoria: idCategoria,
    },
});

uploadersManager.addUploadGroup("#imagen-banner-categoria", {
    multiple: false,
    filesType: "image",
    validation: {
        allowedExtensions: "jpg jpeg png",
        itemLimit: 1,
        image:{
            minWidth: 1920,
            minHeight: 300,
            maxWidth: 3840,
            maxHeight: 600,
        }
    },
    sharedParams: {
        idCategoria: idCategoria,
    },
});

uploadersManager.createUploaders();

$('#submit-form').click(function(e) {
    let form = document.getElementById("form");
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;
    let uploaders = uploadersManager.getUnfilledUploaders();
    uploaders.forEach(element => {
        let closestRequerido = $("#"+element.id).tooltip({
            title: "Falta Una Imagen para el banner",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    });

    if(!error){
        uploadersManager.uploadAll();
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});
