$.switcher = function (filter) {

    var $haul = $('input[type=checkbox]');

    if (filter !== undefined && filter.length) {
        $haul = $haul.filter(filter);
    }

    $haul.on('click', function(){
        event.stopPropagation();
    });

    $haul.each(function () {

        toggleSwitchCopia = function (event, param) {
            $check = $(this);
            let id = $check.attr("data-check-id"),
            elementos = $(".check-mostrar[data-check-id='" + id + "']"),
            $original = elementos.filter('td[style="display: none;"] .check-mostrar');
            // $mostrar = $check.next(".mostrar");
            // activado === true ? $mostrar.text("mostrar") : $mostrar.text("ocultar");
            $original.trigger('click');
        };

        toggleSwitchFromSwitcher = function (e) {
            let $thisSwitcher = $(this);
            //let esCopia = $thisSwitcher.is('td.child .ui-switcher');
            let $checkbox = $thisSwitcher.next('input[type=checkbox]');
            $checkbox.trigger('click');           
        };

        toggleSwitch = function (e) {
            let $inputCheck = $(this);

            let $switcher = $inputCheck.prev(".ui-switcher");

            let checked = $inputCheck.prop('checked');

            checked ? $checkbox.addClass("datachecked") : $checkbox.removeClass("datachecked");

            $switcher.attr('aria-checked', checked);

            $mostrar = $inputCheck.next(".mostrar");
            checked === true ? $mostrar.text("mostrar") : $mostrar.text("ocultar");
        };

        var $checkbox = $(this);
            $hermano = $checkbox.prev(".ui-switcher");
            $tieneSwitcher= $hermano.is(".ui-switcher");

        if(!$tieneSwitcher){
            $original = $checkbox;
            $checkbox.hide();
            $switcher = $(document.createElement('div')).addClass('ui-switcher').attr('aria-checked', $checkbox.hasClass("datachecked"));

            if($checkbox.hasClass("datachecked")){
                $checkbox.prop('checked', true);
            }

            if($checkbox.is('td.child .check-mostrar')){
                $checkbox.on('click', toggleSwitchCopia);
            }

            $switcher.on('click', toggleSwitchFromSwitcher);
            $checkbox.on('click', toggleSwitch);

            $switcher.insertBefore($checkbox);

        }else if($checkbox.is('td.child .check-mostrar')){
            console.log(jQuery._data( $checkbox.get(0), "events" ));
            console.log(jQuery._data( $hermano.get(0), "events" ));
            $checkbox.on('click', toggleSwitchCopia);
            $hermano.on('click', toggleSwitchFromSwitcher);
            $checkbox.on('click', toggleSwitch);
        }
        // else{
        //     let id = $checkbox.attr("data-check-id"),
        //     elementos = $(".check-mostrar[data-check-id='" + id + "']");
        //     if(elementos.length == 2){
        //         let $original = elementos.filter('td[style="display: none;"] .check-mostrar');
        //         let $copia = elementos.filter('td.child .check-mostrar');

        //         let $originalChecked = $original.hasClass("datachecked");
        //         let $copiaChecked = $copia.hasClass("datachecked");

        //         if($originalChecked && !$copiaChecked){
        //             $copia.addClass("datachecked")
        //         }else if(!$originalChecked && $copiaChecked){
        //             $copia.removeClass("datachecked")
        //         }

        //         $mostrar = $copia.next(".mostrar");
        //         $originalChecked === true ? $mostrar.text("mostrar") : $mostrar.text("ocultar");
        //     }
            
        // }
    });

};
