let $loader = $(".overlay");
$(window).on("load", function(){
    $loader.toggleClass("d-none");
});

$(document).ready(function() {

    let table = $('#table_id').DataTable({ 
        processing: true,
        //serverSide: true,
        ajax: {
            url: "administracion/inventario/productos/datatable",
            type: "POST"
        },
        paging: true,
        lengthChange: true,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: false,
        deferRender: true,
        columnDefs: [
            { orderable: false, width: "20%", responsivePriority: 1, targets: -1},
            { width: "11%", targets: -2 },
            { responsivePriority: 3, targets: -3 },
            { className: "id-producto", width: "10%", responsivePriority: 1, targets: 0 },
            { width: "30%", responsivePriority: 2, targets: 1 }
        ],
        columns: [
            { data: "articulo", name: "Articulo"},
            { data: "nombre_articulo", name: "Nombre Articulo"},
            { data: "categoria", name: "Categoria"},
            { data: "subcategoria", name: "Subcategoria"},
            { data: "mostrar", name: "Mostrar"},
            { data: "nuevo_prod", name: "Nuevo Producto"},
            { data: "info", name: "Info"}
        ]
    });

    
    table.on( 'draw', function () {
        $.switcher('.check-mostrar');
    });

    table.on( 'responsive-resize', function ( e, datatable, columns ) {
        $.switcher('.check-mostrar');
    });

    table.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $.switcher('.check-mostrar');
    });

    //table.page.len( 1 ).draw();
    
    $(document).on("change", "tr[role='row'] .check-mostrar", function(){
        let $checkbox = $(this),
        $parentTR = $checkbox.parent().parent();
        id = $checkbox.parent().parent().find(".id-producto").text().trim(),
        activado = $checkbox.prop('checked');


        let url = siteURL + "actualizarVisibilidadProducto";
        let datos = {
            idProducto: id, 
            estado:activado
        };

        let success = function(respuesta){
            if(respuesta.error === true){
                errorReload();
            }
        }

        sendPostJsonAjax(url, datos, success, errorReload);
    });
});

let $botonActualizar = $("#actualizar-registros");
let $loadersAct = $(".loader-actualizar");
let botonesClases = "btn-light btn-danger";

$botonActualizar.click(function(){
    let url = siteURL + "actualizarRegistrosAjax";
    $loader.toggleClass("d-none");
    $botonActualizar.find(".texto").text("Actualizando...");
    $botonActualizar.toggleClass(botonesClases);
    $loadersAct.toggle();
    let success = function(respuesta){
        $loader.toggleClass("d-none");
        $botonActualizar.toggleClass(botonesClases);
        $loadersAct.toggle();
        if(respuesta.error === true){
            $botonActualizar.find(".texto").text("Error al actualizar registros. Reintentar");
            new jBox('Notice', {
                title: respuesta.mensaje,
                content: respuesta.title,
                color: respuesta.color,
                showCountdown: true,
                autoClose: 8000,
                delayOnHover: true,
                attributes: {y: 'bottom'}});
        }else{
            location.reload();
        }
    }

    let error = function (e) {
        $loader.toggleClass("d-none");
        $botonActualizar.toggleClass(botonesClases);
        $loadersAct.toggle();
        $botonActualizar.find(".texto").text("Error al actualizar registros. Reintentar");
    };
    
    sendPostJsonAjax(url, [], success, error);
});
