$(document).ready(function(){
    let $menu = $("#menu").mmenu({
        extensions 	: [ "position-bottom", "fullscreen"],
        navbars		: [
            {
                content : [ "title", "prev", "close" ]
            },
            {
                content : [ "searchfield" ]
            },
        ],
        searchfield: {
            panel: true,
            noResults: "Sin resultados",
            placeholder: "Buscar en Menu",
            clear: true
        },
        iconbar: {
            "use": true,
            "top": [
                '<div class="cart"><a href="carrito"><div class="icon"><img src="assets/img/iconos/shopping.svg"></div></a></div>'
            ],
            "bottom": [
                '<a href="https://www.facebook.com/mayoreoceramicodelapeninsula" class="facebook icono"><i class="fab fa-facebook-square"></i></a>',
                '<a href="https://www.youtube.com/channel/UCBbSj9LP2avuxdnwapLciFw" class="youtube icono"><i class="fab fa-youtube"></i></a>',
            ]
         }
    }, {
        "searchfield": {
           "clear": true
        }}
    );
    var api = $menu.data( "mmenu" );
    //api.open();
    $("#menu").toggle();    
});