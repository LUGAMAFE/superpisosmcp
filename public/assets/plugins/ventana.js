$(window).on('load', function() {
    $('[data-fancybox="ventana"]').attr("style","display:none");
    $('[data-fancybox="ventana"]').fancybox({
      buttons : [ 
        'slideShow',
        'share',
        'zoom',
        'fullScreen',
        'close'
      ],
      thumbs : {
        autoStart : true
      },
      //hideScrollbar: true,
      afterLoad: function( instance, slide ) {
        $('body').addClass("overflow-y-hidden");
      },
      afterClose: function( instance, slide ) {
        $('body').removeClass("overflow-y-hidden");
      }
    });
    setTimeout(function(){
      $('[data-fancybox="ventana"]').attr("style","width:31.25rem");  
      $('[data-fancybox="ventana"]').trigger( "click"); 
    }, 5000);
    //$('[data-fancybox="ventana"]').trigger( "click");
    //$('body').attr("style","overflow:hidden");
  });

  var swiper = new Swiper('.swiper-container', {
    spaceBetween: 30,
    centeredSlides: true,
    loop: true,
    preventClicks: false,
    touchEventsTarget: 'container',
    preventClicksPropagation: false,
    autoplay: {
      delay: 6000,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      270: {
        height: 260
      },
      // when window width is >= 320px
      320: {
        height: 324
      },
      // when window width is >= 480px
      375: {
        height: 372
      },
      // when window width is >= 500px
      500: {
        height: 500
      },
      2500: {
		    height: 724
      }
    },
    on: {
      click: function(event) {
        let elementoClick = $(event.target);
        if(elementoClick.is("a")){
          let $elementoActivo = this.$el;
          $elementoActivo = $($elementoActivo);
          let anchor = $elementoActivo.find("a");
          let $url = anchor.attr("href");
          window.location.href = $url;
        }
      },
    }
  });